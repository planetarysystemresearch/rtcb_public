#define MPIADDRESS(ARG) call MPI_Get_Address(ARG,disp(j1)); j1=j1+1

#define MPIUNPACKREAL(ARG) call MPI_Unpack(buffer,SIZEOFBUFFER,pos,ARG,1,MPI_REAL8,MPI_COMM_WORLD)   
#define MPIUNPACKINTEGER(ARG) call MPI_Unpack(buffer,SIZEOFBUFFER,pos,ARG,1,MPI_INTEGER,MPI_COMM_WORLD)
#define MPIUNPACKLOGICAL(ARG) call MPI_Unpack(buffer,SIZEOFBUFFER,pos,ARG,1,MPI_LOGICAL,MPI_COMM_WORLD)
#define MPIUNPACKCMPLX(ARG) call MPI_Unpack(buffer,SIZEOFBUFFER,pos,ARG,1,MPI_DOUBLE_COMPLEX,MPI_COMM_WORLD)  

#define MPIUNPACKINTARR(ARG,SIZE) call MPI_Unpack(buffer,SIZEOFBUFFER,pos,ARG,SIZE,MPI_INTEGER,MPI_COMM_WORLD)
#define MPIUNPACKREALARR(ARG,SIZE) call MPI_Unpack(buffer,SIZEOFBUFFER,pos,ARG,SIZE,MPI_REAL,MPI_COMM_WORLD)
#define MPIUNPACKCHARARR(ARG,SIZE) call MPI_Unpack(buffer,SIZEOFBUFFER,pos,ARG,SIZE,MPI_CHARACTER,MPI_COMM_WORLD)  

#define ADDLENGTH(ARG)  length(ARG)=1; j1=j1+1
#define TYPELISTADDREAL typelist(j1)=MPI_REAL8;j1=j1+1
#define TYPELISTADDINTEGER typelist(j1)=MPI_INTEGER;j1=j1+1
#define TYPELISTADDCOMPLEX typelist(j1)=MPI_DOUBLE_COMPLEX;j1=j1+1
#define TYPELISTADDLOGICAL typelist(j1)=MPI_LOGICAL;j1=j1+1

#define SUBREADBUFFER(ARG) call subReadBuffer(ind,indPos,ARG)
#define COPYARRAY(ARG) call copyArray(ARG,listOfInds,initPos,indsPos)

#define SIZEOFDATABLOCK 200
#define NUMOFARRAYSINDB 15
#define SIZEOFBUFFER 50000
#define SIZEOFBUFFER2 4000

!--------------------------------------------
! MPI tools
! Copyright (C) 2016 Timo Väisänen and University of Helsinki
! All rights reserved.
! The new BSD License is applied to this software, see LICENSE.txt
!--------------------------------------------
module mpiTools
    use typeDefinitions
    use constants
    use mpi_f08
    implicit none
    character :: buffer(SIZEOFBUFFER)
    real(kind=rk) :: buffer2(SIZEOFBUFFER2)
    integer :: listOfInds(NUMOFARRAYSINDB)
    type(MPI_Datatype) :: newType
    private buffer,buffer2,fillTypeListAndLengths,fillBuffer2
    private newType,listOfInds,readBuffer,subReadBuffer
contains

    !--------------------------------------------
    !subroutine initializeMPI(rank,ierr,nodes)
    !The rank, error flag and the number of MPI processes
    !--------------------------------------------
    subroutine initializeMPI(rank,ierr,processes)
        integer, intent(out) :: ierr      !error flag
        integer, intent(out) :: rank      !rank(master must have always 0)
        integer, intent(out) :: processes !number of MPI processes
        call MPI_INIT(ierr)
        call MPI_COMM_SIZE(MPI_COMM_WORLD,processes,ierr)
        call MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    end subroutine


    !--------------------------------------------
    !subroutine sendDataDB(dB)
    !Broadcast dataBlock to every MPI process
    !--------------------------------------------
    subroutine sendDataDB(dB)
        type(dataBlock), intent(in) :: dB       !datablock
        integer :: lengths(SIZEOFDATABLOCK)
        type(MPI_Datatype) :: newType, typeList(SIZEOFDATABLOCK)
        integer(kind=MPI_ADDRESS_KIND) :: disp(SIZEOFDATABLOCK)  
        integer :: pos,ierr,nodes,j1,w4root
        w4root = 1
        call fillBuffer2(dB)
        call fillTypeListAndLengths(typeList,lengths)
        call fillDisplacements(dB,disp)
        call MPI_Type_create_struct(SIZEOFDATABLOCK,lengths,disp,typeList,newtype)
        call MPI_Type_commit(newType)
        pos = 0
        call MPI_Pack(MPI_BOTTOM, 1, newType, buffer, SIZEOFBUFFER, pos, MPI_COMM_WORLD,ierr)
        !call MPI_BCAST(w4Root,1,MPI_INTEGER,0,MPI_COMM_WORLD)
        call MPI_COMM_SIZE(MPI_COMM_WORLD, nodes, ierr)
        do j1=1,nodes-1
            call MPI_Send(buffer, pos, MPI_PACKED, j1, 123456, MPI_COMM_WORLD)
        enddo
    end subroutine


    !--------------------------------------------
    !subroutine receiveDataDB(dB)
    !Receive a package from the master process and unpack it.
    !--------------------------------------------
    subroutine receiveDataDB(dB)
        type(dataBlock), intent(inout) :: dB        !datablock
        integer :: pos,ierr,w4Root
        pos = 0
        !call MPI_BCAST(w4Root,1,MPI_INTEGER,0,MPI_COMM_WORLD)
        call MPI_Recv(buffer,SIZEOFBUFFER,MPI_PACKED,0,123456,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
        call openPackage(dB,pos)
        call readBuffer(dB)
    end subroutine


    !--------------------------------------------
    !subroutine openPackage(dB,pos0)
    !Unpack the package in the same order as it was packed
    !--------------------------------------------
    subroutine openPackage(dB,pos0)
        integer, intent(in) :: pos0             !position of the first variable
        type(dataBlock), intent(inout) :: dB    !dataBlock
        integer :: ierr,pos
        pos = pos0
!#MPITOOLS_SCRIPT001
        MPIUNPACKREAL(dB%wavel)
        MPIUNPACKREAL(dB%mre)
        MPIUNPACKREAL(dB%mim)
        MPIUNPACKREAL(dB%ssalbedo)
        MPIUNPACKREAL(dB%ell1)
        MPIUNPACKREAL(dB%Fstop)
        MPIUNPACKREAL(dB%the0)
        MPIUNPACKREAL(dB%phi0)
        MPIUNPACKREAL(dB%phiout)
        MPIUNPACKREAL(dB%tauc)
        MPIUNPACKREAL(dB%h)
        MPIUNPACKREAL(dB%mu0)
        MPIUNPACKREAL(dB%nu0)
        MPIUNPACKREAL(dB%cphi0)
        MPIUNPACKREAL(dB%sphi0)
        MPIUNPACKREAL(dB%tauh)
        MPIUNPACKREAL(dB%albedo)
        MPIUNPACKREAL(dB%mre12)
        MPIUNPACKREAL(dB%mre21)
        MPIUNPACKREAL(dB%waven)
        MPIUNPACKREAL(dB%xell)
        MPIUNPACKREAL(dB%dthe)
        MPIUNPACKREAL(dB%nre)
        MPIUNPACKREAL(dB%nim)
        MPIUNPACKREAL(dB%rho)
        MPIUNPACKREAL(dB%predeflambda)
        MPIUNPACKREAL(dB%appFBoundary)
        MPIUNPACKINTEGER(dB%radPoints)
        MPIUNPACKINTEGER(dB%seed)
        MPIUNPACKINTEGER(dB%nrn)
        MPIUNPACKINTEGER(dB%ntot)
        MPIUNPACKINTEGER(dB%nsca)
        MPIUNPACKINTEGER(dB%np)
        MPIUNPACKINTEGER(dB%tauflg)
        MPIUNPACKINTEGER(dB%ncm)
        MPIUNPACKINTEGER(dB%ns)
        MPIUNPACKINTEGER(dB%ntheb)
        MPIUNPACKINTEGER(dB%nphib)
        MPIUNPACKINTEGER(dB%nthe)
        MPIUNPACKINTEGER(dB%nphi)
        MPIUNPACKCMPLX(dB%m12)
        MPIUNPACKCMPLX(dB%m21)
!#MPITOOLS_SCRIPT002
        call MPI_Unpack(buffer,SIZEOFBUFFER,pos,listOfInds,NUMOFARRAYSINDB,MPI_INTEGER,MPI_COMM_WORLD)
        call MPI_Unpack(buffer,SIZEOFBUFFER,pos,buffer2,SIZEOFBUFFER2,MPI_REAL8,MPI_COMM_WORLD)
        call MPI_Unpack(buffer,SIZEOFBUFFER,pos,dB%scattererType,64,MPI_CHARACTER,MPI_COMM_WORLD)
    end subroutine


    !--------------------------------------------
    !subroutine subreadBuffer(ind,indPos,array)
    !Read arrays from the buffer
    !--------------------------------------------
    subroutine subreadBuffer(ind,indPos,array)
        integer, intent(inout) :: ind                           !position of the size data in listOfInds
        integer, intent(inout) :: indPos                        !position of the data in buffer
        real(kind=rk), allocatable, intent(inout) :: array(:)   !array to be filled
        integer :: j1
        if(listOfInds(ind)>0) then
            allocate(array(listOfInds(ind)))
            do j1=1,size(array)
                array(j1) = buffer2(indPos+j1)
            enddo
            indPos=indPos+size(array)
           
        endif
        ind=ind+1
    end subroutine



    !--------------------------------------------
    !subroutine fillTypeListAndLengths(typelist,lengths)
    !Fill information about types and size of the data
    !--------------------------------------------
    subroutine fillTypeListAndLengths(typelist,lengths)
        type(MPI_Datatype), intent(out) :: typelist(:)  !list of types
        integer, intent(out) :: lengths(:)              !list of lengths
        integer :: n,j1
        j1=1
!#MPITOOLS_SCRIPT101
        do j1=1,22
            typelist(j1)=MPI_REAL8
        end do
        do j1=23,34
            typelist(j1)=MPI_INTEGER
        end do
        do j1=35,36
            typelist(j1)=MPI_DOUBLE_COMPLEX
        end do

!#MPITOOLS_SCRIPT102
        typelist(j1) = MPI_INTEGER
        j1=j1+1
        typelist(j1) = MPI_REAL8
        j1=j1+1
        typelist(j1) = MPI_CHARACTER
        j1=0
!#MPITOOLS_SCRIPT201
        do j1=1,36
            lengths(j1)=1
        enddo
!#MPITOOLS_SCRIPT202
        j1=j1+1
        lengths(j1)=NUMOFARRAYSINDB
        j1=j1+1
        lengths(j1)=SIZEOFBUFFER2
        j1=j1+1
        lengths(j1)=64
    end subroutine



    !--------------------------------------------
    !subroutine fillDisplacements(dB,disp)
    !Get the location of the data 
    !--------------------------------------------
    subroutine fillDisplacements(dB,disp)
        type(dataBlock), intent(in) :: dB                           !datablock
        integer(kind=MPI_ADDRESS_KIND),intent(inout) :: disp(:)     !list of addresses
        integer :: j1
        j1=1
!#MPITOOLS_SCRIPT301
        MPIADDRESS(dB%wavel)
        MPIADDRESS(dB%mre)
        MPIADDRESS(dB%mim)
        MPIADDRESS(dB%ssalbedo)
        MPIADDRESS(dB%ell1)
        MPIADDRESS(dB%Fstop)
        MPIADDRESS(dB%the0)
        MPIADDRESS(dB%phi0)
        MPIADDRESS(dB%phiout)
        MPIADDRESS(dB%tauc)
        MPIADDRESS(dB%h)
        MPIADDRESS(dB%mu0)
        MPIADDRESS(dB%nu0)
        MPIADDRESS(dB%cphi0)
        MPIADDRESS(dB%sphi0)
        MPIADDRESS(dB%tauh)
        MPIADDRESS(dB%albedo)
        MPIADDRESS(dB%mre12)
        MPIADDRESS(dB%mre21)
        MPIADDRESS(dB%waven)
        MPIADDRESS(dB%xell)
        MPIADDRESS(dB%dthe)
        MPIADDRESS(dB%seed)
        MPIADDRESS(dB%nrn)
        MPIADDRESS(dB%ntot)
        MPIADDRESS(dB%nsca)
        MPIADDRESS(dB%np)
        MPIADDRESS(dB%tauflg)
        MPIADDRESS(dB%ncm)
        MPIADDRESS(dB%ns)
        MPIADDRESS(dB%ntheb)
        MPIADDRESS(dB%nphib)
        MPIADDRESS(dB%nthe)
        MPIADDRESS(dB%nphi)
        MPIADDRESS(dB%m12)
        MPIADDRESS(dB%m21)
!#MPITOOLS_SCRIPT302
        MPIADDRESS(listOfInds) 
        MPIADDRESS(buffer2)
        MPIADDRESS(dB%scattererType)
    end subroutine



    !--------------------------------------------
    !subroutine readBuffer(dB)
    !Read arrays from the buffer
    !--------------------------------------------
    subroutine readBuffer(dB)
        type(dataBlock), intent(inout) :: dB        !dataBlock
        integer :: ind,indPos
        ind=1
        indPos=0
!#MPITOOLS_SCRIPT401
        call subReadBuffer(ind,indPos,dB%CTHEIF)
        call subReadBuffer(ind,indPos,dB%STHEIF)
        call subReadBuffer(ind,indPos,dB%INORM)
        call subReadBuffer(ind,indPos,dB%STHEI)
        call subReadBuffer(ind,indPos,dB%CTHEI)
        call subReadBuffer(ind,indPos,dB%CPHII)
        call subReadBuffer(ind,indPos,dB%SPHII)
        call subReadBuffer(ind,indPos,dB%THEBF)
        call subReadBuffer(ind,indPos,dB%CTHEBF)
        call subReadBuffer(ind,indPos,dB%STHEBF)
        call subReadBuffer(ind,indPos,dB%STHEB)
        call subReadBuffer(ind,indPos,dB%CTHEB)
        call subReadBuffer(ind,indPos,dB%CPHIB)
        call subReadBuffer(ind,indPos,dB%SPHIB)
        call subReadBuffer(ind,indPos,dB%PHIB)
!#MPITOOLS_SCRIPT402
    end subroutine




    !--------------------------------------------
    !subroutine fillBuffer2(dB)
    !Fill buffer with arrays
    !--------------------------------------------
    subroutine fillBuffer2(dB)
        type(dataBlock), intent(in) :: dB       !dataBlock
        integer :: n,j1,indsPos,initPos
        initPos = 0
        indsPos = 1
!#MPITOOLS_SCRIPT501
        call copyArray(dB%CTHEIF,listOfInds,initPos,indsPos)
        call copyArray(dB%STHEIF,listOfInds,initPos,indsPos)
        call copyArray(dB%INORM,listOfInds,initPos,indsPos)
        call copyArray(dB%STHEI,listOfInds,initPos,indsPos)
        call copyArray(dB%CTHEI,listOfInds,initPos,indsPos)
        call copyArray(dB%CPHII,listOfInds,initPos,indsPos)
        call copyArray(dB%SPHII,listOfInds,initPos,indsPos)
        call copyArray(dB%THEBF,listOfInds,initPos,indsPos)
        call copyArray(dB%CTHEBF,listOfInds,initPos,indsPos)
        call copyArray(dB%STHEBF,listOfInds,initPos,indsPos)
        call copyArray(dB%STHEB,listOfInds,initPos,indsPos)
        call copyArray(dB%CTHEB,listOfInds,initPos,indsPos)
        call copyArray(dB%CPHIB,listOfInds,initPos,indsPos)
        call copyArray(dB%SPHIB,listOfInds,initPos,indsPos)
        call copyArray(dB%PHIB,listOfInds,initPos,indsPos)
!#MPITOOLS_SCRIPT502
    end subroutine



    !--------------------------------------------
    !subroutine copyArray(array,listOfInds,initPos,indsPos)
    !Copies array to buffer and fills data needed for 
    !unpacking the them. 
    !--------------------------------------------
    subroutine copyArray(array,listOfInds,initPos,indsPos)
        real(kind=rk), allocatable,intent(in) :: array(:)   !array
        integer, intent(inout) :: listOfInds(:)             !list of sizes
        integer, intent(inout) :: initPos                   !position in buffer
        integer, intent(inout) :: indsPos                   !position in listOfInds
        integer :: n,j1
        if(allocated(array)) then
            do j1=1,size(array)
                buffer2(initPos+j1)=array(j1)
            enddo
            initPos=initPos+size(array)
            listOfInds(indsPos) = size(array)
        else
            listOfInds(indsPos) = 0
        endif
        indsPos=indsPos+1
    end subroutine





#define sizeOfBufferMerge 80000


    !--------------------------------------------
    !subroutine mergeAssembledData(aD)
    !Merge assembledData from different MPI processes
    !--------------------------------------------
    subroutine mergeAssembledData(aD)
        type(assembledData), intent(inout) :: aD        !assembledData
        real(kind=rk) :: buffer(sizeOfBufferMerge)
        integer :: listOfInds(9),ierr
        integer :: rank,j1,j2,j3,j4,j5,x,nodes,finished
        type(MPI_STATUS) :: status0
        type(MPI_REQUEST) :: request
        request = MPI_REQUEST_NULL
        call MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
        listOfInds=(/size(aD%MRT,dim=1),size(aD%MRT,dim=2),size(aD%MRT,dim=3),size(aD%MRT,dim=4),&
                size(aD%MBS,dim=1),size(aD%MBS,dim=2),size(aD%MBS,dim=3),size(aD%MBS,dim=4),size(aD%MBS,dim=5)/)
        if(rank==0) then
            finished=0
            call MPI_COMM_SIZE(MPI_COMM_WORLD,nodes,ierr)
            do while(finished<nodes-1)
                call MPI_Irecv(buffer,sizeOfBufferMerge,MPI_REAL8,MPI_ANY_SOURCE,0, MPI_COMM_WORLD,request)               
                if(request/=MPI_REQUEST_NULL) then
                    call MPI_WAIT(request,status0)
                    x=1
                    do j4=1,listOfInds(4)
                        do j3=1,listOfInds(3)
                            do j2=1,listOfInds(2)
                                do j1=1,listOfInds(1)
                                    aD%MRT(j1,j2,j3,j4)=aD%MRT(j1,j2,j3,j4)+buffer(x)
                                    x=x+1
                                enddo
                            enddo
                        enddo
                    enddo
                    do j5=1,listOfInds(9)
                        do j4=1,listOfInds(8)
                            do j3=1,listOfInds(7)
                                do j2=1,listOfInds(6)
                                    do j1=1,listOfInds(5)
                                        aD%MBS(j1,j2,j3,j4,j5)=aD%MBS(j1,j2,j3,j4,j5)+buffer(x)
                                        x=x+1
                                    enddo
                                enddo
                            enddo
                        enddo
                    enddo
                    aD%Aref=buffer(x)+aD%Aref
                    x=x+1
                    aD%Aspec=buffer(x)+aD%Aspec
                    x=x+1
                    aD%Aabs=buffer(x)+aD%Aabs
                    x=x+1
                    aD%Astop=buffer(x)+aD%Astop
                    x=x+1
                    aD%Adt=buffer(x)+aD%Adt
                    x=x+1
                    aD%Atra=buffer(x)+aD%Atra 
                    finished=finished+1
                else
                    write(6,*) "waiting for job"
                endif
       
            enddo
        else
            x=1
            do j4=1,listOfInds(4)
                do j3=1,listOfInds(3)
                    do j2=1,listOfInds(2)
                        do j1=1,listOfInds(1)
                            buffer(x) = aD%MRT(j1,j2,j3,j4)
                            x=x+1
                        enddo
                    enddo
                enddo
            enddo
            do j5=1,listOfInds(9)
                do j4=1,listOfInds(8)
                    do j3=1,listOfInds(7)
                        do j2=1,listOfInds(6)
                            do j1=1,listOfInds(5)
                                buffer(x) = aD%MBS(j1,j2,j3,j4,j5)
                                x=x+1
                            enddo
                        enddo
                    enddo
                enddo
            enddo
            buffer(x)=aD%Aref
            x=x+1
            buffer(x)=aD%Aspec
            x=x+1
            buffer(x)=aD%Aabs
            x=x+1
            buffer(x)=aD%Astop
            x=x+1
            buffer(x)=aD%Adt
            x=x+1
            buffer(x)=aD%Atra
            call MPI_SEND(buffer,sizeOfBufferMerge,MPI_REAL8,0,0,MPI_COMM_WORLD)     
        endif
    end subroutine

    !--------------------------------------------
    !subroutine subreadBuffer(ind,indPos,array)
    !Read arrays from the buffer
    !--------------------------------------------
    subroutine finishMPISession()
        integer :: ierr
        call MPI_FINALIZE(ierr)
    end subroutine

    !--------------------------------------------
    !subroutine ABORT(msg)
    !Make clean MPI Abort
    !--------------------------------------------
    subroutine ABORT(msg)
        integer :: ierr,errorcode
        character(*), intent(in) :: msg             !output message
        write(6,*) trim(msg)
        call MPI_ABORT(MPI_COMM_WORLD, errorcode, ierr)  
        stop
    end subroutine

    !--------------------------------------------
    !subroutine subreadBuffer(ind,indPos,array)
    !broadcast openmp thread count
    !--------------------------------------------
    subroutine bcastThreadCount(threadCount)
        integer, intent(inout) :: threadCount       !number of openmp threads
        call MPI_BCAST(threadCount,1,MPI_INTEGER,0,MPI_COMM_WORLD)
    end subroutine


end module



