!----------------------------------------------------
!Scatterer control
!Copyright (C) 2016 Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!----------------------------------------------------
module scattererModule
    use constants
    use typedefinitions
    use rayleighScatterer
    use errorHandler
    use splinePresentation 
    
    implicit none
    abstract interface
        !abstract for subroutine which prepare uses
        subroutine prepScatterer(dataB)
            import :: dataBlock
            type(dataBlock), intent(inout) :: dataB
        end subroutine

        !abstract for subroutine which is used elsewhere
        subroutine scattererF1(val)
            import :: rk
            real(kind=rk), intent(out) :: val
        end subroutine


        !abstract for subroutine which is used elsewhere
        pure subroutine scattererF2(P,val)
            import :: rk
            real(kind=rk), intent(out) :: P(4,4)
            real(kind=rk), intent(in) :: val
        end subroutine

        !abstract for subroutine which is used elsewhere
        pure subroutine scattererF3(S,val)
            import :: rk
            complex(kind=rk), intent(out) :: S(2)
            real(kind=rk), intent(in) :: val
        end subroutine

        subroutine printData(dB,stream)
            import :: dataBlock
            type(dataBlock), intent(in) :: dB
            integer, intent(in) :: stream
        end subroutine

        !-------------------------------------------------
        !MPI VERSION ADDITIONS
        !abstract for subroutine which is used elsewhere
        !-------------------------------------------------
        subroutine MPIfunc()
        end subroutine
    end interface

    !-------------------------------------------------
    !pointers which are used to access the scatterer
    !-------------------------------------------------
    procedure (prepScatterer), pointer :: prepareScatterers => null()
    procedure (scattererF1), pointer :: scattererFunc1 => null()
    procedure (scattererF2), pointer :: scattererFunc2 => null()
    procedure (scattererF3), pointer :: scattererSElements => null()
    procedure (printData), pointer :: printScattererData =>null()
    procedure (MPIfunc), pointer :: sendMPIData=> null()
    procedure (MPIfunc), pointer :: rcvMPIData => null()
contains

    !-------------------------------------------------
    !subroutine selectScatterer(dB)
    !selectScatterer according to input. Makes function pointers
    !to point right functions. In a case of a wrong input, 
    !add an error.
    !-------------------------------------------------
    subroutine selectScatterer(dB)
        type(dataBlock), intent(in) :: dB
        if(adjustl(trim(dB%scattererType))=="rayleigh") then
            prepareScatterers => prepareRayleigh
            scattererFunc1 => rayleighSca1
            scattererFunc2 => rayleighSca2
            scattererSElements => rayleighSElements
            printScattererData => rayleighPrintData
            sendMPIData => rayleighMPISend
            rcvMPIData => rayleighMPIrcv
        else if(adjustl(trim(dB%scattererType))=="spline") then
            prepareScatterers => prepareSpline
            scattererFunc1 => splineSca1
            scattererFunc2 => splineSca2
            scattererSElements => splineSElements
            printScattererData=>splinePrintData
            sendMPIData => splineMPISend
            rcvMPIData => splineMPIrcv
        else 
            call addError("Scatterer type not defined: "//dB%scattererType)
        endif
    end subroutine




end module
