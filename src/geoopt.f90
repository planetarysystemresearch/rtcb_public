!----------------------------------------------------
!RTCB
!Geometric Optics
!Copyright (C) 2003,2015 Karri Muinonen
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt

!Geometric optics:
!
!REFREF:    specular reflection and refraction 
!MIRROR:    mirror reflection law 
!SNEL:      Snel's law of refraction 
!FRESNEL:   Fresnel coefficients for reflection and refraction 
!MREFREF:   Mueller matrix for reflection and refraction 
module geoopt
    use constants
    use mathRoutines
    implicit none
    private :: MREFREF,FRESNEL,SNEL,MIRROR
contains



    !For an incident vector of Stokes parameters and ray coordinate system 
    !(no subscript), computes the reflected (subscript 1) and refracted 
    !(subscript 00) vectors of Stokes parameters and the corresponding
    !ray coordinate systems. 
    subroutine REFREF(I1,K1,ER1,EL1,I2,K2,ER2,EL2,&
        &             I,K,ER,EL,N,m,mre,kn,flg)
        real(kind=rk), intent(out) :: I1(4),K1(3),ER1(3),EL1(3) !reflected ray
        real(kind=rk), intent(out) :: I2(4),K2(3),ER2(3),EL2(3) !refracted ray
        real(kind=rk), intent(in) :: I(4),K(3),ER(3),EL(3)      !incident ray
        real(kind=rk), intent(in) :: N(3)                  !vector parallel to plane-parallel medium
        real(kind=rk), intent(in) :: kn                    !dot product of N and K
        logical, intent(out) :: flg                        !total reflection happened
        complex(kind=rk), intent(in) :: m                  !refractive index
        real(kind=rk), intent(in) :: mre                   !real refractive index
        real(kind=rk) :: T1(3),T2(3),MM(4,4)
        real(kind=rk) :: ci,si,ct,st,cpsi,spsi,c2psi,s2psi
        real(kind=rk) :: Itmp(4)
        real(kind=rk), parameter :: tol = -0.99999999_rk    
        integer :: ind

        complex(kind=rk) ::  rr,rl,tr,tl

        !Initial setting:
        
        I1(:)=I(:)
        Itmp=I
        K1(:)=K(:)
        ER1(:)=ER(:)
        EL1(:)=EL(:)

        !Tangent vectors perpendicular (T1) and parallel (T2)
        !to the plane of incidence:

        if (kn>tol) then
            ci=-kn
            si=sqrt(1.0_rk-kn**2)
            T2(:)=(K1(:)-kn*N(:))/si
        else
            T2(:)=EL1(:)
            ci=1.0_rk
            si=0.0_rk
        endif
        T1 = crossProduct(T2,N)

        !Rotation of Stokes parameters to the plane of incidence:
        cpsi = dot_product(ER1,T1)
        spsi = dot_product(EL1,T1)
        spsi=-spsi
        c2psi=cpsi**2-spsi**2
        s2psi=2.0_rk*spsi*cpsi
        call getRotationMatrix(MM,c2psi,s2psi) 
        Itmp=matXVec(MM,I1)

        !New direction vectors for the reflected ray:
        call MIRROR(K1,ER1,EL1,T1,T2,N,ci,si)

        !Reflection and refraction:
        if (si<mre) then
            call SNEL(K2,ER2,EL2,T1,T2,N,si,mre)
            call FRESNEL(rr,rl,tr,tl,m,ci)
            call MREFREF(MM,rr,rl)
            I1=matXvec(MM,Itmp)
                    
            tr=tr*sqrt(abs(real(m*sqrt(1.0_rk-(1.0_rk-ci**2)/m**2),kind=rk)/ci))
            tl=tl*sqrt(abs(real(conjg(m)*sqrt(1.0_rk-(1.0_rk-ci**2)/m**2),kind=rk)/ci))
            call MREFREF(MM,tr,tl)
            I2=matXVec(MM,Itmp)
            flg=.false.
        !Total reflection:
        else
            st=si/mre
            ct=sqrt(st**2-1.0_rk)
            rr=cmplx(ci,-mre*ct,kind=rk)/cmplx(ci,mre*ct,kind=rk)
            rl=cmplx(mre*ci,-ct,kind=rk)/cmplx(mre*ci,ct,kind=rk)
            call MREFREF(MM,rr,rl)
            I1=matXVec(MM,Itmp)
            flg=.true.
        endif
    end subroutine


    !Mirror law of reflection. Version 2003-11-14.en
    pure subroutine MIRROR(K,ER,EL,T1,T2,N,ci,si)
        integer i
        real(kind=rk), intent(out) :: K(3),ER(3),EL(3)
        real(kind=rk), intent(in) :: T1(3),T2(3),N(3),ci,si
        K(:)=si*T2(:)+ci*N(:)
        ER(:)=T1(:)
        EL(:)=ci*T2(:)-si*N(:)
    end subroutine


    !Snel's law of refraction for real refractive index. Version 2003-11-14.
    !Copyright (C) 2003 Karri Muinonen
    pure subroutine SNEL(K,ER,EL,T1,T2,N,si,mre)
        real(kind=rk), intent(in) :: si,mre
        real(kind=rk), intent(out) :: K(3),ER(3),EL(3)
        real(kind=rk), intent(in) :: T1(3),T2(3),N(3)
        real(Kind=rk) :: st,ct
        integer :: i
        st=si/mre
        ct=sqrt(1.0_rk-st**2)        
        K(:)=st*T2(:)-ct*N(:)
        ER(:)=T1(:)
        EL(:)=-ct*T2(:)-st*N(:)
    end subroutine

    !Fresnel reflection and refraction coefficients. Version 2003-11-14.    
    pure subroutine FRESNEL(rr,rl,tr,tl,m,ci)
        real(kind=rk), intent(in) :: ci
        complex(kind=rk), intent(in) :: m
        complex(kind=rk),intent(out) :: rr,rl,tr,tl
        complex(kind=rk) :: q1,q2
        q1=m**2
        q2=sqrt(q1-1.0_rk+ci**2)
        rr=(ci-q2)/(ci+q2)
        rl=(ci*q1-q2)/(ci*q1+q2)
        tr=2.0_rk*ci/(ci+q2)
        tl=2.0_rk*m*ci/(ci*q1+q2)
    end subroutine

    !Mueller matrix for Fresnel reflection and refraction. Version 2003-11-14.
    pure subroutine MREFREF(M,r,l)
        real(kind=rk),intent(out) :: M(4,4)
        complex(kind=rk), intent(in) :: r,l
        M=0.0_rk
        M(1,1)=0.5_rk*(abs(l)**2+abs(r)**2)
        M(2,2)=M(1,1)
        M(1,2)=0.5_rk*(abs(l)**2-abs(r)**2)
        M(2,1)=M(1,2)
        M(3,3)=real(l*conjg(r),kind=rk)
        M(3,4)=aimag(l*conjg(r))
        M(4,3)=-M(3,4)
        M(4,4)=M(3,3)
    end subroutine

end module

