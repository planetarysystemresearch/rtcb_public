!----------------------------------------------------
!Tools for handling splines
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen, and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
module splinetools
    use constants
    implicit none
    !private cubicspline,bisectionSearch
contains

    !---------------------------------------------
    !initPresentation(xp,coeffs,x,y)
    !Fills xp and coeffs usign data obtained from x and y
    !
    !IN:    x: data for x-axis
    !       y(a,b): the data for f, where a is the angle
    !              and b is the number of elements
    !INOUT  xp: data for x-axis
    !   coeffs: coefficients for multiple splines
    !---------------------------------------------
    subroutine initPresentation(xp,coeffs,x,y)
        real(kind=rk),allocatable,intent(inout) :: xp(:)            !points in x-axis
        real(kind=rk),allocatable,intent(inout) :: coeffs(:,:,:)    !coefficients
        real(kind=rk), intent(in) :: x(:)                           !points in x-axis in
        real(kind=rk), intent(in) :: y(:,:)                         !points in y-axis
        real(kind=rk) :: val
        real(kind=rk) :: tmpP,tmpVal
        integer :: n,m,j1,ind
        n=size(y,dim=2)
        m=size(y,dim=1)
        if(.not. allocated(xp)) then
            allocate(xp(n))            
            xp=x
        endif

        if(.not. allocated(coeffs)) then
            allocate(coeffs(4,m,n))  
        endif
              
        !Store d_k
        do j1=1,n
            coeffs(4,:,j1)=y(:,j1)
        enddo

        call cubicspline(xp,coeffs,0.0_rk,0.0_rk)
    end subroutine

    !---------------------------------------------
    !cubicspline(xp,coeffs,dfa,dfb)
    !Creates multiple clamped cubic spline presentations
    !to coeffs(1:3,:,1:np) from coeffs(4,:,1:np)
    !and xp(1:np).
    !
    !IN:    xp: x-axis data
    !      dfa: 1st derivative at f(x_0)
    !      dfb: 1st derivative at f(x_n)
    !INOUT: coeffs(): spline's coefficients
    !---------------------------------------------
    subroutine cubicspline(xp,coeffs,dfa,dfb)
        real(kind=rk),intent(in) :: xp(:)            !x-axis data
        real(kind=rk),intent(inout) :: coeffs(:,:,:)   !coeffs, coeffs(4,:,:) should contain y-axis data
        real(kind=rk), intent(in) :: dfa,dfb      !derivatives at the end points                                
        real(kind=rk),allocatable :: h(:),a(:),aa(:),bb(:),cc(:),x(:)
        integer :: n,j1,j2
        n = size(xp)
        allocate(h(n),a(n),aa(n),bb(n),cc(n),x(n))
        do j1=1,n-1
            h(j1)=xp(j1+1)-xp(j1)
        enddo
        do j2=1,size(coeffs,dim=2)
            a(1)=3.0_rk*(coeffs(4,j2,2)-coeffs(4,j2,1))/h(1)-3.0_rk*dfa
            a(n)=3.0_rk*dfb-3.0_rk*(coeffs(4,j2,n)-coeffs(4,j2,n-1))/h(n-1)

            do j1=2,n-1       
                a(j1)=3.0_rk*(coeffs(4,j2,j1+1)-coeffs(4,j2,j1))/h(j1)&
                &   -3.0_rk*(coeffs(4,j2,j1)-coeffs(4,j2,j1-1))/h(j1-1)
            enddo

            do j1=1,n-1
                aa(j1)=h(j1)
                cc(j1)=h(j1)
            enddo

            bb(1) = 2.0_rk*h(1)
            do j1=2,n-1
                bb(j1)=2.0_rk*(h(j1)+h(j1-1))
            enddo
            bb(n) = 2.0_rk*h(n-1)
            call thomasAlgorithm(x,aa,bb,cc,a)
            do j1=1,n-1
               
                coeffs(1,j2,j1)=(x(j1+1)-x(j1))/(3.0_rk*h(j1))
                coeffs(2,j2,j1)=x(j1)
                coeffs(3,j2,j1)=(coeffs(4,j2,j1+1)-coeffs(4,j2,j1))/h(j1)-&
                    & (2.0_rk*x(j1)+x(j1+1))*h(j1)/3.0_rk 
            enddo
        enddo
        deallocate(h,a,aa,bb,cc)
    end subroutine

    !---------------------------------------------
    !interpolate(tmpP,xp,coeffs,the)
    !Find tmpP=f(the) where f is expressed as a spline
    !All the splines are evaluated
    !
    !IN:    the:    =x in f(x)
    !    coeffs:    spline coefficients
    !        xp:    x-axis data
    !OUT:  tmpP:    multiple f(the) 
    !
    !---------------------------------------------
    pure subroutine interpolate(tmpP,xp,coeffs,the)
        real(kind=rk), intent(out) :: tmpP(:)         !interpolated values
        real(kind=rk), intent(in) :: xp(:)            !x-axis data
        real(kind=rk), intent(in) :: coeffs(:,:,:)    !spline coefficients
        real(kind=rk), intent(in) :: the              !point where spline is evaluated
        real(kind=rk) :: tmpX
        integer :: ind
        if(the<xp(size(xp))) then          
            !ind = bisectionSearch(xp,the)
            ind=1+int(the*(size(xp)-1)/pi,kind=rk)
            tmpX = the-xp(ind)      
            
        else
            ind=size(coeffs,dim=3)
            tmpP(:)=coeffs(4,:,ind)
            return
        endif
        tmpP(:)=coeffs(1,:,ind)*tmpX**3+coeffs(2,:,ind)*tmpX**2+   &
            & coeffs(3,:,ind)*tmpX+coeffs(4,:,ind)
    end subroutine
    
    !---------------------------------------------
    !interpolate2(tmpP,xp,coeffs,the)
    !Find tmpP=f(the) where f is expressed as a spline
    !Only the first spline is evaluated
    !
    !IN:    the:    =x in f(x)
    !    coeffs:    spline coefficients
    !        xp:    x-axis data
    !OUT:  tmpP:    =f(the)
    !---------------------------------------------
    pure subroutine interpolate2(tmpP,xp,coeffs,the)
        real(kind=rk), intent(out) :: tmpP            !interpolated values
        real(kind=rk), intent(in) :: xp(:)            !x-axis data
        real(kind=rk), intent(in) :: coeffs(:,:,:)    !spline coefficients
        real(kind=rk), intent(in) :: the              !point where spline is evaluated
        real(kind=rk) :: tmpX
        integer :: ind
        
        if(the<xp(size(xp))) then
            ind=1+int(the*(size(xp)-1)/pi,kind=rk)
            !ind = bisectionSearch(xp,the)
            tmpX = the-xp(ind) 
        else
            ind=size(coeffs,dim=3)
            tmpP=coeffs(4,1,ind)
            return
        endif
        tmpP=coeffs(1,1,ind)*tmpX**3+coeffs(2,1,ind)*tmpX**2+   &
            & coeffs(3,1,ind)*tmpX+coeffs(4,1,ind)
    end subroutine

    !---------------------------------------------
    !bisectionSearch(xp,the)
    !Search index for the value(lower) closest to
    !the value "the" using bisection search.
    !
    !IN: xp:    data   
    !   the:    the value to look for
    !---------------------------------------------
    pure function bisectionSearch(xp,the) result(retVal)
        real(kind=rk), intent(in) :: the         !point to be evaluated
        real(kind=rk), intent(in) :: xp(:)       !sorted array
        real(kind=rk) :: errCheck
        integer :: retVal
        integer :: x1,x2,xmid 
        x1=1
        x2=size(xp)
        do while(x2-x1>1)
            xmid=(x1+x2)/2
            if(xp(xmid) > the) then
                x2=xmid
            else
                x1=xmid
            endif
        enddo
        retVal=x1
    end function
        
    
    !---------------------------------------------
    !thomasAlgorithm(x,a0,b0,c0,d0)
    !solves tridiagonal matrix
    !
    !IN: a0,b0,c0:  diagonal vectors
    !          d0:  right side column vector
    !OUT:       x:  result
    !--------------------------------------------
    subroutine thomasAlgorithm(x,a0,b0,c0,d0)
        real(kind=rk), intent(in) :: a0(:),b0(:),c0(:),d0(:)
        real(kind=rk), intent(out) :: x(:)
        real(kind=rk), allocatable :: c(:),d(:)
        integer :: n,j1
        n = size(d0)
        allocate(c(n),d(n))
        c(1) = c0(1)/b0(1)
        do j1=2,n-1
            c(j1)=c0(j1)/(b0(j1)-a0(j1-1)*c(j1-1))
        enddo
        d(1)=d0(1)/b0(1)
        do j1=2,n
            d(j1)=(d0(j1)-a0(j1-1)*d(j1-1))/(b0(j1)-a0(j1-1)*c(j1-1))
        enddo
        x(n)=d(n)
        do j1=n-1,1,-1
            x(j1)=d(j1)-c(j1)*x(j1+1)
        enddo
        deallocate(c,d)
    end subroutine

end module
