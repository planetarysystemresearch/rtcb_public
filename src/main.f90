!-----------------------------
!RT-CB
!The main program
!
!Copyright (C) 2016 Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!-----------------------------

program rtcb
    use inputReader
    use typedefinitions
    use errorHandler
    use preparedata
    use mcSolution
    use output
    use mpiTools
    use omp_lib
    implicit none
    type(dataBlock) :: dB
    type(assembledData) :: aD

    integer :: ierr,cores,rank,id
    integer :: myLoad,ompThreads,tid,mtid,MPIThreads
    character(len=32) :: arg
    real(kind=rk) :: sTime,fTime,frac

    !Initialize MPI
    call initializeMPI(id,ierr,MPIThreads)
    
    !master process initializes, other processes wait
    if(id==0) then
        write(6,*) "RT-CB"
        write(6,*) "START initialization"  
        !get the number of openmp threads. 
        call get_command_argument(1,arg)
        if(len_trim(arg) == 0) then
            ompThreads=1
        else
            read(arg,*) ompThreads
        endif

        call readInput(dB)
        if(checkIfErrors()) call Abort("Abort: readInput")
        call prepare(dB)  
        if(checkIfErrors()) call Abort("Abort: prepare")
        call sendDataDB(dB) 
    else
        call receiveDataDB(dB)
    endif

    !broadcast omp_thread count
    call bcastThreadCount(ompThreads)
    call omp_set_num_threads(ompThreads)

    !make final preparations
    call prepare2(dB,id)
    if(checkIfErrors()) call Abort("Abort: prepare2")

    !initialize the array in which the data is stored
    call initAssembledData(dB,aD) 

    !Initialize random number generators
    !$omp parallel private(tid,mtid)
    tid = omp_get_thread_num()
    mtid = omp_get_max_threads()
    call init_rng(dB%seed,id*mtid+tid)
    !write(6,*) "mpi_thread, openmp_thread, seed:", id,tid,dB%seed+id*mtid+tid
    !$omp end parallel
    
    !print info screen
    if(id==0) call writeInfoScreen(dB,MPIThreads,OMPthreads,6)

    !estimate time if asked
    if(id==0 .and. dB%estimateTime) then
        write(6,*) "Estimate time..."
        sTime  = OMP_get_wtime() 
        call estimateTime(dB,id,MPIThreads,frac)
        fTime  = OMP_get_wtime()
        write(6,*) "Time estimate: ", (fTime-sTime)*frac/(60.0_rk),"minutes"
        write(6,*) "//////////////////////////////////////////////////"    
    endif 
        
    !start algorithm and keep track of time usage
    if(id==0) sTime  = OMP_get_wtime()
    call start(dB,aD,id,MPIThreads)
    !collect data
    call mergeAssembledData(aD)
    if(id==0) fTime  = OMP_get_wtime()
    if(id==0) write(6,*) "Time: ", (fTime-sTime)/60.0_rk

    !print output
    if(id==0) call printOutput(aD,dB)

    !check if there were any errors
    if(checkIfErrors()) call Abort("Abort: main algorithm. Output was saved anyway")

    !make clean finish
    call finishMPISession()
    stop

end program
