!----------------------------------------------------
!Functions and subroutines for rayleigh scatterers.
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!----------------------------------------------------
module rayleighScatterer
    use constants
    use typedefinitions
    use rng    
    implicit none
contains

    !----------------------------------------------------
    !subroutine prepareRayleigh(dataB)
    !Not much to do
    !----------------------------------------------------
    subroutine prepareRayleigh(dataB)
        type(dataBlock), intent(inout) :: dataB     !dataBlock
    end subroutine


    !----------------------------------------------------
    !subroutine rayleighSca1(cthe)
    !Generate scattering angle from Rayleigh scatterer
    !----------------------------------------------------
    subroutine rayleighSca1(cthe)
        real(kind=rk), intent(out) :: cthe      !cos(theta) scattering angle
        real(kind=rk) :: rn
        rn = 2.0_rk*(1.0_rk-2.0_rk*randNum())
        cthe = (sqrt(1.0_rk+rn**2)+rn)**(1.0_rk/3.0_rk)&
        & -(sqrt(1.0_rk+rn**2)-rn)**(1.0_rk/3.0_rk)
    end subroutine



    !----------------------------------------------------
    !pure subroutine rayleighSca2(P,cthe)
    !Generate Mueller matrix corresponding to scattering into the angle cos(theta)
    !----------------------------------------------------
    pure subroutine rayleighSca2(P,cthe)
        real(kind=rk), intent(out) :: P(4,4)    !Scattering matrix
        real(kind=rk), intent(in) :: cthe       !cos(theta)
        real(Kind=rk) :: rn
        P=0.0_rk
        P(1,1)=0.75_rk*(1.0_rk+cthe**2)
        P(2,2)=P(1,1)
        P(1,2)=-0.75_rk*(1.0_rk-cthe**2)
        P(2,1)=P(1,2)
        P(3,3)=1.5_rk*cthe
        P(4,4)=P(3,3)
    end subroutine



    !----------------------------------------------------
    !pure subroutine rayleighSElements(S,cthe)
    !Generate amplitude matrix elements corresponding to scattering into the angle cos(theta)
    !----------------------------------------------------
    pure subroutine rayleighSElements(S,cthe)
        complex(kind=rk), intent(out) :: S(2)       !Jones matrix
        real(kind=rk), intent(in) :: cthe           !cos(theta)
        S(1)=cmplx(0.0_rk,1.0_rk,kind=rk)
        S(2)=cmplx(0.0_rk,cthe,kind=rk)
    end subroutine


    subroutine rayleighPrintData(dB,stream)
        type(dataBlock), intent(in) :: dB
        integer, intent(in) :: stream
    end subroutine

    subroutine rayleighMPISend()
    end subroutine

    subroutine rayleighMPIrcv()
    end subroutine

end module
