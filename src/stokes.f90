!----------------------------------------------------------
!RT-CB
!Module to handle scattering into the given direction.
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!
!
!subroutine updateStokes(I2,K2,EH2,EV2,I1,mu1,nu1,cphi1,sphi1)
!pure subroutine setEHEVK(EH,EV,K)
!-----------------------------------------------------------

module stokes
    use constants
    use scattererModule
    use mathroutines
    implicit none
contains


    !---------------------------------------------------
    !subroutine updateStokes(I2,K2,EH2,EV2,I1,mu1,nu1,cphi1,sphi1)
    !
    !Scatter the Stoke's vector I1 into the direction K2. Also
    !provides the electric field components in a fixed scattering plane.
    !---------------------------------------------------
    subroutine updateStokes(I2,K2,EH2,EV2,I1,mu1,nu1,cphi1,sphi1)
        real(kind=rk), intent(in) :: I1(4)                  !Incident Stoke's vector
        real(kind=rk), intent(in) :: mu1,nu1,cphi1,sphi1    !incident direction in the normal coordinate system
        real(kind=rk), intent(in) :: K2(3)                  !scattering direction in the normal coordinate system
        real(kind=rk), intent(out) :: I2(4)                 !outgoing Stoke's vector
        real(kind=rk), intent(out) :: EH2(3),EV2(3)         !electric field horizontal&vertical components
    
        !temporary values
        real(kind=rk) :: nuk,muk,r0k,phik,cphik,sphik
        real(kind=rk) :: c2psi,cpsi,spsi,s2psi,aaaa
        real(kind=rk) :: EHk(3),EVk(3)
        real(kind=rk) :: Kk(3),R(4,4),Ik(4),Itmp(4)

        !Find emergent direction in the incident ray coordinate system from 
        !the direction in the normal coordinate system:       
        Kk=K2    
        call normToRay(Kk,mu1,nu1,cphi1,sphi1)
        !Express incident Stokes vector with respect to the temporary scattering 
        !plane:
        call getSphericalCoordinates2(Kk,r0k,muk,phik)
        !muk can sometimes be slightly over 1, probably due to numerical errors.
        !causes invalid sqrt-operation, so we will check the value beforehand
        if(muk >= 1.0_rk) then
           nuk = 0.0_rk
           muk = 1.0_rk
        else if(muk <= -1.0_rk) then
           nuk = 0.0_rk
           muk = -1.0_rk
        else
           nuk=sqrt(1.0_rk-muk**2)
        end if

        cphik = cos(phik)
        sphik = sin(phik)

        EHk(1)=sphik
        EHk(2)=-cphik
        EHk(3)=0.0_rk
        EVk(1)=muk*cphik
        EVk(2)=muk*sphik
        EVk(3)=-nuk
        !call updateRotationMatrix(R,EHk(2)**2-&
        !    & EHk(1)**2,2.0_rk*EHk(1)*EHk(2))
        !call matXvec(Ik,R,I1)

        Ik=combinedRotationFunc(I1,EHk(2)**2-EHk(1)**2,2.0_rk*EHk(1)*EHk(2))
        
        !Multiply incident Stokes vector by the scattering phase matrix
        call scattererFunc2(R,Kk(3))
        I2 = matXvec(R,Ik)

        !Express emergent Stokes vector in the normal coordinate system 
        !with respect to the fixed scattering plane (compute basis vectors
        !EH2 and EV2):
        call setEHEVK(EH2,EV2,K2)
        call rayToNorm(EHk,mu1,nu1,cphi1,sphi1)
        call rayToNorm(EVk,mu1,nu1,cphi1,sphi1)
 
        cpsi = EH2(1)*EHk(1)+EH2(2)*EHk(2)
        spsi = -EH2(1)*EVk(1)-EH2(2)*EVk(2)

        c2psi = cpsi**2-spsi**2
        s2psi=2.0_rk*spsi*cpsi
        !call updateRotationMatrix(R,c2psi,s2psi)
        !call matXvec(I2,R,I2)
        Itmp=I2
        I2 = combinedRotationFunc(Itmp,c2psi,s2psi)
    end subroutine


    !---------------------------------------------------
    !pure subroutine setEHEVK(EH,EV,K)
    !
    !Compute basis vector in the scattering direction K
    !---------------------------------------------------
    pure subroutine setEHEVK(EH,EV,K)
        real(kind=rk), intent(out) :: EH(3),EV(3)   !Basis vectors
        real(kind=rk), intent(in) :: K(3)           !direction          
        real(kind=rk), parameter :: tol = 0.99999999999999_rk
        !upward direction 
        if (K(3)>tol) then
            EH(1)= 0.0_rk
            EH(2)=-1.0_rk
            EH(3)= 0.0_rk
            EV(1)= 1.0_rk
            EV(2)= 0.0_rk
            EV(3)= 0.0_rk
             
        !Downward direction:
        elseif (K(3)<-tol) then
            EH(1)= 0.0_rk
            EH(2)=-1.0_rk
            EH(3)= 0.0_rk

            EV(1)=-1.0_rk
            EV(2)= 0.0_rk
            EV(3)= 0.0_rk

        !All other directions:
        else
            EV(3)=-sqrt(1.0_rk-K(3)**2)
            EH(1)= K(2)/(-EV(3))
            EH(2)=-K(1)/(-EV(3))
            EH(3)=0.0_rk
            EV(1)=K(3)*K(1)/(-EV(3))
            EV(2)=K(3)*K(2)/(-EV(3))
        endif
    end subroutine

end module
