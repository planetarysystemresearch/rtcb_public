!-----------------------------
!a simple error handler
!
!Copyright (C) 2016 Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!-----------------------------
module errorHandler
    implicit none
    integer :: errorCount = 0
    private errorCount
contains

    !-----------------------------
    !subroutine addError(msg)
    !
    !Increase the error count and 
    !display a message (msg)
    !-----------------------------
    subroutine addError(msg)
        character(*), intent(in) :: msg     !message to be displayed
        errorCount=errorCount+1
        write(6,*) "error: "//trim(msg)
    end subroutine

    !-----------------------------
    !function checkIfErrors() result(retVal))
    !
    !Check whether the error count is /= 0
    !-----------------------------
    function checkIfErrors() result(retVal)
       logical :: retVal                    
       retVal=.false.
       if(errorCount/=0) retVal=.true.
    end function

    !-----------------------------
    !checkCondition(bool,msg)
    !
    !IN:    bool:   condition
    !IN:    msg:    message sent if condition is not true
    !-----------------------------
    subroutine checkCondition(bool,msg)
        logical, intent(in) :: bool
        character(*), intent(in) :: msg 
        if(.not. bool) call addError(msg)
    end subroutine
end module
