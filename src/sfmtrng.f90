!------------------------------------------------
!Get random number's from 
!SIMD-oriented Fast Mersenne Twister (dSFMT) by
!Mutsuo Saito, Makoto Matsumoto(2007,2008)
!
!Uses Fortran interface for dSFMT by James Spencer(2012)
!
!1. Ask module to generate a seed from the given input
!(initSeed())
!2. Use the seed to initialise the RNG
!3. Get numbers by calling randNum() 
!
!Each module has own generator(rng0) which are 
!initialised with the givenSeed. The generator stores
!a certain amount of random numbers(RANDOMNUMBERSSTORED)
!from where the numbers can be accessed fast
!
!Copyright (C) 2016 Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!------------------------------------------------

!Define how many random numbers generator should store
#define     RANDOMNUMBERSSTORED 65536
module rng
    use dSFMT_interface
    implicit none
    integer, parameter :: rk=selected_real_kind(15)
    real(kind=rk), parameter :: pi = 4.0_rk*atan(1.0_rk)
    integer :: idum
    !module's private rng0
    type(dSFMT_t) :: rng0
    !$OMP threadprivate(rng0)
    private :: rng0,rk,pi
contains


    !------------------------------------------------
    !Initialise PRNG with the given seed
    !Calls dSFMT interface's dSFMT_init()
    !
    !IN: givenSeed: seed for the RNG
    !------------------------------------------------
    subroutine init_rng(givenSeed,steps)
        integer, intent(in) :: givenSeed
        integer, intent(in) :: steps
        !Parameter: seed, number of randoms, generator to operate
        !write(6,*) givenSeed,tid
        idum = 2
        call dSFMT_init(givenSeed, RANDOMNUMBERSSTORED, rng0) 
        if(steps/=0) then
            call dSFMT_jumpf(rng0,steps) 
        endif
    end subroutine

    !------------------------------------------------
    !Initialise seed    
    !Seed will be the current time from OS's clock
    !if givenSeed<=0. Otherwise seed=givenSeed
    !Function will return seed for further use
    !
    !IN: givenSeed: input for seed
    !OUT: seed: seed for RNG
    !------------------------------------------------
    function initSeed(givenSeed) result(seed)
        integer, intent(in) :: givenSeed
        integer :: seed,clock
    end function


    !------------------------------------------------
    !Get random number from the generator's array
    !
    !OUT: retVal: Random number [0,1) 
    !------------------------------------------------
    function randNum() result(retVal)
        real(kind=rk) :: retVal
        retVal = get_rand_close_open(rng0)
    end function



    !!Generate random number with normal distribution
    !!Uses Box Muller-algorithm
    !!Give: mean and stdev
    !!if stdev<=0 returns mean
    function rnd_normal(mean,stdev) result(c)
      real(kind=rk), intent(in) :: mean,stdev
      real(kind=rk) :: c,temp(2), theta,r 
      if(stdev <= 0.0_rk) then
        c = mean
      else
        c = -1.0_rk
        do while(c<0.0_rk)
          r=(-2.0_rk*log(randNum()))**0.5_rk
          theta = 2.0_rk*pi*randNum()
          c= mean+stdev*r*sin(theta)
        enddo
      endif
    end function


end module

