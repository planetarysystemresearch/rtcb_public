!----------------------------------------------------
!Collect data from the accessible data to assembled data. 
!Results are stored from stoke's vector to mueller matrix.
!Also collects the total intensities from reflection, Specular reflection,
!absorption, stopping intensity, transmitted and direct transmission.
!sphere geometry.
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!----------------------------------------------------
module collectdata
    use constants
    use typedefinitions

contains

    !----------------------------------------------------
    !
    !----------------------------------------------------
    subroutine updateMS(aD, accD, I)
        type(assembledData), intent(inout) :: aD    !Collect temporary results to here
        type(accessibleData), intent(in) :: AccD    !temporary results from the algorithm
        real(kind=rk), intent(in) :: I(4)           !initial Stoke's vector configuration
        integer :: NFI(8),NFIB(8)
        logical :: linear
        integer :: j1,j3,k1,k2,j0
        integer :: ntheb, nphib,nphi,nthe
        linear = .true.

        ntheb=size(accD%IBS,dim=3)-1
        nphib=size(accD%IBS,dim=4)
        nphi=size(accD%IRT,dim=3)
        nthe=size(accD%IRT,dim=2)

        if(I(4)/=0.0_rk) linear=.false.

        do j1=0,7
            NFI(j1+1)=j1*(nphi/8)+1
            NFIB(j1+1)=j1*(nphib/8)+1
        enddo

        if(linear) then
            
            aD%Aref=aD%Aref+accD%Aref
            aD%Adt=aD%Adt+accD%Adt
            aD%Aabs=aD%Aabs+accD%Aabs
            aD%Astop=aD%Astop+accD%Astop
            do j1=1,nthe
                aD%MRT(j1,1,:,1)=aD%MRT(j1,1,:,1)+                        &
                &  (accD%IRT(:,j1,NFI(1))+accD%IRT(:,j1,NFI(5))+     &
                &   accD%IRT(:,j1,NFI(3))+accD%IRT(:,j1,NFI(7)))/4.0_rk
                aD%MRT(j1,1,:,2)=aD%MRT(j1,1,:,2)+                        &
                & (-accD%IRT(:,j1,NFI(1))-accD%IRT(:,j1,NFI(5))+    &
                &   accD%IRT(:,j1,NFI(3))+accD%IRT(:,j1,NFI(7)))/4.0_rk
                aD%MRT(j1,1,:,3)=aD%MRT(j1,1,:,3)+                        &
                & (-accD%IRT(:,j1,NFI(4))-accD%IRT(:,j1,NFI(8))+    &
                &   accD%IRT(:,j1,NFI(2))+accD%IRT(:,j1,NFI(6)))/4.0_rk
            end do
            do j1=1,ntheb+1
                do j0=1,2
                    aD%MBS(j0,j1,1,:,1)=aD%MBS(j0,j1,1,:,1)+                      &
                    &  (accD%IBS(:,j0,j1,NFIB(1))+accD%IBS(:,j0,j1,NFIB(5))+ &
                    &   accD%IBS(:,j0,j1,NFIB(3))+accD%IBS(:,j0,j1,NFIB(7)))/4.0_rk
                    aD%MBS(j0,j1,1,:,2)=aD%MBS(j0,j1,1,:,2)+                      &
                    & (-accD%IBS(:,j0,j1,NFIB(1))-accD%IBS(:,j0,j1,NFIB(5))+&
                    &   accD%IBS(:,j0,j1,NFIB(3))+accD%IBS(:,j0,j1,NFIB(7)))/4.0_rk
                    aD%MBS(j0,j1,1,:,3)=aD%MBS(j0,j1,1,:,3)+                      &
                    & (-accD%IBS(:,j0,j1,NFIB(4))-accD%IBS(:,j0,j1,NFIB(8))+&
                    &   accD%IBS(:,j0,j1,NFIB(2))+accD%IBS(:,j0,j1,NFIB(6)))/4.0_rk
                end do
            end do
        else
            do j1=1,nthe
                aD%MRT(j1,1,1,4)=aD%MRT(j1,1,1,4)+0.0_rk
                aD%MRT(j1,1,2,4)=aD%MRT(j1,1,2,4)+0.0_rk
                aD%MRT(j1,1,3,4)=aD%MRT(j1,1,3,4)-&
                    &   (accD%IRT(3,j1,NFI(1))+accD%IRT(3,j1,NFI(5)))/2.0_rk
                do j3=1,nphi
                    aD%MRT(j1,1,4,4)=aD%MRT(j1,1,4,4)-accD%IRT(4,j1,j3)/nphi
                enddo
            enddo
            do j1=1,ntheb+1
                do j0=1,2
                    aD%MBS(j0,j1,1,1,4)=aD%MBS(j0,j1,1,1,4)+0.0_rk
                    aD%MBS(j0,j1,1,2,4)=aD%MBS(j0,j1,1,2,4)+0.0_rk
                    aD%MBS(j0,j1,1,3,4)=aD%MBS(j0,j1,1,3,4)-                        &
                    &   (accD%IBS(3,j0,j1,NFIB(1))+accD%IBS(3,j0,j1,NFIB(5)))/2.0_rk
                    do j3=1,nphib
                        aD%MBS(j0,j1,1,4,4)=aD%MBS(j0,j1,1,4,4)-accD%IBS(4,j0,j1,j3)/nphib
                    enddo
                enddo
            enddo
        endif
    end subroutine

end module

