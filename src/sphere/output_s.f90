!----------------------------------------------------
!RTCB
!Output
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt

module output
    use constants
    use typedefinitions
    use scattererModule
    implicit none
contains

    subroutine printOutput(aD,dB)
        type(assembledData),intent(inout) :: aD
        type(dataBlock), intent(in) :: dB
        character(len=120) :: addString
        character(8)  :: date
        character(10) :: time
        character(5)  :: zone
        real(kind=rk) :: Aref,Aspec,Asca,Aabs,Astop,Atot,renorm,Atmp,Atra,Adt
        real(kind=rk) :: norm,rd,phiout,M(4,4),the
        integer :: kphi, nthe,nphi,nphib, ntheb
        integer :: j0,j1,j2,j3,j4
        real(kind=rk), allocatable :: MBS(:,:,:,:),MRT(:,:,:)
        
        Aref=aD%Aref/dB%ntot
        Aspec=aD%Aspec/dB%ntot
        Adt=aD%Adt/dB%ntot
        Atra=aD%Atra/dB%ntot

        call date_and_time(DATE=date,ZONE=zone)
        call date_and_time(TIME=time)

        Asca=Aref+Adt
        write(addString,*) "#RT-CB sphere geometry,",date,",",time,",",dB%hr 

        Aabs=aD%Aabs/dB%ntot
        Astop=aD%Astop/dB%ntot
        Atot=Asca+Aspec+Aabs+Astop
        allocate(MBS(2,db%ntheb+1,4,4))
        allocate(MRT(db%nthe,4,4))
        MBS=aD%MBS(:,:,1,:,:)
        MRT=aD%MRT(:,1,:,:)
        rd=pi/180.0_rk

        !parameter output
        open(unit=1,file=trim(adjustl(dB%output_details)))
        write(1,*) addString        
        if(dB%addInfo) write(1,*) "#",dB%outputExt
        call writeInfoScreen(dB,-1,-1,1)
        write(1,'(A)') ""
        write(1,'(A)') "Raw albedos"
        write(1,'(A20,"=",3X,E12.5)') "Aref",Aref
        write(1,'(A20,"=",3X,E12.5)') "Atra",Atra
        write(1,'(A20,"=",3X,E12.5)') "Adt",Adt
        write(1,'(A20,"=",3X,E12.5)') "Atra+Adt",Atra+Adt
        write(1,'(A20,"=",3X,E12.5)') "Asca",Asca
        write(1,'(A20,"=",3X,E12.5)') "Aabs",Aabs
        write(1,'(A20,"=",3X,E12.5)') "Asca+Aabs",Asca+Aabs
        write(1,'(A)') "Waste albedo"
        write(1,'(A20,"=",3X,E12.5)') "Astop",Astop
        write(1,'(A)') "Total albedo:"
        write(1,'(A20,"=",3X,E12.5)') "Atot",Atot
        write(1,'(A20,"=",3X,E12.5)') "1 - Atot",1.0_rk - Atot
        
        

        !Conditionally, include wasted energy via renormalization:
        renorm=1.0_rk
        Aref=renorm*Aref
        Asca=renorm*Asca
        Aabs=renorm*Aabs
        Atra=renorm*Atra
        Adt=renorm*Adt



        nthe = dB%nthe
        nphi = dB%nphi
        phiout = dB%phiout
        nphib = dB%nphib
        ntheb = dB%ntheb
        write(1,'(A)') ""
        write(1,'(A)') "Renormalized albedos:"
        write(1,'(A20,"=",3X,E12.5)') "Aref",Aref
        write(1,'(A20,"=",3X,E12.5)') "Atra",Atra
        write(1,'(A20,"=",3X,E12.5)') "Adt",Adt
        write(1,'(A20,"=",3X,E12.5)') "Asca",Asca
        write(1,'(A20,"=",3X,E12.5)') "Aabs",Aabs
        write(1,'(A20,"=",3X,E12.5)') "Asca+Aabs",Asca+Aabs
        renorm=1.0_rk/(Asca*dB%ntot)
        do j1=1,nthe
            norm=renorm/dB%INORM(j1)
            do j2=1,4
                do j3=1,4
                    MRT(j1,j2,j3)=norm*MRT(j1,j2,j3)
                enddo
            enddo
        enddo
        norm=1.0_rk/(Asca*dB%ntot)   
        do j1=1,ntheb+1
            do j2=1,4
                do j3=1,4 
                    MBS(1,j1,j2,j3)=norm*MBS(1,j1,j2,j3)
                    MBS(2,j1,j2,j3)=norm*MBS(2,j1,j2,j3)
                enddo
            enddo
        enddo


        Atmp=0.0d0
        if(aD%Adt/=0.0_rk) then
            do j1=1,dB%nthe
                norm=4.0d0*pi*dB%nphi/(dB%ntot*aD%Adt)
                Atmp=Atmp+norm*MRT(j1,1,1)
            enddo
        endif

        write (1,'(A20,"=",3X,E12.5)') "Calc",Atmp
        write(1,'(A)') "miscellaneous data"
        write(1,'(A20,"=",3X,I12)') "seed",dB%seed


        call printScattererData(dB,1)
        close(1)

        !call outputReflectedRT(aD,dB)
        open(unit=1234,file=trim(adjustl(dB%output_rt)))
        if(dB%addInfo) write(1234,*) "#",dB%outputExt
        if(dB%addInfo) write(1234,*) addString      
        do j1=nthe,1,-1
            write(1234,'(F7.2,16(1X,E15.8))') acos(dB%CTHEI(j1))/rd,   &
     &             (MRT(j1,1,j2),j2=1,4),(MRT(j1,2,j2),j2=1,4),     &
     &             (MRT(j1,3,j2),j2=1,4),(MRT(j1,4,j2),j2=1,4)
        enddo
        close(1234)


        !Complete output of the scattering radiation:
        open(unit=1234,file=trim(adjustl(dB%output_cb)))
        if(dB%addInfo) write(1234,*) "#",dB%outputExt
        if(dB%addInfo) write(1234,*) addString
       
        do j1=1,ntheb+1
            write(1234,'(F8.4,32(1X,E15.8))') dB%THEBF(j1)/rd,      &
            &   (MBS(1,j1,1,j2),j2=1,4),(MBS(1,j1,2,j2),j2=1,4),&
            &   (MBS(1,j1,3,j2),j2=1,4),(MBS(1,j1,4,j2),j2=1,4),&
            &   (MBS(2,j1,1,j2),j2=1,4),(MBS(2,j1,2,j2),j2=1,4),&
            &   (MBS(2,j1,3,j2),j2=1,4),(MBS(2,j1,4,j2),j2=1,4)
        enddo
        close(1234)
    end subroutine


    !For test purposes
    subroutine writeInfoScreen(dB,nodes,threads,stream)
        integer, intent(in) :: nodes,threads,stream
        type(dataBlock), intent(in) :: dB
        write(stream,*) "////////////////////////////"
        write(stream,*) "RT-CB"
        write(stream,*) "Sphere geometry"        

       
        if(nodes/=-1 .and. threads/=-1) then
#ifdef MPIVERSION
        write(stream,*) "MPI PROCESSES",nodes,"openMP threads",threads
#else
        write(stream,*) "openMP threads",threads
#endif
        endif

        write(stream,*) "////////////////////////////"
        write(stream,'(A30,E12.5)') "wavelength", dB%wavel
        write(stream,'(A30,E12.5)') "single_scattering_albedo", dB%ssalbedo
        write(stream,'(A30,E12.5)') "mean_free_path",dB%ell1
        write(stream,'(A30,E12.5)') "medium_thickness",dB%hr
        write(stream,'(A30,E12.5)') "min_relative_flux",dB%Fstop
        write(stream,'(A30,E12.5)') "threshold_for_optical_depth",dB%tau_c
        write(stream,'(A30,I12)') "number_of_theta_angles",dB%nthe
        write(stream,'(A30,I12)') "number_of_phi_angles",dB%nphi
        write(stream,'(A30,I12)') "max_number_of_scattering",dB%nsca
        write(stream,'(A30,I12)') "seed",dB%seed
        write(stream,'(A30,I12)') "number_of_rays",dB%ntot
        write(stream,*)

        write(stream,'(A30,E12.5)') "host_med_refractive_index_real", dB%mre
        write(stream,'(A30,E12.5)') "host_med_refractive_index_imag", dB%mim  
        write(stream,*)

        write(stream,'(A30,1X,A30)') "output_details",dB%output_details
        write(stream,'(A30,1X,A30)') "rt_solution",dB%output_rt
        write(stream,'(A30,1X,A30)') "cb_solution",dB%output_cb
        write(stream,'(A30,2X,L1)') "output_add_additional_info",dB%addInfo
        write(stream,'(A30,2X,l1)') "estimate_time",dB%estimateTime
        write(stream,'(A30,2X,A30)') "output_extra_info",dB%outputExt
        write(stream,'(A30,E12.5)') "vol_element_radius",dB%volrad0

        write(stream,*) "///////////////////////////////////////"
        write(stream,'(A30,A12)') "scatterer_type ",adjustl(trim(dB%scattererType))
        if(trim(adjustl(dB%scattererType))=="spline") then         
            !Scatterer type        
            if(.not. dB%generateMieScatterer) then
                write(stream,"(A30)") "mean_free_path is user-defined"
                write(stream,"(A30)") "single_scattering_albedo is user-defined" 
                write(stream,'(A30,A12)') "load_scatterer_data_from",adjustl(trim(dB%scattererData))
            else
                write(stream,"(A30)") "mie scatterer was generated"
                write(stream,"(A30)") "mean_free_path is generated by the program" 
                write(stream,"(A30)") "single_scattering_albedo is generated by the program" 
                write(stream,'(A30,I12)') "points_in_spline_presentation",dB%np
                write(stream,'(A30,E12.5)') "scatterer_radius",dB%scRadius
                write(stream,'(A30,E12.5)') "scatterer_real_ref",dB%scRealRef
                write(stream,'(A30,E12.5)') "scatterer_imag_ref",dB%scImagRef
                write(stream,'(A30,E12.5)') "volume_fraction",dB%vf
                write(stream,'(A30,A30)') "save_scatterer_data_to",adjustl(trim(dB%saveScatterer))
                write(stream,'(A30,I12)') "nrn",dB%nrn
                write(stream,'(A30,I12)') "ncm",dB%ncm

            endif
        else
            write(stream,"(A40)") "mean_free_path is user-defined"
            write(stream,"(A40)") "single_scattering_albedo is user-defined"
        endif

        write(stream,*) "////////////////////////////"
    end subroutine

end module


