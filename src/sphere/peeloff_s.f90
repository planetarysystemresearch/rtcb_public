!----------------------------------------------------
!Module peeloff contains all the functions required for
!peel-off. This module needs to be modified for each geometry.
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!----------------------------------------------------
module peeloff
    use constants
    use geoopt
    use mathroutines
    use stokes
    implicit none

contains

    !----------------------------------------------------
    !subroutine scart(IDS,I0,X0,K0,INORM,CTHEI,STHEI,&
    !   &            CPHII,SPHII,Atmp,m,mre,tau,tau_c,&
    !   &            nthe,nphi)
    !
    !Peel-off which is done by collecting intensity at 
    !detectors located at the infinity in the location
    !determined by the normcoeff.
    !---------------------------------------------------- 
    subroutine scart(IDS,I0,X0,K0,INORM,CTHEI,STHEI,&
     &                     CPHII,SPHII,Atmp,tau,tau_c,&
     &                     nthe,nphi)
        real(kind=rk), intent(in) :: I0(4)                                  !Stoke's vector of the incident ray
        real(kind=rk), intent(inout) :: IDS(:,:,:)                          !intensities collected at detectors
        real(kind=rk), intent(in) :: INORM(:)                               !normalization coefficients
        real(kind=rk), intent(in) :: CTHEI(:),STHEI(:),CPHII(:),SPHII(:)    !location of the detectors
        real(kind=rk), intent(in) :: X0(3)                                  !the location of the scatterer
        real(kind=rk), intent(in) :: K0(3)                                  !incident angle
        real(kind=rk), intent(in) :: tau                                    !radius of the sphere (as tau)
        real(kind=rk), intent(in) :: tau_c                                  !threshold for exp(tau_c)=>0
        real(kind=rk),intent(out) :: Atmp                                   !The sum of collected intensity
        integer, intent(in) :: nthe,nphi                                    !number of theta and phi angle
        real(kind=rk) :: r0,mu0,phi0,nu0,cphi0,sphi0,I1(4)
        real(kind=rk) :: K1(3),EH1(3),EV1(3),I2(4)
        real(kind=rk) :: next,t,K(3),kx,xx,ar
        integer :: j1,j2
        call getSphericalCoordinates2(K0,r0,mu0,phi0)
        !Cosines and sines of the local incidence:
        nu0=sqrt(1.0_rk-mu0**2)
        cphi0=cos(phi0)
        sphi0=sin(phi0)
        Atmp=0.0_rk
        do j2=1,nphi        
            do j1=1,nthe
                !Stokes vector of scattered radiation
                K1(1)=STHEI(j1)*CPHII(j2)
                K1(2)=STHEI(j1)*SPHII(j2)
                K1(3)=CTHEI(j1)        
                call updateStokes(I1,K1,EH1,EV1,I0,mu0,nu0,cphi0,sphi0)
                
                !Extinction
                kx=dotProduct(K1,X0)
                xx=dotProduct(X0,X0)
                t=-kx+sqrt(kx**2+tau**2-xx) 

                if(t<=tau_c) then
                    next=exp(-t)
                    I2(:)=I1(:)*INORM(j1)*next
                    IDS(:,j1,j2)=IDS(:,j1,j2)+I2(:)  
                    Atmp=Atmp+I2(1)
                endif
            enddo
        enddo
    end subroutine
end module
