!----------------------------------
!Generate the grid for detector locations and normalization coefficients
!Plane-parallel version
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt

module normCoeff
    !use same precision everywhere
    use constants
    !type definitions
    use typedefinitions
    use mathroutines
    implicit none
contains
    

    !----------------------------------
    !subroutine normCoeffsIJCS(dB)
    !Computes the Stokes vector normalisation coefficients
    !Stokes,Jacobian,Cosines and Sines
    !----------------------------------
    subroutine normCoeffsIJCS(dB)
        type(dataBlock), intent(inout) :: dB
        real(kind=rk), allocatable :: W(:),X(:)
        real(kind=rk) :: phi        
        integer :: j1,j2,nhalf,ind

        nhalf=dB%nthe/2
        !allocate temporary arrays 
        allocate(X(dB%nthe),W(dB%nthe))
        
        !allocate data in dB
        allocate(dB%CTHEIF(dB%nthe))
        allocate(dB%STHEIF(dB%nthe))
        allocate(dB%CTHEI(dB%nthe))
        allocate(dB%STHEI(dB%nthe))
        allocate(dB%INORM(dB%nthe))
        allocate(dB%CPHII(dB%nphi))
        allocate(dB%SPHII(dB%nphi))

        !Gaussian Legengdre
        call gaussLQuads(-1.0_rk,1.0_rk,X,W)
        do j1 = 1,dB%nthe
            dB%INORM(j1)=W(j1)*(2.0_rk*pi/dB%nphi)/(4.0_rk*pi)
            dB%CTHEI(j1)=X(j1)
            dB%STHEI(j1)=sqrt(1.0_rk-dB%CTHEI(j1)**2)    
            dB%CTHEIF(j1)=dB%CTHEI(j1)
            dB%STHEIF(j1)=dB%STHEI(j1)
        end do

        do j1 = 1,dB%nphi
            phi=(j1-1)*2.0_rk*pi/dB%nphi
            dB%CPHII(j1)=cos(phi)
            dB%SPHII(j1)=sin(phi)
        end do

        !Don't need these anymore
        deallocate(X,W)
    end subroutine

    !----------------------------------
    !subroutine normCoeffsBJCS(dB)
    !Computes the Stokes vector normalization coefficients, including
    !a jacobian, and related cosines and sines. Note that the exact forward 
    !scattering direction must be excluded from the set of angles.
    !----------------------------------
    subroutine normCoeffsBJCS(dB)
        type(dataBlock), intent(inout) :: dB
        integer j1,ntheb,nphib,ind,j2
        real(kind=rk), parameter :: rd=pi/180.0_rk
        ntheb = 50
        nphib = 8
        dB%ntheb=ntheb        
        dB%nphib=nphib

        allocate(dB%THEBF(ntheb+1))
        allocate(dB%CTHEBF(ntheb+1))
        allocate(dB%STHEBF(ntheb+1))
        allocate(dB%STHEB(ntheb+1))
        allocate(dB%CTHEB(ntheb+1))
        allocate(dB%CPHIB(nphib))
        allocate(dB%SPHIB(nphib))
        allocate(dB%PHIB(nphib))


        dB%THEBF(1)=0.0_rk

        dB%THEBF(2) =0.02_rk*rd
        dB%THEBF(3) =0.04_rk*rd
        dB%THEBF(4) =0.06_rk*rd
        dB%THEBF(5) =0.08_rk*rd
        dB%THEBF(6) =0.1_rk*rd

        dB%THEBF(7) =0.15_rk*rd
        dB%THEBF(8) =0.20_rk*rd
        dB%THEBF(9) =0.25_rk*rd
        dB%THEBF(10)=0.30_rk*rd
        dB%THEBF(11)=0.35_rk*rd
        dB%THEBF(12)=0.40_rk*rd
        dB%THEBF(13)=0.45_rk*rd
        dB%THEBF(14)=0.50_rk*rd
        
        dB%THEBF(15)=0.60_rk*rd
        dB%THEBF(16)=0.70_rk*rd
        dB%THEBF(17)=0.80_rk*rd
        dB%THEBF(18)=0.90_rk*rd
        dB%THEBF(19)=1.0_rk*rd

        dB%THEBF(20)=1.5_rk*rd
        dB%THEBF(21)=2.0_rk*rd
        dB%THEBF(22)=2.5_rk*rd
        dB%THEBF(23)=3.0_rk*rd
        dB%THEBF(24)=4.0_rk*rd
        dB%THEBF(25)=5.0_rk*rd

        dB%THEBF(26)=6.0_rk*rd
        dB%THEBF(27)=7.0_rk*rd
        dB%THEBF(28)=8.0_rk*rd
        dB%THEBF(29)=9.0_rk*rd
        dB%THEBF(30)=12.0_rk*rd

        dB%THEBF(31)=15.0_rk*rd
        dB%THEBF(32)=18.0_rk*rd
        dB%THEBF(33)=21.0_rk*rd
        dB%THEBF(34)=24.0_rk*rd
        dB%THEBF(35)=27.0_rk*rd
        dB%THEBF(36)=30.0_rk*rd
        dB%THEBF(37)=40.0_rk*rd
        dB%THEBF(38)=50.0_rk*rd

        dB%THEBF(39)=60.0_rk*rd
        dB%THEBF(40)=70.0_rk*rd
        dB%THEBF(41)=80.0_rk*rd
        dB%THEBF(42)=90.0_rk*rd
        dB%THEBF(43)=100.0_rk*rd
       
        dB%THEBF(44)=110.0_rk*rd
        dB%THEBF(45)=120.0_rk*rd
        dB%THEBF(46)=130.0_rk*rd
        dB%THEBF(47)=140.0_rk*rd
        dB%THEBF(48)=150.0_rk*rd
        dB%THEBF(49)=160.0_rk*rd
        dB%THEBF(50)=170.0_rk*rd
        dB%THEBF(51)=180.0_rk*rd    ! Final the must be 180 deg

        dB%CTHEB(1)=1.0_rk
        dB%STHEB(1)=0.0_rk
        dB%CTHEBF(1)=1.0_rk
        dB%STHEBF(1)=0.0_rk
        
        do j1 = 2,ntheb+1
            dB%CTHEB(j1)=cos(dB%THEBF(j1))
            dB%STHEB(j1)=sin(dB%THEBF(j1))
            dB%CTHEBF(j1)=dB%CTHEB(j1)
            dB%STHEBF(j1)=dB%STHEB(j1)
        end do

        do j1=1,nphib
            dB%PHIB(j1)=(j1-1)*2.0_rk*pi/nphib
            dB%CPHIB(j1)=cos(dB%PHIB(j1))
            dB%SPHIB(j1)=sin(dB%PHIB(j1))
        end do

       
    end subroutine    
end module
