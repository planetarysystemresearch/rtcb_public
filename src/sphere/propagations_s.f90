!----------------------------------------------------
!Handle propagations inside the medium. The extinction is assumed to be exponential.
!This is the plane-parallel version
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!----------------------------------------------------
module propagations
    use constants
    use mathroutines
    use rng
    implicit none
contains
    

    !----------------------------------------------------
    !subroutine PRPGS1(X,K,tau_c,taur)
    !Propagates the ray within the spherical scattering 
    !and absorbing medium. This is used when entering
    !the medium
    !----------------------------------------------------
    subroutine prpgs1(X,K,tau_c,taur)
        real(kind=rk), intent(inout) :: X(3)   !in: entry point, out: next scattering location 
        real(kind=rk), intent(in) :: K(3)      !direction
        real(kind=rk), intent(in) :: tau_c     !threshold for optical depth
        real(kind=rk), intent(in) :: taur      !radius of the sphere (optical depth)
        real(kind=rk) :: tauc,kx,xx,rn,t
        kx = dot_product(K,X)
        xx = dot_product(X,X)
        tauc=-kx+sqrt(kx**2+taur**2-xx)
        if(tauc<=tau_c) then
            rn=randNum()
            t=-log(1.0_rk-rn*(1.0_rk-exp(-tauc)))
        else
            t=tauc+1.0_rk
            do while(t>tauc)
                rn=randNum()
                t=-log(1.0_rk-rn)
            enddo
        endif
        X(:)=X(:)+t*K(:)
    end subroutine

    !----------------------------------------------------
    !subroutine prpgs2(X,K,taur,inside)
    !Propagates the ray within the spherical scattering 
    !and absorbing medium. This is used when entering
    !the medium. The subroutine will tell if the
    !ray scattered out of the medium by setting inside=.false.
    !----------------------------------------------------
    subroutine prpgs2(X,K,taur,inside,volrad)
        real(kind=rk), intent(inout) :: X(3)   !in: entry point, out: next scattering location 
        real(kind=rk), intent(in) :: K(3)      !direction
        real(kind=rk), intent(in) :: taur      !radius of the sphere (optical depth)
        logical, intent(out) :: inside         !Is the ray inside?    
        real(kind=rk) :: t
        real(kind=rk) :: Y(3),yy,forced_r_sca,inter
        real(kind=rk), intent(in) :: volrad
        inside=.true.
        t=-log(randNum())

        if(t<2*volrad) then
            inter=1/12.0_rk*pi*(4*volrad+t)*(2*volrad-t)**2
            if(randNum()>(1-(inter/(4/3.0_rk*pi*volrad**3))**(1.0_rk/1.0_rk))) then
                inside = .false.
                return
            endif
        endif


        Y(:)=X(:)+t*K(:)
        yy=dotProduct(Y,Y)
        if(sqrt(yy)<taur) then
            X(:)=Y(:)
        else
            inside=.false.
        endif
    end subroutine

end module
