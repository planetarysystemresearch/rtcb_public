!-----------------------------
!RTCB
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!-----------------------------
module prepareData 
    !get global precision values
    use constants
    !and error handler
    use errorHandler
    !dBlock type
    use typedefinitions
    !scatterer module
    use scattererModule
    !get normalisation subroutines
    use normCoeff
    implicit none
    private doChecks

contains

    !-----------------------------
    !prepare(dB,aD)
    !INOUT dB: dataBlock: initialise data using input data
    !-----------------------------
    subroutine prepare(dB)
        type(dataBlock), intent(inout) :: dB
        real(kind=rk) :: Kext1,Kext2,ell2,Kext,ell,xell1,xell2,xell,waven
        real(kind=rk) :: xh
        real :: arg
        integer :: j1,j2
        integer, allocatable :: seed(:)
        integer :: size0,id,clock
        !Do checks before proceeding
        call doCheckS(dB)

        !wavenumber and refractive indeces
        waven=2.0_rk*pi/dB%wavel
        xh=waven*dB%hr
        dB%waven=waven

        !Extinction coefficients, mean-free-path parameters, size parameter
        !conversion factor, optical thicknesses,
        !total single-scattering albedo,

#ifdef  HM
        dB%m12=cmplx(dB%mre,dB%mim,kind=rk)
        dB%m21=1.0_rk/dB%m12
        dB%mre12=dB%mre 
        dB%mre21=1.0_rk/dB%mre
#endif

        !convert degrees to radians
        dB%the0 = dB%the0*(pi/180_rk)
        dB%phi0 = dB%phi0*(pi/180_rk)
        dB%phiout = dB%phiout*(pi/180_rk)

        
        !incident angles
        dB%mu0=cos(dB%the0)
        dB%nu0=sqrt(1.0_rk-dB%mu0**2)
        dB%cphi0=cos(dB%phi0)
        dB%sphi0=sin(dB%phi0)

        !Compute intensity normalization coefficients, cosines, sines, and
        !jacobians:
        call normCoeffsIJCS(dB)
        call normCoeffsBJCS(dB)
        !Select scatterer
        call selectScatterer(dB)        
        !Initialize the scattering phase matrix and
        !the amplitude scattering matrix:
        call prepareScatterers(dB)

#ifdef  HM
        Kext1=1.0_rk/dB%ell1
        Kext2=2.0_rk*dB%mim*waven
        ell2=1.0_rk/Kext2
        Kext=Kext1+Kext2
        ell=1.0_rk/Kext
        xell1=dB%mre*waven*dB%ell1
        xell2=dB%mre*waven*ell2
        dB%xell=dB%mre*waven*ell
        dB%albedo = dB%ssalbedo*Kext1/Kext   
#else
        dB%xell=waven*dB%ell1
        dB%tau=dB%hr/dB%ell1
        dB%albedo=dB%ssalbedo
#endif
        dB%volrad=dB%volrad0/dB%ell1


        if(db%seed==0) then
            call random_seed(size=size0)
            allocate(seed(size0))
            call system_clock(count=clock)
            id = getpid() !Intel might need IFPORT, fix it later
            seed = clock+id * (/(j1-1,j1=1,size0)/)
            call random_seed(put=seed)
            call random_number(arg)
            dB%seed=int(arg*100000)
        endif
    end subroutine 


    !-----------------------------
    !prepare2(dB,iD)
    !Distribute data
    !-----------------------------
    subroutine prepare2(dB,id)
        type(dataBlock), intent(inout) :: dB    !dataBlock
        integer, intent(in) :: id               !id of the process
        integer :: ierr,ntasks          
        if(id==0) then
            call sendMPIData()
        else
            !Select scatterer
            call selectScatterer(dB)
            call rcvMPIData()
        endif
    end subroutine

    !-----------------------------
    !initAssembledData(dB,iD)
    !Initialize assembled Data
    !-----------------------------
    subroutine initAssembledData(dB,aD)
        type(dataBlock), intent(in) :: dB
        type(assembledData), intent(inout) :: aD   
        aD%ntheb = dB%ntheb
        aD%nthe = dB%nthe
        aD%nphib = dB%nphib
        aD%nphi = dB%nphi
        !MuellerMatrices,theta Angle, phiAngle
        allocate(aD%MBS(2,aD%ntheb+1,aD%nphib,4,4))
        allocate(aD%MRT(aD%nthe,aD%nphi,4,4))
        aD%MBS=0.0_rk
        aD%MRT=0.0_rk      
    end subroutine


    !-----------------------------
    !doChecks(dB)
    !
    !IN:    dB: input data
    !Check whether initial conditions are true
    !-----------------------------
    subroutine doChecks(dB)
        type(dataBlock), intent(in) :: dB
        !Prevent going from bot to top. Theta is angle from the z-axis clockwise
        !Radiation should come from top
        call checkCondition(dB%the0>90.0_rk,&
            &'impossible illumination direction(incTheta)')
        !phi out angle has to be less than 180
        call checkCondition(dB%phiout<180.0_rk .and. dB%phiout>=0.0_rk,&
            &'revise output plane(phiout)')
        !prevent zero wavelength
        call checkCondition(dB%wavel>0.0_rk,&
            &'wavelength has to be positive')
        !prevent negative refractive indeces
        call checkCondition(dB%mre>=0.0_rk,&
            &'refractive index(real) has to be positive')
        call checkCondition(dB%mim>=0.0_rk,&
            &'refractive index(imag) has to be positive')
        !check that albedo is between 0 and 1
        call checkCondition(dB%ssalbedo>=0.0_rk .and. dB%ssalbedo<=1.0_rk,&
            &'invalid single scatterer albedo: valid region [0,1]')
        !mean free path has to be positive
        call checkCondition(dB%ell1>=0.0_rk,&
            &'mean free path has to be positive')
        call checkCondition(dB%hr>0.0_rk,&
            &'thickness/radius has to be >0.0')
        call checkCondition(dB%ntot>=0,&
            &'num of rays has to be >0.0')
        call checkCondition(dB%Fstop>0,&
            &'min relative flux > 0.0')
        call checkCondition(dB%tau_c>0,&
            &'threshold for optical depth > 0.0')
        call checkCondition(dB%nthe>0,&
            &'add more theta angles')
        call checkCondition(dB%nphi>0,&
            &'add more phi angles')
        call checkCondition(dB%nrn>0,&
            &'add more phi nrn points')
        call checkCondition(dB%ncm>0,&
            &'add more phi ncm points') 
#ifdef HM
        call checkCondition(.not. dB%finite,&
            &'HM-version is only supported with semi-infinite medium')       
#endif
    end subroutine

end module
