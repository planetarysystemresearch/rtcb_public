!------------------------------------------
!Scatterer which attributes are read from
!spline presentation. Requires two
!additional files scatterer_file1(default=pmie.in) and
!scatterer_file2(default=smie.in)
!
!This module is used through scattererModule
!if selected scatterer_type=splinePresentation
!
!Notice that this module is split into two files
!(other one is splinepresentation2) and 
!the rest of the code is attached using include
!because otherwise this module would be
!too long. splinepresentation2 has all the
!procedures used for preparation purposes
!while splinepresentation has the interface (scattererModule)
!related procedures.
!
!Tables
!XP(1:np+1): covers angles from 0:pi
!coeffs(4,6,theta): coeffiecients for P matrix 
!coeffs2(4,4,theta): coefficients for S matrix
!Read more about coefficients from splineTools.f08
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!------------------------------------------

module splinePresentation
    !global precision
    use constants
    !type definition
    use typedefinitions
    use rng    
    use errorHandler
    use mathroutines
    use splinetools
    use mie
    implicit none
    integer :: np,ncm,nrn,ns
    real(kind=rk) :: dthe
    !phase matrix related
    real(kind=rk), allocatable :: XP(:),coeffs(:,:,:)
    !amplitude scattering matrix related
    real(kind=rk), allocatable :: coeffs2(:,:,:)
    real(kind=rk), allocatable :: CSRN(:)
    private np,ncm,nrn,ns,dthe,CSRN,xp
    private PSMIE
    private bisectionMod,pcdfspli,cspspli
contains


    include "splinepresentation2.f90"
#ifdef MPIVERSION
    include "splineMPItools.f90"
#else
    subroutine splineMPISend()
    end subroutine

    subroutine splineMPIrcv()
    end subroutine          
#endif


    !------------------------------------------
    !prepareSpline(dataB)
    !
    !Prepares module's private variables before
    !simulation.  
    !
    !IN: dataB: Pass input
    !
    !Fills coeffs,coeffs2,xp,npncm,nrn etc.
    !variables. Procedures are located in splinepresentation2
    !------------------------------------------
    subroutine prepareSpline(dB)
        type(dataBlock), intent(inout) :: dB    !dataBlock
        real(kind=rk),allocatable :: P(:,:)
        real(kind=rk) :: dummy
        call PSMIE(P,dB)
        if(checkIfErrors()) return
        call psplii(P,dummy)
    end subroutine


    !------------------------------------------
    !splineSca1(cthe)
    !generate polar scattering angle  
    !
    !OUT: cthe:     polar scattering angle
    !------------------------------------------
    subroutine splineSca1(cthe)
        real(kind=rk), intent(out) :: cthe      !scattering cos(theta) angle
        real(kind=rk) :: rn
        integer :: mrn
        rn=nrn*randNum()
        mrn=int(rn)
        cthe= (mrn+1.0_rk-rn)*CSRN(mrn+1)+(rn-mrn)*CSRN(mrn+2)
    end subroutine



    !---------------------------------------------
    !splineSca(P,cthe)
    !Generate scattering matrix P using polar angle cthe
    !
    !IN: cthe:  polar angle
    !OUT:   P:  phase Matrix
    !---------------------------------------------
    pure subroutine splineSca2(P,cthe)
        real(kind=rk), intent(out) :: P(4,4)        !Scattering matrix
        real(kind=rk), intent(in) :: cthe           !scattering angle cos(theta)
        real(Kind=rk) :: rn,help
        real(kind=rk) :: tmpP(4)    
        help = cthe
        if(help>=1.0_rk) help=1.0_rk   
        if(help<=-1.0_rk) help=-1.0_rk   
        call interpolate(tmpP,xp,coeffs,acos(help))
        P=0.0_rk
        P(1,1)=tmpP(1)
        P(1,2)=tmpP(2)
        P(2,1)=tmpP(2)
        P(2,2)=tmpP(1)
        P(3,3)=tmpP(3)
        P(4,4)=tmpP(3)
        P(3,4)=tmpP(4)
        P(4,3)=-tmpP(4)
    end subroutine

    !---------------------------------------------
    !splineSElements(S,cthe)
    !Generate S matrix elements using polar angle cthe
    !
    !IN: cthe:  polar angle
    !OUT:   P:  S matrix
    !---------------------------------------------
    !Rayleigh amplitude scattering phase matrix
    pure subroutine splineSElements(S,cthe)
        complex(kind=rk), intent(out) :: S(2)       !Amplitude elements
        real(kind=rk), intent(in) :: cthe           !cos(theta)
        real(kind=rk) :: tmpS(4),help
        help = cthe
        if(help>=1.0_rk) help=1.0_rk
        if(help<=-1.0_rk) help=-1.0_rk 
        call interpolate(tmpS,xp,coeffs2,acos(help))
        S(1)=cmplx(tmpS(1),tmpS(2),kind=rk)
        S(2)=cmplx(tmpS(3),tmpS(4),kind=rk)
    end subroutine

    !---------------------------------------------
    !splinePrintData(filename)
    !print additional output regarding scatterers
    !
    !IN: filename:  name of the file into which the data
    !               is written
    !---------------------------------------------
    subroutine splinePrintData(dB,unit0)
        type(dataBlock), intent(in) :: dB           !dataBlock
        integer, intent(in) :: unit0               !into what unit the output is written
        character(len=64) :: filename1,filename2
        real(kind=rk) :: the,rd
        real(kind=rk) :: M(4,4),tmpP(4),tmpS(4)
        integer :: j0
        rd=pi/180.0_rk
        write (unit0,'(A)') "Input single-scattering phase matrix"
        write (unit0,'(A)')  "     theta P11          -P21/P11     P22/P11      P33/P11',&
     &  '      P34/P11      P44/P11      S1_real      S1_imag      S4_real      S4_imag"
        M=0.0_rk
        do j0=1,np+1
            the=(j0-1)*pi/np
            call interpolate(tmpP,xp,coeffs,the)
            M(1,1)=tmpP(1)
            M(1,2)=tmpP(2)
            M(2,1)=tmpP(2)
            M(2,2)=tmpP(1)
            M(3,3)=tmpP(3)
            M(4,4)=tmpP(3)
            M(3,4)=tmpP(4)
            M(4,3)=-tmpP(4)
            call interpolate(tmpS,xp,coeffs2,the)
            write (unit0,'(F7.1,10(1X,E13.6))') the/rd,M(1,1),-M(2,1)/M(1,1),M(2,2)/M(1,1),&
                &     M(3,3)/M(1,1),M(3,4)/M(1,1),M(4,4)/M(1,1),tmpS(1),tmpS(2),tmpS(3),tmpS(4)
        enddo
    end subroutine


end module
