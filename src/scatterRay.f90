!----------------------------------------------------
!Module to handle scattering process 
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!----------------------------------------------------
module scatterRayM
    use constants
    use scattererModule
    use mathroutines
    use rng
    use stokes
    use typedefinitions
    implicit none   
contains


    !----------------------------------------------------
    !subroutine scatterRay(I,K,EH,EV)
    !
    !Generates the scattered ray based on the Rayleigh or spline phase 
    !matrix. The basis and Stokes vectors of the incident and scattered radiation 
    !are given with respect to a fixed scattering plane in the normal coordinate 
    !system.
    !----------------------------------------------------
    subroutine scatterRay(I,K,EH,EV)
        real(kind=rk), intent(inout) :: I(4)            !in: incident out: scattered intensity
        real(kind=rk), intent(inout) :: K(3)            !in: incident out: scattered directions
        real(kind=rk), intent(out) ::EH(3),EV(3)        !scattered electric field

        real(kind=rk) :: ct,jt,tm,nu1,sphi1,cphi1,cthe,sthe,eq,eu,ea,gam,ee
        real(kind=rk) :: phi,ma,r1,mu1,phi1
        real(kind=rk) :: tmpI(4),tmpK(3),tmpEH(3),tmpEV(3)
        real(kind=rk) :: newI(4),P(4,4)

        ! Set:
        tmpI=I
        ! Cosines and sines:
        call getSphericalCoordinates2(K,r1,mu1,phi1)
        nu1=sqrt(1.0_rk-mu1**2)
        cphi1=cos(phi1)
        sphi1=sin(phi1)

        !Generate polar scattering angle, and compute scattering phase matrix:
        call scattererFunc1(cthe)
        call scattererFunc2(P,cthe)   

        !Generate azimuthal scattering angle:
        eq=-P(1,2)*tmpI(2)/(P(1,1)*tmpI(1))
        eu=-P(1,2)*tmpI(3)/(P(1,1)*tmpI(1))
        ee=sqrt(eq**2+eu**2)
        if (ee>10.0_rk**(-12)) then 
            gam=acos(eq/ee)
            if (eu<0.0_rk) gam=2.0_rk*pi-gam
            ma=4.0_rk*pi*randNum()+gam-ee*sin(gam)
            call solveKepler(ea,ee,ma)
            phi=0.5_rk*(ea-gam)
        else
            phi=2.0_rk*pi*randNum()
        endif

        !Compute the scattered Stokes vector in the normal coordinate system: 
        sthe=sqrt(1.0_rk-cthe**2)
        K(1)=sthe*cos(phi)
        K(2)=sthe*sin(phi)
        K(3)=cthe

        
        call rayToNorm(K,mu1,nu1,cphi1,sphi1)
        call updateStokes(newI,K,EH,EV,tmpI,mu1,nu1,cphi1,sphi1)
        
        !Renormalize for the final Stokes vector:
        I(:) = newI(:)*(tmpI(1)/newI(1))

    end subroutine



    !----------------------------------------------------
    !pure subroutine solveKepler(ea,ee,ma)
    !
    !see manual.
    !Kepler's equation solved by accelerated Newton's by
    !Danby. 
    !Reference: Fundametals of celestial mechanics,J. M. A. Danby, 1988
    !----------------------------------------------------
    pure subroutine solveKepler(ea,e,M)
        real(kind=rk), intent(in) :: e          !eccentricity
        real(kind=rk), intent(in) :: M          !mean anomaly
        real(kind=rk), intent(out) :: ea        !eccentric anomaly
        real(kind=rk) :: f,f1,f2,f3,dea
        real(kind=rk), parameter :: tol = 10.0_rk**(-12)
        !Initialise
        ea=M+0.85_rk*e*sign(1.0_rk,sin(M-int(M/(2.0_rk*pi))*2.0_rk*pi))
        dea=1.0_rk
        !Iterate
        do while(abs(dea)>tol)
            f3=e*cos(ea)
            f2=e*sin(ea)
            f1=1.0_rk-f3
            f=ea-f2-M
            dea=-f/f1
            dea=-f/(f1+f2*dea/2.0_rk)
            dea=-f/(f1+f2*dea/2.0_rk+f3*dea**2/6.0_rk)
            ea=ea+dea            
        end do
    end subroutine

end module 
