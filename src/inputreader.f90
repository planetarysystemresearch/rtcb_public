!------------------------------------------
!Input reader module for rtcb
!
!Uses hashtable and InputParser by Antti Penttilä, 2012, 2015
!
!There are two possible ways to give input
!1: program reads input from "input.dat"
!2: program reads input from the file given
!   through the command line
!
!SYNTAX for the input is:
!wavelength=0.71123123
!
!Possible parameters are listed below and in the
!documentation coming with the program
!
!If the program can't find "input.dat", it will 
!use default values which are written below.
!If the program fails to read or find the given file,
!the program will be terminated
!
!Copyright (C) 2016 Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!------------------------------------------

module inputReader
    !Input parser for input value handling
    use InputParser
    !get definition for dataBlock
    use typedefinitions
    use errorHandler
    implicit none

    private readInput2,filldataBlock,setDefaultValues
contains


    subroutine readInput(dB)
        type(dataBlock),intent(inout) :: dB
        call setDefaultValues()
        call readInput2()
        call filldataBlock(dB) 
    end subroutine

    !------------------------------------------
    !readInput(dB)
    !
    !Read input to dataBlock
    !
    !INOUT: dB
    !
    !Fills inputParser with default values and 
    !inputParser will overwrite them depending on the choice of input.
    !After it, these values are read to dataBlock
    !------------------------------------------
    subroutine setDefaultValues()
        character(len=64) :: tmp    
        !init inputParser
        call init_input(300)

        call setup_value("wavelength", 6.283185307179586_rk)
        call setup_value("single_scattering_albedo", 1.0_rk)
        call setup_value("mean_free_path",86.028_rk)
        call setup_value("medium_thickness_or_radius",86.7481_rk)
        call setup_value("number_of_rays",1000)
        call setup_value("min_relative_flux",0.000001_rk)
        call setup_value("max_number_of_scattering",9999)
        call setup_value("seed",0)                    
        call setup_value("threshold_for_optical_depth",50.0_rk)
        call setup_value("number_of_theta_angles",64)
        call setup_value("number_of_phi_angles",48)
        call setup_value("compute_cb",.true.)  
        call setup_value("vol_element_radius",0.0_rk)  
        call setup_value("static_structure_factor",.false.)
        call setup_value("coated",.false.)
        !PLANE/SLAB ONLY
        call setup_value("finite",.true.)
        call setup_value("phi_output_angle",0.0_rk)
        call setup_value("theta_angle_of_incidence",120.0_rk)
        call setup_value("phi_angle_of_incidence",0.0_rk)

        !FOR SPLINE PRESENTATION
        call setup_value("nrn",1000)
        call setup_value("ncm",32)
        
        !HM MEDIUM/surrounding environment of a spherical scatterer 
        call setup_value("host_medium_refractive_index_real", 1.0000_rk)
        call setup_value("host_medium_refractive_index_imag", 0.0_rk)        

        !parameters which require strings
        !Notice syntax
        write(tmp,"(A)") "rayleigh"
        call setup_value("scatterer_type",tmp,64)
        write(tmp,"(A)") "scatterer.in"
        call setup_value("load_scatterer_data_from",tmp,64)
        
      

        !OUTPUT
        write(tmp,"(A)") "details.out"
        call setup_value("details_output",tmp,64)
        write(tmp,"(A)") "rt1.out"
        call setup_value("rt_solution",tmp,64)
        write(tmp,"(A)") "rtcb1.out"
        call setup_value("cb_solution",tmp,64)
        call setup_value("output_add_additional_info",.false.)     
        call setup_value("estimate_time",.true.)
        call setup_value("I21_safety_check",.false.)
        write(tmp,"(A)") "simulation num. X"
        call setup_value("output_extra_info",tmp,64)
        
!MIE
        call setup_value("generate_mie_scatterer",.true.)
        call setup_value("scatterer_radius",0.5_rk)
        call setup_value("scatterer_real_ref",1.3_rk)
        call setup_value("scatterer_imag_ref",0.0_rk)
        call setup_value("core_radius",0.05_rk)
        call setup_value("core_real_ref",1.3_rk)
        call setup_value("core_imag_ref",0.0_rk)
        
        call setup_value("points_in_spline_presentation",180)
        call setup_value("volume_fraction",0.1_rk)
        write(tmp,"(A)") "scatterer.out"
        call setup_value("save_scatterer_data_to",tmp,64)
        call setup_value("stickiness",1000.0_rk)
        !----------------------------------------
    end subroutine



    !-----------------------------------------
    !readInput2()
    !
    !check which kind of input is given and
    !overwrite default values
    !
    !The subroutine will check if there are
    !any command line arguments. If there are none, 
    !it will try to find "input.dat". Otherwise
    !default values are used.
    !------------------------------------------
    subroutine readInput2()
        !file where input is located
        character(len=128) :: filename
        !integer for various tasks
        integer :: i
        logical :: fileExists   
        !default input location
        filename="input.dat"
        
        !check whether user has given filename
        i = command_argument_count()      
        if(i<=1) then
            !Check if input.dat exits
            inquire(file=filename,exist=fileExists)
            if(.not. fileExists) then
                write(6,*) "use default values"
                !Leave, use default values
                return
            endif
        else
            !get filename from command line
            call get_command_argument(2,filename)
            !check that file exists
            inquire(file=filename,exist=fileExists)
            if(.not. fileExists) then
                !Critical error so terminate program
                call addError("input: the file does not exists: "//filename)
                return
            endif
        endif
        write(6,*) "reading input from ",adjustl(trim(filename))
        call read_input(filename, .false. )
    end subroutine



    !-----------------------------------------
    !filldataBlock(dB)
    !
    !Get data from inputParser to datablock
    !
    !INOUT: DB: input will be put into this
    !------------------------------------------
    subroutine filldataBlock(dB)
        type(dataBlock),intent(inout) :: dB
        integer :: tmp
        tmp=64
        call get_value("wavelength", dB%wavel)
        call get_value("single_scattering_albedo", dB%ssalbedo)
        call get_value("mean_free_path",dB%ell1)
        call get_value("medium_thickness_or_radius",dB%hr)
        call get_value("number_of_rays",dB%ntot)
        call get_value("min_relative_flux",dB%Fstop)
        call get_value("max_number_of_scattering",dB%nsca)
        call get_value("seed",dB%seed)
        call get_value("threshold_for_optical_depth",dB%tau_c)
        call get_value("number_of_theta_angles",dB%nthe)
        call get_value("number_of_phi_angles",dB%nphi)
        call get_value("compute_cb",dB%compute_cb)
        call get_value("static_structure_factor",dB%ssf)
        call get_value("stickiness",dB%v)
        
        !PLANE/SLAB ONLY
        call get_value("finite",dB%finite)
        call get_value("phi_output_angle",dB%phiout)
        call get_value("theta_angle_of_incidence",dB%the0)
        call get_value("phi_angle_of_incidence",dB%phi0)
        call get_value("vol_element_radius",dB%volrad0)
        
        !SPLINE PRESENTATION
        call get_value("nrn",dB%nrn)
        call get_value("ncm",dB%ncm)
 
        !HM MEDIUM
        call get_value("host_medium_refractive_index_real", dB%mre)
        call get_value("host_medium_refractive_index_imag", dB%mim)       
                  
        !Scatterer type
        call get_value("scatterer_type",dB%scattererType,tmp)
        call get_value("load_scatterer_data_from",dB%scattererData,tmp)
          
        !output filenames
        call get_value("details_output",dB%output_details,tmp)
        call get_value("rt_solution",dB%output_rt,tmp)
        call get_value("cb_solution",dB%output_cb,tmp)
        call get_value("output_add_additional_info",dB%addInfo)
        call get_value("estimate_time",dB%estimateTime)
        call get_value("I21_safety_check",dB%I21test)
        call get_value("output_extra_info",dB%outputExt,tmp)
    

        call get_value("generate_mie_scatterer",dB%generateMieScatterer)
        call get_value("scatterer_radius",dB%scRadius)
        call get_value("scatterer_real_ref",dB%scRealRef)
        call get_value("scatterer_imag_ref",dB%scImagRef)
        call get_value("points_in_spline_presentation",dB%np)
        call get_value("volume_fraction",dB%vf)
        call get_value("save_scatterer_data_to",dB%saveScatterer,tmp)
        call get_value("core_radius",dB%coRadius)
        call get_value("core_real_ref",dB%coRealRef)
        call get_value("core_imag_ref",dB%coImagRef)
        call get_value("coated",dB%coated)
    end subroutine


end module
