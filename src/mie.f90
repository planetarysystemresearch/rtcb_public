!-----------------------------
!Generate spherical scatterers by using the Mie solution
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!-----------------------------
module mie
    use constants
    implicit none
contains

subroutine SSF_correction(v, f, theta, lambda, r0, S)
    real(kind=rk) :: f, lambda, r0
    real(kind=rk) :: theta(:)
    real(kind=rk) :: S(size(theta))

    real(kind=rk) :: n, alpha, beta, delta, u, p, C, v
    integer :: i1
    
    do i1 = 1, size(theta)
       
       n = f *3.0_rk/4.0_rk / pi /r0**3.0_rk
       
       p = 4*pi*sin(theta(i1)/2.0_rk) / lambda
       
       alpha = (1.0_rk + 2*f)**2.0_rk / (1.0_rk - f)**4.0_rk
       beta = -6*f*(1.0_rk + f/2.0_rk)**2.0_rk / (1.0_rk - f)**4.0_rk
       delta = alpha*f/2.0_rk
       !delta = f*(1.0_dp + 2*f)**2.0_dp /(2*(1.0_dp - f)**2_dp)
       u=2*p*r0

       if(abs(theta(i1)) <  1.0d-8) then
          C = -24.0_rk * f * (alpha/3.0_rk + beta/4.0_rk + delta/6.0_rk)
       else
          C = 24.0_rk *(f) * ((alpha+beta+delta)/u**2.0_rk * cos(u) &
               -(alpha+ 2*beta + 4*delta)/u**3.0_rk * sin(u) -  2*(beta+6*delta)/u**4.0_rk*cos(u) &
               + 2*beta/u**4.0_rk + 24*delta/u**5.0_rk*sin(u) + 24*delta/u**6.0_rk*(cos(u)-1.0_rk))
       end if
    
       S(i1) = 1.0_rk/(1.0_rk-C)

    end do

  end subroutine SSF_correction

  
  subroutine SSF_correction2(v, f, theta, lambda, r0, S)
    real(kind=rk) :: f, lambda, r0
    real(kind=rk) :: theta(:)
    real(kind=rk) :: S(size(theta))

    real(kind=rk) :: n, x, psi, sigma, alpha, beta, delta, u, p, C, q, t, aa, bb, cc,  v, t1(2)
    integer :: i1
    real(kind=rk) :: mu1,mu2, mu, ff
  
    print*, 'structure factor correction with v=',v
    
    
    aa = f/12.0_rk
    bb = -(v+f/(1.0_rk-f))
    cc = (1.0_rk + f/2.0_rk) / ((1.0_rk - f)**2.0_rk)

    t1(1) = (-bb + sqrt(bb**2.0_rk - 4.0_rk * aa * cc)) / (2.0_rk*aa)   
    t1(2) = (-bb - sqrt(bb**2.0_rk - 4.0_rk * aa * cc)) / (2.0_rk*aa)
    t = minval(t1)
 !   print*, t1, t
    mu = t*f*(1.0_rk-f)
    ff = (1.0_rk + 2.0_rk*f) / (f*(1.0_rk-f))
 !   print*, ff
    
    if(mu > 1.0_rk+2.0_rk*f) then 
       print*, 'error: stickiness parameter is too small '
       stop
    end if
    
    
    do i1 = 1, size(theta)
       if(theta(i1) < 1.0e-8) then 
          S(i1) = ((1.0_rk - f)**2.0_rk / (1.0_rk + 2.0_rk * f - t*f*(1.0_rk-f)))**2.0_rk
       else
       q = 4.0_rk*pi/lambda * sin(theta(i1)/2.0_rk) 
       x = q * r0
       psi = sin(x)/x
       sigma = 3.0_rk*(sin(x)/x**3.0_rk - cos(x)/x**2.0_rk)
       alpha = f/(1.0_rk-f) * ((1.0_rk -t*f + 3*f/(1.0_rk-f)) * sigma + (3.0_rk - t*(1.0_rk-f))*psi) + cos(x)
       
       beta = f/(1.0_rk-f) * x * sigma + sin(x)
       S(i1)=1.0_rk / (alpha**2.0_rk + beta**2.0_rk)
    end if
    end do

  end subroutine SSF_correction2

 subroutine depended_scattering(S, s11,s12,s33,s34, theta, Csca, Cabs, Cext, v, density, r0, lambda)
    real(kind=rk) :: s11(:),s12(:),s33(:),s34(:), theta(:)
    real(kind=rk) :: Csca, Cabs, Cext
    real(kind=rk) :: density, r0, lambda, k, cr, v

    real(kind=rk) :: S(size(s11)), int, qsca
    integer :: i1

   
    k = 2*pi/lambda
    cr = pi*r0**2.0
    qsca = Csca/(pi*r0**2.0_rk)
    
    S(:) = 1.0_rk
    call SSF_correction2(v, density, theta*pi/180.0, lambda, r0, S)
    !print*,S_out2(1,2), theta(1)
    
    int= 0.0_rk  
    do i1 = 1, size(s11)
       int = int + 2*pi*pi/(size(s11)-1) * sin(theta(i1)*pi/180.0) * s11(i1) * S(i1)
    end do
    
    Csca = int / k **2.0_rk      
    Cext = Csca + Cabs

   ! S = S/int*4.0_rk
    
    s11 = s11 * S 
    s12 = s12 * S
    s33 = s33 * S
    s34 = s34 * S
  end subroutine depended_scattering
  
    !----------------------------------------------------
    !subroutine getPSmatrix(wavel,a,mre,mim,mmed,vf,nthe,theta,M,S0,varpi,ell)
    !
    !Create data for splinePresentation by using the mie solution from Bohren and Huffman's book
    !----------------------------------------------------
  subroutine getPSmatrix(ssf, v, wavel,a_c, a, mre_c,mim_c, mre,mim,mmed,vf,nthe,theta,P,S0,varpi, &
    ell,sext,ssca,sabs, coated )
        integer, intent(in) :: nthe                             !number of scattering angles
        real(kind=rk), intent(in) :: wavel                      !wavelength
        real(kind=rk), intent(in) :: a_c, a                     !radius core, mantle
        real(kind=rk), intent(in) :: mre,mim,mmed, mre_c,mim_c  !real, imag, medium refractive index
        logical, intent(in) :: ssf                              !structure factor correction
        logical, intent(in) :: coated                           !use coated sphere
        real(kind=rk), intent(in) :: v                          !stickiness 
        real(kind=rk), intent(in) :: vf                         !volume faction
        real(kind=rk), allocatable, intent(inout) :: theta(:)   !theta angle
        real(kind=rk), allocatable, intent(inout) :: S0(:,:)    !amplitude matrix elements
        real(kind=rk), allocatable, intent(inout) :: P(:,:)     !scattering phase matrix
        real(kind=rk), intent(out) :: varpi                     !single scattering albedo
        real(kind=rk), intent(out) :: ell                       !mean free path
        real(kind=rk), intent(out) :: sext,ssca,sabs            !extincition, sca,abs cross sections

        complex(kind=rk), allocatable :: S(:,:)
        real(kind=rk),allocatable :: s11(:),s12(:),s33(:),s34(:), Sdep(:)
        real(kind=rk) :: waven,x,qext,qsca,qback,norm(2),asym(2),bin,kext, x_c
        
        real(kind=rk) :: sback,qabs,n
        complex(kind=rk) :: mrel, mrel_c
        integer :: j1
        mrel=cmplx(mre,mim,kind=rk)/mmed
        mrel_c=cmplx(mre_c,mim_c,kind=rk)/mmed
        waven=2.0_rk*pi*mmed/wavel
        x=waven*a
        x_c=waven*a_c
        qext=0.0_rk
        qsca=0.0_rk
        qback=0.0_rk
        allocate(P(4,nthe+1),S(2,nthe+1),theta(nthe+1),S0(4,nthe+1))
        allocate(Sdep(nthe+1))
        Sdep(:) = 1.0_rk
        if(coated) then
           print*,'Coated sphere:'
           print*,'core/sphere radius' ,a_c,'/',a
           if(x_c >= x) then
              print*, 'Error: Core radius should be smaller that sphere radius'
              stop
           end if
           call mieSolution2(x_c,x,mrel_c,mrel,nthe/2+1,S(1,:),S(2,:),qext,qsca,qback)
        else
           call mieSolution(x,mrel,nthe/2+1,S(1,:),S(2,:),qext,qsca,qback)
        end if
        qabs=qext-qsca    
        sext=qext*pi*a**2
        ssca=qsca*pi*a**2
        sabs=qabs*pi*a**2
        sback=qback*pi*a**2
        allocate(s11(nthe+1),s12(nthe+1),s33(nthe+1),s34(nthe+1))
       
        do j1=1,nthe+1
            theta(j1)=180.0_rk*(j1-1)/nthe
            s11(j1)=0.5_rk*(abs(s(1,j1))**2+abs(s(2,j1))**2)
            s12(j1)=-0.5_rk*(abs(s(1,j1))**2-abs(s(2,j1))**2)
            s33(j1)=real(s(2,j1)*conjg(s(1,j1)),kind=rk)
            s34(j1)=aimag(s(2,j1)*conjg(s(1,j1)))
           
        enddo
        if(ssf) call depended_scattering(Sdep, s11,s12,s33,s34, theta, ssca, sabs, sext, v, vf, a, wavel)

        S(1,:) = S(1,:)*sqrt(Sdep)
        S(2,:) = S(2,:)*sqrt(Sdep)
        
        do j1=1,nthe+1
            theta(j1)=180.0_rk*(j1-1)/nthe
            s11(j1)=0.5_rk*(abs(s(1,j1))**2+abs(s(2,j1))**2)
            s12(j1)=-0.5_rk*(abs(s(1,j1))**2-abs(s(2,j1))**2)
            s33(j1)=real(s(2,j1)*conjg(s(1,j1)),kind=rk)
            s34(j1)=aimag(s(2,j1)*conjg(s(1,j1)))
           
        enddo


        
        bin=2.0_rk*pi*0.5_rk*(1.0_rk-cos(0.5_rk*pi/nthe))
        norm(1)=(s11(1)+s11(nthe+1))/(waven**2*ssca)*bin
        norm(2)=0.0_rk
        asym(1)=(s11(1)-s11(nthe+1))/(waven**2*ssca)*bin
        asym(2)=0.0_rk
        
        do j1=2,nthe
            bin=2.0_rk*pi*(cos((j1-1.5_rk)*pi/nthe)-cos((j1-0.5_rk)*pi/nthe))
            norm(1)=norm(1)+s11(j1)/(waven**2*ssca)*bin
            norm(2)=norm(2)-s12(j1)/(waven**2*ssca)*bin
            asym(1)=asym(1)+cos((j1-1)*pi/nthe)*s11(j1)/(waven**2*ssca)*bin
            asym(2)=asym(2)-cos((j1-1)*pi/nthe)*s12(j1)/(waven**2*ssca)*bin
        enddo
        
        varpi=qsca/qext
        n=3.0_rk*vf/(4.0_rk*pi*a**3)
        kext=n*qext*pi*a**2
        ell=1.0_rk/kext

       print*, 'albedo, mfp'  
       print*,'Mie', varpi, ell
      
     
       varpi=ssca/sext
       kext=n*sext
       ell=1.0_rk/kext
       print*,'SSF', varpi, ell
       
        do j1=1,nthe+1
            P(1,j1)=4.0_rk*pi*s11(j1)/(waven**2*ssca)
            P(2,j1)=-s12(j1)/s11(j1)
            P(3,j1)=s33(j1)/s11(j1)
            P(4,j1)=s34(j1)/s11(j1)
        enddo
        
        do j1=1,nthe+1
            S0(1,j1) = real(S(1,j1))
            S0(2,j1) = aimag(S(1,j1))
            S0(3,j1) = real(S(2,j1))
            S0(4,j1) = aimag(S(2,j1))
        enddo

        deallocate(s11,s12,s33,s34,S)
    end subroutine


    !----------------------------------------------------
    !subroutine savePSToFile(theta,M,S,additionalText,filename,writeAddionalText)
    !
    !Save data created by mie module to later use.
    !Data will be saved into "filename" in a format: theta angle, Phase, Amplitude
    !If writeAddionalText=.true. , additional text will be added at the beginning of
    !the file
    !----------------------------------------------------
    subroutine savePSToFile(theta,P,S,additionalText,filename,writeAdditionalText)
        real(kind=rk), intent(in) :: theta(:)           !theta angle
        real(kind=rk), intent(in) :: P(:,:)             !Scattering phase matrix
        real(kind=rk), intent(in) :: S(:,:)             !Amplitude matrix elements
        character(len=*), intent(in) :: additionalText  !additional text
        character(len=*), intent(in) :: filename        !filename 
        logical, intent(in) :: writeAdditionalText        !if true write addional text
        integer :: j1,stream
        stream=123456
        open(unit=stream,file=adjustl(trim(filename)))
        if(writeAdditionalText) write(stream,*) "#",adjustl(trim(additionalText))
        do j1=1,size(theta)
            write(stream,*) theta(j1),P(:,j1),S(:,j1) 
        enddo
        close(stream)
    end subroutine

    !----------------------------------------------------
    !subroutine mieSolution(x,mrel,nang,s1,s2,qext,qsca,qback)
    !Solve amplitude scattering matrix of the spherical scatterer
    !
    !Original: BHMIE by Bohren and Huffman (1983).
    !Modified to accept arrays of any size and modernized.
    !----------------------------------------------------
    subroutine mieSolution(x,mrel,nang,s1,s2,qext,qsca,qback)
        real(kind=rk),intent(inout) :: qext,qsca,qback      !efficiencies
        complex(kind=rk), intent(inout) :: s1(:),s2(:)      !amplitude scattering matrix elements
        complex(kind=rk), intent(in) :: mrel                !relative refractive index
        real(kind=rk), intent(inout) :: x                   !size parameter of the scatterer
        integer, intent(in) :: nang                         !number of scattering angles

        complex(kind=rk) :: y
        real(kind=rk) :: xstop,norm(2),asym(2),psi,p
        real(kind=rk),allocatable :: amu(:),theta(:),pii(:),tau(:),pi0(:),pi1(:)
        complex(kind=rk), allocatable :: d(:)
        integer :: j1,j2,n,jj,nn,nstop,nmx
        real(kind=rk) :: apsi,apsi1,apsi0,chi,chi1
        real(kind=rk) :: fn,chi0,psi0,psi1,rn,t,ymod,dang
        complex(kind=rk) xi,xi0,xi1,bn,an
        y=x*mrel
        xstop=x+4.0_rk*x**(1.0_rk/3.0_rk)+2.0_rk
        nstop=int(xstop)
        ymod=abs(y)
        nmx=max(xstop,ymod)+15
        dang=pi/(2.0_rk*(nang-1.0_rk))
        allocate(theta(nang),amu(nang))
        allocate(pi0(nang),pi1(nang),pii(nang))
        allocate(d(nmx),tau(nang))
        do j1=1,nang
            theta(j1)=(j1-1)*dang
            amu(j1)=cos(theta(j1))
        enddo

        d(nmx)=cmplx(0.0_rk,0.0_rk,kind=rk)
        nn=nmx-1
        do n=1,nn
            rn=nmx-n+1
            d(nmx-n)=(rn/y)-(1.0_rk/(d(nmx-n+1)+rn/y))
        enddo
        
        pi0(:)=0.0_rk
        pi1(:)=1.0_rk
        s1(:)=0.0_rk
        s2(:)=0.0_rk
        
        psi0=cos(x)
        psi1=sin(x)
        chi0=-sin(x)
        chi1=cos(x)
        apsi0=psi0
        apsi1=psi1
        xi0=cmplx(apsi0,-chi0,kind=rk)
        xi1=cmplx(apsi1,-chi1,kind=rk)
        qsca=0.0_rk
        
        do n=1,nstop
            rn=n
            fn=(2.0_rk*rn+1.0_rk)/(rn*(rn+1))
            psi=(2.0_rk*rn-1.0_rk)*psi1/x-psi0
            apsi=psi
            chi=(2.0_rk*rn-1.0_rk)*chi1/x-chi0
            xi=cmplx(apsi,-chi,kind=rk)
            an=(d(n)/mrel+rn/x)*apsi-apsi1
            an=an/((d(n)/mrel+rn/x)*xi-xi1)
            bn=(mrel*d(n)+rn/x)*apsi-apsi1
            bn=bn/((mrel*d(n)+rn/x)*xi-xi1)
            qsca=qsca+(2.0_rk*rn+1.0_rk)*(abs(an)**2.0_rk+abs(bn)**2.0_rk)
            do j1=1,nang
                jj=2*nang-j1
                pii(j1)=pi1(j1)
                tau(j1)=rn*amu(j1)*pii(j1)-(rn+1.0_rk)*pi0(j1)
                p=(-1.0_rk)**(n-1)
                s1(j1)=s1(j1)+fn*(an*pii(j1)+bn*tau(j1))
                t=(-1.0_rk)**n
                s2(j1)=s2(j1)+fn*(an*tau(j1)+bn*pii(j1))
                if (j1/=jj) then
                    s1(jj)=s1(jj)+fn*(an*pii(j1)*p+bn*tau(j1)*t)
                    s2(jj)=s2(jj)+fn*(an*tau(j1)*t+bn*pii(j1)*p)
                endif
            enddo
            
            psi0=psi1
            psi1=psi
            apsi1=psi1
            chi0=chi1
            chi1=chi
            xi1=cmplx(apsi1,-chi1,kind=rk)
            
            do j1=1,nang
                pi1(j1)=((2.0_rk*rn+1.0_rk)/(rn))*amu(j1)*pii(j1)-(rn+1.0_rk)*pi0(j1)/rn
                pi0(j1)=pii(j1)
            enddo
        enddo

        qsca=2.0_rk/x**2*qsca
        qext=4.0_rk/x**2*real(s1(1),kind=rk)
        qback=4.0_rk/x**2*abs(s1(2*nang-1))**2

        deallocate(theta,amu)
        deallocate(pi0,pi1,pii)
        deallocate(d,tau)
      end subroutine mieSolution

      
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11


!******* Coated BHMIE ****************

!SUBROUTINE BHCOAT(NSTOP, XX1 ,XX2, RRFRL1, RRFRL2 ,a_n,b_n)
      subroutine mieSolution2(xx1,xx2,RRFRL1,RRFRL2,nang,s1,s2,QQEXT,QQSCA,QBACK)
! Arguments:

    real(kind=rk) :: XX1, XX2
      REAL(kind=rk) :: GSCA,QBACK,QQEXT,QQSCA,XX,YY
      COMPLEX(kind=rk) :: RRFRL1,RRFRL2
      integer :: nang
       complex(kind=rk), intent(inout) :: s1(:),s2(:) 
! Local variables:

      complex(kind=rk), PARAMETER :: II=dcmplx(0.0,1.D0)
      real(kind=rk), PARAMETER :: DEL=1.D-8

      INTEGER :: IFLAG,N,NSTOP, j1, jj
      real(kind=rk) :: CHI0Y,CHI1Y,CHIY,EN,PSI0Y,PSI1Y,PSIY,QEXT,QSCA,RN,X,Y,YSTOP, fn, dang, p, t
     COMPLEX(kind=rk) :: AMESS1,AMESS2,AMESS3,AMESS4,AN,AN1,ANCAP, &
        BN,BN1,BNCAP,BRACK, &
        CHI0X2,CHI0Y2,CHI1X2,CHI1Y2,CHIX2,CHIPX2,CHIPY2,CHIY2,CRACK,&
        D0X1,D0X2,D0Y2,D1X1,D1X2,D1Y2,DNBAR,GNBAR, &
        REFREL,RFREL1,RFREL2, &
        XBACK,XI0Y,XI1Y,XIY, & 
        X1,X2,Y2

     complex(kind=rk), allocatable :: a_n(:), b_n(:)
     real(kind=rk),allocatable :: amu(:),theta(:),pii(:),tau(:),pi0(:),pi1(:)

     !xx1 = xx2/2.0_rk
     !RRFRL1 = dcmplx(1.7125,0.0038)  !blue
     !RRFRL1 = dcmplx(1.7004,0.0008) ! red
     !RRFRL1 = dcmplx(1.313,1.0e-7)
!***********************************************************************
!
! Subroutine BHCOAT calculates Q_ext, Q_sca, Q_back, g=<cos> 
! for coated sphere.
! All bessel functions computed by upward recurrence.
! Input:
!        X = 2*PI*RCORE*REFMED/WAVEL
!        Y = 2*PI*RMANT*REFMED/WAVEL
!        RFREL1 = REFCOR/REFMED
!        RFREL2 = REFMAN/REFMED 
! where  REFCOR = complex refr.index of core)
!        REFMAN = complex refr.index of mantle)
!        REFMED = real refr.index of medium)
!        RCORE = radius of core
!        RMANT = radius of mantle
!        WAVEL = wavelength of light in ambient medium

! returns:
!        QQEXT = C_ext/pi*rmant^2
!        QQSCA = C_sca/pi*rmant^2
!        QBACK = 4*pi*(dQ_sca/dOmega)
!              = "radar backscattering efficiency factor"
!        GSCA  = <cos(theta)> for scattered power
!
! Routine BHCOAT is taken from Bohren & Huffman (1983)
! extended by Prof. Francis S. Binkowski of The University of North
! Carolina at Chapel Hill to evaluate GSCA=<cos(theta)>
! History:
! 92.11.24 (BTD) Explicit declaration of all variables
! 00.05.16 (BTD) Added IMPLICIT NONE
! 12.04.10 (FSB) Modified by Prof. Francis S. Binkowski of
!                The University of North Carolina at Chapel Hill
!                to evaluate GSCA=<cos(theta)>
! 12.06.15 (BTD) Cosmetic changes
!***********************************************************************

      allocate(theta(nang),amu(nang))
      allocate(pi0(nang),pi1(nang),pii(nang))
      allocate(tau(nang))
      dang=pi/(2.0_rk*(nang-1.0_rk))
       do j1=1,nang
            theta(j1)=(j1-1)*dang
            amu(j1)=cos(theta(j1))
        enddo
      
      X=XX1
      Y=XX2
      RFREL1=RRFRL1
      RFREL2=RRFRL2
!         -----------------------------------------------------------
!              del is the inner sphere convergence criterion
!         -----------------------------------------------------------
      X1=RFREL1*X
      X2=RFREL2*X
      Y2=RFREL2*Y
      YSTOP=Y+4.*Y**0.3333+2.0
      REFREL=RFREL2/RFREL1
      NSTOP=YSTOP

      allocate(a_n(NSTOP), b_n(NSTOP))
!         -----------------------------------------------------------
!              series terminated after nstop terms
      !         -----------------------------------------------------------

      pi0(:)=0.0_rk
      pi1(:)=1.0_rk
      s1(:)=0.0_rk
      s2(:)=0.0_rk

      
      D0X1=COS(X1)/SIN(X1)
      D0X2=COS(X2)/SIN(X2)
      D0Y2=COS(Y2)/SIN(Y2)
      PSI0Y=COS(Y)
      PSI1Y=SIN(Y)
      CHI0Y=-SIN(Y)
      CHI1Y=COS(Y)
      XI0Y=PSI0Y-II*CHI0Y
      XI1Y=PSI1Y-II*CHI1Y
      CHI0Y2=-SIN(Y2)
      CHI1Y2=COS(Y2)
      CHI0X2=-SIN(X2)
      CHI1X2=COS(X2)
      QSCA=0.0_rk
      QEXT=0.0_rk
      XBACK=dcmplx(0.0,0.0)
      IFLAG=0
      DO N=1,NSTOP
         RN=N
         fn=(2.0_rk*RN+1.0_rk)/(RN*(RN+1))
         EN=RN
         PSIY=(2.0_rk*RN-1.0_rk)*PSI1Y/Y-PSI0Y
         CHIY=(2.0_rk*RN-1.0_rk)*CHI1Y/Y-CHI0Y
         XIY=PSIY-II*CHIY
         D1Y2=1.0_rk/(RN/Y2-D0Y2)-RN/Y2
         IF(IFLAG.EQ.0)THEN

            D1X1=1.0_rk/(RN/X1-D0X1)-RN/X1
            D1X2=1.0_rk/(RN/X2-D0X2)-RN/X2
            CHIX2=(2.0_rk*RN-1.0_rk)*CHI1X2/X2-CHI0X2
            CHIY2=(2.0_rk*RN-1.0_rk)*CHI1Y2/Y2-CHI0Y2
            CHIPX2=CHI1X2-RN*CHIX2/X2
            CHIPY2=CHI1Y2-RN*CHIY2/Y2
            ANCAP=REFREL*D1X1-D1X2
            ANCAP=ANCAP/(REFREL*D1X1*CHIX2-CHIPX2)
            ANCAP=ANCAP/(CHIX2*D1X2-CHIPX2)
            BRACK=ANCAP*(CHIY2*D1Y2-CHIPY2)
            BNCAP=REFREL*D1X2-D1X1
            BNCAP=BNCAP/(REFREL*CHIPX2-D1X1*CHIX2)
            BNCAP=BNCAP/(CHIX2*D1X2-CHIPX2)
            CRACK=BNCAP*(CHIY2*D1Y2-CHIPY2)
            AMESS1=BRACK*CHIPY2
            AMESS2=BRACK*CHIY2
            AMESS3=CRACK*CHIPY2
            AMESS4=CRACK*CHIY2

         ENDIF ! test on iflag.eq.0

         IF(ABS(AMESS1).LT.DEL*ABS(D1Y2).AND. &
           ABS(AMESS2).LT.DEL.AND. &
           ABS(AMESS3).LT.DEL*ABS(D1Y2).AND. &
           ABS(AMESS4).LT.DEL)THEN

! convergence for inner sphere

            BRACK=dcmplx(0.0,0.0)
            CRACK=dcmplx(0.0,0.0)
            IFLAG=1
         ELSE

! no convergence yet

            IFLAG=0

         ENDIF
         DNBAR=D1Y2-BRACK*CHIPY2
         DNBAR=DNBAR/(1.0_rk-BRACK*CHIY2)
         GNBAR=D1Y2-CRACK*CHIPY2
         GNBAR=GNBAR/(1.0_rk-CRACK*CHIY2)

! store previous values of an and bn for use in computation of 
! g=<cos(theta)>

         IF(N.GT.1)THEN
            AN1=AN
            BN1=BN

            a_n(N-1) = -AN
            b_n(N-1) = -BN
         ENDIF

! update an and bn

         AN=(DNBAR/RFREL2+RN/Y)*PSIY-PSI1Y
         AN=AN/((DNBAR/RFREL2+RN/Y)*XIY-XI1Y)
         BN=(RFREL2*GNBAR+RN/Y)*PSIY-PSI1Y
         BN=BN/((RFREL2*GNBAR+RN/Y)*XIY-XI1Y)

! calculate sums for qsca,qext,xback

         QSCA=QSCA+(2.0*RN+1.0)*(ABS(AN)*ABS(AN)+ABS(BN)*ABS(BN))
         XBACK=XBACK+(2.0*RN+1.0)*(-1.)**N*(AN-BN)
         QEXT=QEXT+(2.0*RN+1.0)*(DBLE(AN)+DBLE(BN))

! (FSB) calculate the sum for the asymmetry factor

         GSCA=GSCA+((2.0*EN+1.)/(EN*(EN+1.0)))* &
              (REAL(AN)*REAL(BN)+IMAG(AN)*IMAG(BN))
         
         IF(N.GT.1)THEN
            GSCA=GSCA+((EN-1.)*(EN+1.)/EN)* &
                (REAL(AN1)*REAL(AN)+IMAG(AN1)*IMAG(AN)+ &
                 REAL(BN1)*REAL(BN)+IMAG(BN1)*IMAG(BN))
         ENDIF

          do j1=1,nang
             jj=2*nang-j1
             pii(j1)=pi1(j1)
             tau(j1)=rn*amu(j1)*pii(j1)-(rn+1.0_rk)*pi0(j1)
             p=(-1.0_rk)**(n-1)
             s1(j1)=s1(j1)+fn*(an*pii(j1)+bn*tau(j1))
             t=(-1.0_rk)**n
             s2(j1)=s2(j1)+fn*(an*tau(j1)+bn*pii(j1))
             if (j1/=jj) then
                s1(jj)=s1(jj)+fn*(an*pii(j1)*p+bn*tau(j1)*t)
                s2(jj)=s2(jj)+fn*(an*tau(j1)*t+bn*pii(j1)*p)
             endif             
          end do
         
! continue update for next iteration

         PSI0Y=PSI1Y
         PSI1Y=PSIY
         CHI0Y=CHI1Y
         CHI1Y=CHIY
         XI1Y=PSI1Y-II*CHI1Y
         CHI0X2=CHI1X2
         CHI1X2=CHIX2
         CHI0Y2=CHI1Y2
         CHI1Y2=CHIY2
         D0X1=D1X1
         D0X2=D1X2
         D0Y2=D1Y2

          do j1=1,nang
             pi1(j1)=((2.0_rk*rn+1.0_rk)/(rn))*amu(j1)*pii(j1)-(rn+1.0_rk)*pi0(j1)/rn
             pi0(j1)=pii(j1)
          enddo
      ENDDO

! have summed sufficient terms
! now compute QQSCA,QQEXT,QBACK, and GSCA
      
      QQSCA=(2.0/(Y*Y))*QSCA
      QQEXT=(2.0/(Y*Y))*QEXT
      QBACK=(ABS(XBACK))**2
      QBACK=(1.0/(Y*Y))*QBACK
      GSCA=2.0*GSCA/QSCA

      deallocate(theta,amu)
      deallocate(pi0,pi1,pii)
      deallocate(tau)
      
    END SUBROUTINE miesolution2


    
end module 
