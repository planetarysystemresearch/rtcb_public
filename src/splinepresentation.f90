!------------------------------------------
!Scatterer which attributes are read from
!spline presentation. Requires two
!additional files scatterer_file1(default=pmie.in) and
!scatterer_file2(default=smie.in)
!
!This module is used through scattererModule
!if selected scatterer_type=splinePresentation
!
!Notice that this module is split into two files
!(other one is splinepresentation2) and 
!the rest of the code is attached using include
!because otherwise this module would be
!too long. splinepresentation2 has all the
!procedures used for preparation purposes
!while splinepresentation has the interface (scattererModule)
!related procedures.
!
!Tables
!XP(1:np+1): covers angles from 0:pi
!coeffs(4,6,theta): coeffiecients for P matrix 
!coeffs2(4,4,theta): coefficients for S matrix
!Read more about coefficients from splineTools.f08
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!------------------------------------------

module splinePresentation
    !global precision
    use constants
    !type definition
    use typedefinitions
    use rng    
    use errorHandler
    use mathroutines
    use splinetools
    use mie
    implicit none
    integer :: np,ncm,nrn
    real(kind=rk) :: dthe
    !phase matrix related
    real(kind=rk), allocatable :: XP(:),coeffs(:,:,:)
    !amplitude scattering matrix related
    real(kind=rk), allocatable :: coeffs2(:,:,:)
    real(kind=rk), allocatable :: CSRN(:)
    !work arrays for function 'pcdfspli'
    real(kind=rk), allocatable :: tempX(:),tempWX(:)
    private np,ncm,nrn,dthe,CSRN,xp,tempX,tempWX
    private PSMIE
    private bisectionMod,pcdfspli,cspspli
contains


#ifdef MPIVERSION
    include "splineMPItools.f90"
#else
    subroutine splineMPISend()
    end subroutine

    subroutine splineMPIrcv()
    end subroutine          
#endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Old splinepresentation2.f90
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!------------------------------------------
    !pmie(p,dB)
    !
    !Create a phase matrix using input. 
    !Only for preparation purposes
    !  
    !
    !IN: dB: input data
    !OUT: P: phase matrix
    !
    !Reads phase matrix from scatterer_file1(default "pmie.in")
    !------------------------------------------
     subroutine PSMIE(P,dB)
        type(dataBlock), intent(inout) :: dB                !dataBlock
        real(kind=rk), allocatable, intent(out) :: P(:,:)   !Scattering matrix data and different angles
        integer :: j1,stat,np0
        logical :: fileExists,firstLine
        real(kind=rk) :: the,p11,p12r,p33r,p34r,tmp,AA(4)
        real(kind=rk),allocatable :: tmpS(:,:),tmpP(:,:),theta(:)
        character(len=512) :: A
        character :: ch
        if(dB%generateMieScatterer) then
           
            call getPSmatrix(dB%ssf, dB%v, dB%wavel,db%coRadius,db%scRadius,           &
            &                       dB%coRealRef,dB%coImagRef,dB%scRealRef,dB%scImagRef,      &
            &                       dB%mre,dB%vf,dB%np,theta,tmpP,tmpS, &
            &                       dB%ssalbedo,db%ell1,db%sext,db%ssca,dB%sabs, db%coated)
            call savePSToFile(theta,tmpP,tmpS,dB%outputExt,dB%saveScatterer,dB%addInfo)
            !write(6,*) "single scattering albedo will be replaced"
            !write(6,*) "mean free path will be replaced"
        else
            inquire(file=adjustl(trim(dB%scattererData)),exist=fileExists)
            if(fileExists) then
                call getLineNumber(dB%scattererData,np0,firstLine)
                db%np=np0-1
                allocate(theta(dB%np+1),tmpP(4,dB%np+1),tmpS(4,dB%np+1))
                open(unit=1,file=adjustl(trim(dB%scattererData)))
                if(firstLine) read(1,*) A        
                do j1=1,dB%np+1
                    read(1,*) theta(j1),tmpP(:,j1),tmpS(:,j1)
                enddo
                close(1)
            else
                call addError("spline not found from :"//dB%scattererData)
            endif 
        endif
        theta=theta*pi/180.0_rk
        allocate(P(4,dB%np+1))
        do j1=1,dB%np+1
            P(1,j1)=tmpP(1,j1)
            P(2,j1)=-tmpP(2,j1)*tmpP(1,j1)
            P(3,j1)=tmpP(3,j1)*tmpP(1,j1)
            P(4,j1)=tmpP(4,j1)*tmpP(1,j1)   
        enddo
        dthe=pi/(1.0_rk*dB%np)
        np=dB%np
        ncm=dB%ncm
        nrn=dB%nrn
        call initPresentation(xp,coeffs2,theta,tmpS)
        deallocate(tmpS,theta,tmpP)
    end subroutine

    !------------------------------------------
    !PSPLII(P)
    !
    !Fills coeffs using phase matrix P from pmie()
    !Read description from splineTools
    !  
    !IN: P: contains data input from pmie
    !------------------------------------------
    subroutine PSPLII(P,pnorm)
        real(kind=rk),intent(in) :: P(:,:)          !Scattering matrix data at different angles
        integer :: j1
        real(kind=rk), intent(out) :: pnorm
        real(kind=rk), allocatable :: XI(:),WXI(:)
        real(kind=rk) :: R,tmpX(1)
        
        allocate(XI(ncm),WXI(ncm))
        call initPresentation(xp,coeffs,tmpX,P)
        !normalise
        call gaussLQuads(-1.0_rk,1.0_rk,XI,WXI)
        pnorm = 0.0_rk

        do j1=1,ncm
            call interpolate2(R,xp,coeffs,acos(XI(j1)))
            pnorm = pnorm+WXI(j1)*R
        end do

        do j1=1,np
            coeffs(:,:,j1)=2.0_rk*coeffs(:,:,j1)/pnorm
        enddo

        !Map scattering angle cosines with random numbers:
        call cspspliNEW(P)

        deallocate(XI,WXI)

    end subroutine


   
    !------------------------------------------
    !Modified Bisection method which finds a root 
    !for f(x)-rn=0
    !
    !2020-01-09
    !Modified to test first the area close to preceding run result
    !
    !Input:    ncm: random points total
    !              rn: how much a function has 
    !                   cumulated at the point      
    !           x01,x02: interval
    !               
    !
    !Output:   retVal: solution to f(x)-rn=0
    !------------------------------------------
    function bisectionMod(rn,x1,x2,tol,ncm) result(retVal)
        real(kind=rk), intent(in) :: rn             !see manual
        real(kind=rk), intent(in) :: tol            !tolerance
        real(kind=rk), intent(in) :: x1,x2          !lower and upper bound
        integer, intent(in) :: ncm                  !number of integration points

        real(kind=rk), save :: c = 0.0_rk
        real(kind=rk) :: a,b,retVal,f,y1,y2
        integer :: j1,nmax

        nmax = 60
        if(x1>=x2) then
            call addError('bisection condition violation.')
            return
        endif

        ! Test first range around previous answer
        a=max(x1,c-(x2-x1)*0.01_rk)
        b=min(c+(x2-x1)*0.01_rk,x2)
        y1=PCDFSPLI(a,ncm)-rn
        y2=PCDFSPLI(b,ncm)-rn
        if(sign(1.0_rk,y1) == sign(1.0_rk,y2)) then
          ! Not successful, revert to default
          a=x1
          b=x2
          y1=PCDFSPLI(a,ncm)-rn
        end if
        do j1=1,nmax
          c=(a+b)*0.5_rk
          f=PCDFSPLI(c,ncm)-rn
          if(f==0.0_rk .or. (b-a)*0.5_rk<tol) then
            retVal=c
            return            
          endif
          if(sign(1.0_rk,f)==sign(1.0_rk,y1)) then
            a=c
            y1=f        
          else
            b=c
          endif
        enddo
        
        call addError('bisection failed to find a root.')
        return
        !iee_denormal
    end function


    !---------------------------------------------
    !pcdfspli(xi,ncm)
    !
    !2020-01-10
    !Modified, removed temporary memory allocation
    !
    !IN: ncm:   number of random points
    !     xi:   point f(x1) 
    !
    !Computes the cumulative distribution function for the spline
    !scattering phase function of the spline scattering phase matrix
    !---------------------------------------------
    function pcdfspli(xi,ncm) result(retVal)
        integer,intent(in) :: ncm       !number of integration points
        real(kind=rk), intent(in) :: xi !x-value
        integer :: j1,nnt       
        real(kind=rk) :: retVal
        real(kind=rk) :: P

        nnt=16+int(0.5_rk*(xi+1.0_rk)*ncm)
        call gaussLQuads(-1.0_rk,xi,tempX(1:nnt),tempWX(1:nnt))
        retVal=0.0_rk
        do j1=1,nnt
            call interpolate2(P,xp,coeffs,acos(tempX(j1)))
            retVal=retVal+tempWX(j1)*0.5_rk*P
        enddo
        
    end function


    !---------------------------------------------
    !subroutine cspspli()
    !
    !Maps the cosines of scattering angles to random numbers for
    !the spline scattering phase function of the spline scattering 
    !phase matrix.
    !---------------------------------------------
    subroutine cspspli()
        integer :: j1, nnt
        real(kind=rk) :: rn
        real(kind=rk), parameter :: tol = (10.0_rk**(-14))

        if(.not. allocated(CSRN)) then
            allocate(CSRN(nrn+1))
        endif
        
        ! Allocate work arrays for function 'pcdfspli' which
        ! will be called in a loop inside 'bisectionMod'.
        ! Memory allocation adjusted for upper bound 1.0 in
        ! bisectionMod -call
        nnt=16+ncm
        allocate(tempX(nnt),tempWX(nnt))

        CSRN(1)=-1.0_rk
        do j1=2,nrn
            rn=(j1-1)/(nrn*1.0_rk)
            CSRN(j1)=bisectionMod(rn,-1.0_rk,1.0_rk,tol,ncm)
        enddo
        CSRN(nrn+1)=1.0_rk
        
        deallocate(tempX,tempWX)
        
    end subroutine


    !---------------------------------------------
    !subroutine getLineNumber(filename,np,additionalInfo)
    !
    !Get how many lines (np) are in the file "filename"
    !Also inform the called if the additional line is present (additionalInfo)
    !---------------------------------------------
    subroutine getLineNumber(filename,np,additionalInfo)
        integer, intent(out) :: np                !number of lines
        logical, intent(out) :: additionalInfo    !addiotional line was present
        character(len=*), intent(in) :: filename    !file name
        integer :: stat
        character :: ch
        character(len=512) :: A
        np=0
        additionalInfo=.false.
        open(unit=1,file=adjustl(trim(filename)))
        do while(.true.)
            read(1,*,iostat=stat)  A
            if(stat==0) then
                read(A,*) ch
                if(.not.ch.eq.'#') then
                    np=np+1
                else
                    additionalInfo=.true.
                endif
            else
                exit    
            endif
        enddo
        close(1)
    end subroutine
    
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Old splinepresentation2.f90 ends here
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    !------------------------------------------
    !prepareSpline(dataB)
    !
    !Prepares module's private variables before
    !simulation.  
    !
    !IN: dataB: Pass input
    !
    !Fills coeffs,coeffs2,xp,npncm,nrn etc.
    !variables. Procedures are located in splinepresentation2
    !------------------------------------------
    subroutine prepareSpline(dB)
        type(dataBlock), intent(inout) :: dB    !dataBlock
        real(kind=rk),allocatable :: P(:,:)
        real(kind=rk) :: dummy

        call PSMIE(P,dB)
        if(checkIfErrors()) return
        call psplii(P,dummy)

    end subroutine


    !------------------------------------------
    !splineSca1(cthe)
    !generate polar scattering angle  
    !
    !OUT: cthe:     polar scattering angle
    !------------------------------------------
    subroutine splineSca1(cthe)
        real(kind=rk), intent(out) :: cthe      !scattering cos(theta) angle
        real(kind=rk) :: rn
        integer :: mrn
        rn=nrn*randNum()
        mrn=int(rn)
        cthe= (mrn+1.0_rk-rn)*CSRN(mrn+1)+(rn-mrn)*CSRN(mrn+2)
    end subroutine



    !---------------------------------------------
    !splineSca(P,cthe)
    !Generate scattering matrix P using polar angle cthe
    !
    !IN: cthe:  polar angle
    !OUT:   P:  phase Matrix
    !---------------------------------------------
    pure subroutine splineSca2(P,cthe)
        real(kind=rk), intent(out) :: P(4,4)        !Scattering matrix
        real(kind=rk), intent(in) :: cthe           !scattering angle cos(theta)
        real(Kind=rk) :: help
        real(kind=rk) :: tmpP(4)    
        help = cthe
        if(help>=1.0_rk) help=1.0_rk   
        if(help<=-1.0_rk) help=-1.0_rk   
        call interpolate(tmpP,xp,coeffs,acos(help))
        P=0.0_rk
        P(1,1)=tmpP(1)
        P(1,2)=tmpP(2)
        P(2,1)=tmpP(2)
        P(2,2)=tmpP(1)
        P(3,3)=tmpP(3)
        P(4,4)=tmpP(3)
        P(3,4)=tmpP(4)
        P(4,3)=-tmpP(4)
    end subroutine

    !---------------------------------------------
    !splineSElements(S,cthe)
    !Generate S matrix elements using polar angle cthe
    !
    !IN: cthe:  polar angle
    !OUT:   P:  S matrix
    !---------------------------------------------
    !Rayleigh amplitude scattering phase matrix
    pure subroutine splineSElements(S,cthe)
        complex(kind=rk), intent(out) :: S(2)       !Amplitude elements
        real(kind=rk), intent(in) :: cthe           !cos(theta)
        real(kind=rk) :: tmpS(4),help
        help = cthe
        if(help>=1.0_rk) help=1.0_rk
        if(help<=-1.0_rk) help=-1.0_rk 
        call interpolate(tmpS,xp,coeffs2,acos(help))
        S(1)=cmplx(tmpS(1),tmpS(2),kind=rk)
        S(2)=cmplx(tmpS(3),tmpS(4),kind=rk)
    end subroutine

    !---------------------------------------------
    !splinePrintData(filename)
    !print additional output regarding scatterers
    !
    !IN: filename:  name of the file into which the data
    !               is written
    !---------------------------------------------
    subroutine splinePrintData(dB,unit0)
        type(dataBlock), intent(in) :: dB           !dataBlock
        integer, intent(in) :: unit0               !into what unit the output is written
        real(kind=rk) :: the,rd
        real(kind=rk) :: M(4,4),tmpP(4),tmpS(4)
        integer :: j0
        
        rd=pi/180.0_rk
        write (unit0,'(A)') "Input single-scattering phase matrix"
        write (unit0,'(A)')  "     theta P11          -P21/P11     P22/P11      P33/P11',&
     &  '      P34/P11      P44/P11      S1_real      S1_imag      S4_real      S4_imag"
        M=0.0_rk
        do j0=1,np+1
            the=(j0-1)*pi/np
            call interpolate(tmpP,xp,coeffs,the)
            M(1,1)=tmpP(1)
            M(1,2)=tmpP(2)
            M(2,1)=tmpP(2)
            M(2,2)=tmpP(1)
            M(3,3)=tmpP(3)
            M(4,4)=tmpP(3)
            M(3,4)=tmpP(4)
            M(4,3)=-tmpP(4)
            call interpolate(tmpS,xp,coeffs2,the)
            write (unit0,'(F7.1,10(1X,E13.6))') the/rd,M(1,1),-M(2,1)/M(1,1),M(2,2)/M(1,1),&
                &     M(3,3)/M(1,1),M(3,4)/M(1,1),M(4,4)/M(1,1),tmpS(1),tmpS(2),tmpS(3),tmpS(4)
        enddo
    end subroutine


    !---------------------------------------------
    !subroutine cspspliNEW()
    !
    !Maps the cosines of scattering angles to random numbers for
    !the spline scattering phase function of the spline scattering 
    !phase matrix. New version where list is filled with linear
    !interpolation, many magnitudes faster than the previous.
    !---------------------------------------------
    subroutine cspspliNEW(P)
        real(kind=rk),intent(in) :: P(:,:)
        integer :: j1, xj, yj
        real(kind=rk) :: rn, x
        real(kind=rk), allocatable :: xv(:), yv(:), ex(:)

        if(.not. allocated(CSRN)) then
            allocate(CSRN(nrn+1))
        endif
        
        ! Work vectors for inverting the cumulative phase distribution
        allocate(xv(nrn+1), yv(nrn+1), ex(nrn+1))
        
        ! x is cosine of phase angle, let's start
        ! filling y with phase function values
        do j1=1,nrn+1
          xv(j1)=(j1-1)/(nrn*1.0_rk)*pi
          ex(j1)=(j1-1)/(nrn*1.0_rk)
          call interpolate2(yv(j1),xp,coeffs,xv(j1))
        end do
        
        ! Weight with sine
        do j1=1,nrn+1
          yv(j1)=yv(j1)*sin(xv(j1))
        end do

        ! Accumulate
        do j1=2,nrn+1
          yv(j1)=yv(j1)+yv(j1-1)
        end do
        do j1=1,nrn+1
          xv(j1)=pi-xv(j1)
          yv(j1)=1-yv(j1)/yv(nrn+1)
        end do
        
        ! To cosine and swap
        do j1=1,nrn+1
          x=xv(j1)
          xv(j1)=yv(j1)
          yv(j1)=-cos(x)
        end do
        
        ! Reverse
        xv(:) = xv(nrn+1:1:-1)
        yv(:) = yv(nrn+1:1:-1)

        ! Finally, interpolate for constant-step x
        CSRN(1)=-1.0_rk
        xj=2
        j1=1
        do while(xj<nrn+1)
          if(ex(xj)>xv(j1)) then
            j1=j1+1
          else
            CSRN(xj) = yv(j1) + (yv(j1+1)-yv(j1)) * (xv(j1+1)-ex(xj))/(xv(j1+1)-xv(j1))
            xj=xj+1
          end if
        end do
        CSRN(nrn+1)=1.0_rk
        
    end subroutine
    
    
end module
