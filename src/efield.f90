!----------------------------------------------------
!Electric field related functions required by coherent backscattering
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!----------------------------------------------------
module efield
    use constants
    use mathroutines
    use scattererModule
    use stokes
    implicit none
    
contains

    !----------------------------------------------------
    !pure subroutine eFromStokes(E,I)
    !
    !Get Electric field E generated from Stokes I
    !----------------------------------------------------
    pure subroutine eFromStokes(E,I)
        complex(kind=rk), intent(out) :: E(2)       !Electric field
        real(kind=rk),intent(in) :: I(4)            !Stokes
        real(kind=rk), parameter :: tol = (10.0_rk**(-14))
        real(kind=rk) :: A(2),PH(2),q
        if(I(1)-abs(I(2))>tol*I(1)) then
            A(1)=sqrt(0.5_rk*(I(1)-I(2)))
            A(2)=sqrt(0.5_rk*(I(1)+I(2)))
            q=sqrt(I(1)**2-I(2)**2)
            if(I(3)>=q) then
                PH(1)=0.0_rk
                PH(2)=0.0_rk
            elseif(I(3)<=-q) then
                PH(1)=0.0_rk
                PH(2)=pi
            else
                PH(1)=0.0_rk
                PH(2)=acos(I(3)/q)
                if(I(4)>0.0_rk) PH(2)=2.0_rk*pi-PH(2)
            endif
            
        elseif(I(1)+I(2)<=tol*I(1)) then
            A(1)=sqrt(I(1))
            A(2)=0.0_rk
            PH(1)=0.0_rk
            PH(2)=0.0_rk
            
        else
            A(1)=0.0_rk    
            A(2)=sqrt(I(1))
            PH(1)=0.0_rk
            PH(2)=0.0_rk
        endif
        
        !E(1)=A(1)*exp(cmplx(0.0_rk,PH(1),kind=rk))
        E(1)=A(1)*cmplx(cos(PH(1)),sin(PH(1)),kind=rk)
        !E(2)=A(2)*exp(cmplx(0.0_rk,PH(2),kind=rk))        
        E(2)=A(2)*cmplx(cos(PH(2)),sin(PH(2)),kind=rk)
    end subroutine

    !----------------------------------------------------
    !pure subroutine stokesFromE(I,E)
    !
    !Get Stokes generated from electric field E
    !----------------------------------------------------
    pure subroutine stokesFromE(I,E)
        complex(kind=rk),intent(in) :: E(2)     !Electric field
        real(kind=rk),intent(out) :: I(4)       !Stokes
        real(kind=rk) :: qh,qv
        complex(kind=rk) :: img
        img=cmplx(0.0_rk,1.0_rk,kind=rk)
        qh=abs(E(1))**2
        qv=abs(E(2))**2
        I(1)=qh+qv
        I(2)=-qh+qv
        I(3)=E(2)*conjg(E(1))+conjg(E(2))*E(1)
        I(4)=img*(E(2)*conjg(E(1))-conjg(E(2))*E(1))
    end subroutine


    !----------------------------------------------------
    !subroutine updateE(E2,K2,EH2,EV2,E1,mu1,nu1,cphi1,sphi1)   
    !
    !Generate Electric field scattered into the direction K2.
    !Similar to updateStokes see stokes.f90.
    !----------------------------------------------------
    subroutine updateE(E2,K2,EH2,EV2,E1,mu1,nu1,cphi1,sphi1)   
        complex(kind=rk), intent(out) :: E2(2)              !outgoing Electric field
        complex(kind=rk), intent(in) :: E1(2)               !Incident Electric field     
        real(Kind=rk), intent(inout) :: EH2(3),EV2(3)       !electric field horizontal&vertical components
        real(kind=rk), intent(in) :: mu1,nu1,cphi1,sphi1    !incident direction in the normal coordinate system
        real(kind=rk), intent(in) :: K2(3)                  !scattering direction in the normal coordinate system
  
        real(kind=rk) :: EH(3),EV(3)
        real(Kind=rk) :: K(3)
        real(kind=rk) :: tmpR,muk,phik
        real(kind=rk) :: nuk,cp1,sp1,cp2,sp2
        real(kind=rk) :: P(4,4)
        complex(kind=rk) :: S(2)
        integer :: i 
        !Find emergent direction in the incident ray  
        !coordinate system from the direction in 
        !the normal coordinate system:
        K(:)=K2(:)
        
        call normToRay(K,mu1,nu1,cphi1,sphi1)
        
        !Rotation cosine and sine from the normal system to the scattering 
        !plane system, new basis vectors:
        
        call getSphericalCoordinates(K,tmpR,muk,phik)
        nuk=sqrt(1.0_rk-muk**2)
        cp1=cos(phik)
        sp1=sin(phik)
        EH(1)=sp1
        EH(2)=-cp1
        EH(3)=0.0_rk
        EV(1)=muk*cp1
        EV(2)=muk*sp1
        EV(3)=-nuk

        !Amplitude scattering matrix elements
        call scattererSElements(S,K(3))
        !rotation cosine and sine from the normal system to the
        !scattering plane system, new basis vectors
        call setEHEVK(EH2,EV2,K2)


        call rayToNorm(EH,mu1,nu1,cphi1,sphi1)
        call rayToNorm(EV,mu1,nu1,cphi1,sphi1)
        cp2=EH2(1)*EH(1)+EH2(2)*EH(2)
        sp2=EH2(1)*EV(1)+EH2(2)*EV(2)

        !perpendicular and parallel field components
        E2(1)= cp2*S(1)*( E1(1)*cp1+E1(2)*sp1) &
            & +sp2*S(2)*(-E1(1)*sp1+E1(2)*cp1)
        E2(2)=-sp2*S(1)*( E1(1)*cp1+E1(2)*sp1) &
            & +cp2*S(2)*(-E1(1)*sp1+E1(2)*cp1)
    end subroutine

    !----------------------------------------------------
    !subroutine forwardE(E2,KPATH,E0,enorm,mu0,nu0,cphi0,sphi0,jsca)
    !
    !Go through the scattering path from the second to the last scatterer
    !----------------------------------------------------
    subroutine forwardE(E2,KPATH,E0,enorm,mu0,nu0,cphi0,sphi0,jsca)
        real(kind=rk),intent(in) :: KPATH(10000,3)          !Scattering path
        real(kind=rk), intent(in) :: mu0,nu0,cphi0,sphi0    !incident angle to the second scatterer
        real(kind=rk), intent(out) :: enorm                 !normalization coeeff. from the subroutine
        complex(kind=rk), intent(in) :: E0(2)               !Electric field coming from the first scatterer
        complex(kind=rk), intent(out) :: E2(2)              !Electric field scattered from the last scatterer
        integer, intent(in) :: jsca                         !number of scattering processes

        complex(kind=rk) :: E1(2)
        real(kind=rk) :: norm0,K2(3),norm2
        real(kind=rk) :: r1,mu1,nu1,phi1,cphi1,sphi1
        real(kind=rk) :: r2,mu2,nu2,phi2,cphi2,sphi2
        integer :: j1       
        real(kind=rk) :: EH2(3),EV2(3)

        enorm=0.0_rk
        norm0=sqrt(abs(E0(1))**2+abs(E0(2))**2)
        E1(:)=E0(:)
        
        mu1=mu0
        nu1=nu0
        cphi1=cphi0
        sphi1=sphi0

        !sequence of scattering process
        do j1 =2,jsca
            !Wave vector of scattered field in the normal system:
            K2(:)=KPATH(j1,:)

            !scattering
            call updateE(E2,K2,EH2,EV2,E1,mu1,nu1,cphi1,sphi1)
            
            norm2=sqrt(abs(E2(1))**2+abs(E2(2))**2)
            E2(1)=norm0*E2(1)/norm2
            E2(2)=norm0*E2(2)/norm2
            enorm=enorm+log(norm0/norm2)
            !Compute and store sines and cosines:
            call getSphericalCoordinates(K2,r2,mu2,phi2)
            
            nu2=sqrt(1.0_rk-mu2**2)
            cphi2=cos(phi2)
            sphi2=sin(phi2)
            !Update field, and sines and cosines:
            mu1=mu2
            nu1=nu2
            cphi1=cphi2
            sphi1=sphi2
            E1(:)=E2(:)
        enddo
    end subroutine

    !----------------------------------------------------
    !subroutine backE(E2,KPATH,E0,enorm,mu0,nu0,cphi0,sphi0,jsca)
    !
    !Go through the scattering path from the last to the second scatterer
    !----------------------------------------------------
    subroutine backE(E2,KPATH,E0,enorm,mu0,nu0,cphi0,sphi0,jsca)
        real(kind=rk),intent(in) :: KPATH(10000,3)          !Scattering path
        real(kind=rk), intent(in) :: mu0,nu0,cphi0,sphi0    !incident angle to the last scatterer
        real(kind=rk), intent(out) :: enorm                 !normalization coeff. from the subroutine
        complex(kind=rk), intent(in) :: E0(2)               !Electric field coming to the last scatterer
        complex(kind=rk), intent(out) :: E2(2)              !Electric field scattered from the second scatterer
        integer, intent(in) :: jsca                         !number of scattering processes

        real(kind=rk) :: norm0
        real(kind=rk) :: r1,mu1,phi1,nu1,cphi1,sphi1
        real(kind=rk) :: r2,mu2,phi2,nu2,cphi2,sphi2
        complex(kind=rk) :: E1(2)
        real(kind=rk) :: K2(3)
        real(kind=rk) :: norm2
        real(kind=rk) :: EH2(3),EV2(3)
        integer :: j1        
        enorm=0.0_rk
        norm0=sqrt(abs(E0(1))**2+abs(E0(2))**2)
        E1(:)=E0(:)
        mu1=mu0
        nu1=nu0
        cphi1=cphi0
        sphi1=sphi0
        !sequence of scattering process
        do j1 =jsca,2,-1
            K2(:)=-KPATH(j1,:)
            call updateE(E2,K2,EH2,EV2,E1,mu1,nu1,cphi1,sphi1)
            !scattering
            norm2=sqrt(abs(E2(1))**2+abs(E2(2))**2)
            E2(:)=norm0*E2(:)/norm2
            enorm=enorm+log(norm0/norm2)
            !Compute and store sines and cosines:
            call getSphericalCoordinates(K2,r2,mu2,phi2)
            nu2=sqrt(1.0_rk-mu2**2)
            cphi2=cos(phi2)
            sphi2=sin(phi2)
            !Update field, and sines and cosines:
            mu1=mu2
            nu1=nu2
            cphi1=cphi2
            sphi1=sphi2
            E1(:)=E2(:)
        enddo
    end subroutine


end module
