!----------------------------------------------------
!RTCB
!misc module has all kinds of small subroutines. Better categorization would
!be advised because the module contains subroutines, which
!modules like stokes and efield does not need to access.
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!
!subroutine ABSORB(I,Aabs,varpi)
!subroutine allocateAccessibleData(dB,accD)
!subroutine resetAccessibleData(accD)
!subroutine RTinit(I,X,K,EH,EV,I0)
!pure subroutine setIKEHEV(I,K,EH,EV,I1,K1,EH1,EV1)
!subroutine getMyShare(jobSize,ntasks,id,ind1,ind2)
!----------------------------------------------------
module misc
    use constants
    use typedefinitions
    implicit none
contains
  

    !---------------------------------------------------
    !subroutine ABSORB(I,Aabs,varpi)
    !
    !inout:     I:  Stoke's vector
    !        Aabs:  Collected absorbed energy
    !in:    varpi:  Albedo of the single scatterer
    !---------------------------------------------------
    subroutine ABSORB(I,Aabs,varpi)
        real(kind=rk), intent(inout) :: I(4)        !Stoke's vector
        real(kind=rk), intent(inout) :: Aabs        !Collected absorbed energy
        real(kind=rk), intent(in) :: varpi          !The albedo of the single scatterer
        integer :: j1
        Aabs=Aabs+(1.0_rk-varpi)*I(1)
        I(:)=varpi*I(:)
    end subroutine

    !---------------------------------------------------
    !subroutine allocateAccessibleData(dB,accD)
    !
    !in:        dB: input
    !inout    accD: accessibleData
    !
    !allocate arrays in accessibleData accD        
    !--------------------------------------------------- 
    subroutine allocateAccessibleData(dB,accD)
        type(dataBlock), intent(in) :: dB           !input
        type(accessibleData), intent(inout) :: accD !initialize arrays in this accD
        allocate(accD%IBS(4,2,dB%ntheb+1,dB%nphib))
        allocate(accD%IRT(4,dB%nthe,dB%nphi))      
    end subroutine


    !---------------------------------------------------
    !subroutine resetAccessibleData(accD)
    !
    !inout    accD: accessibleData
    !
    !Zero all data in accessibleData accD
    !---------------------------------------------------
    subroutine resetAccessibleData(accD)
        type(accessibleData), intent(inout) :: accD !zero variables/arrays in this accD
        accD%Aref = 0.0_rk
        accD%Atra = 0.0_rk
        accD%Adt = 0.0_rk
        accD%Aabs = 0.0_rk
        accD%Astop = 0.0_rk
        accD%Aspec = 0.0_rk
        accD%IBS = 0.0_rk
        accD%IRT = 0.0_rk
    end subroutine


    !---------------------------------------------------
    !subroutine RTinit(I,X,K,EH,EV,I0)
    !
    !Initial Stokes vector, location, propagation direction, and parallel 
    !and perpendicular coordinate axes.
    !
    !I will be I0, X starts from the origin, K (the direction in the ray
    !coordinate system will be set to go along positive z-axis, and
    !horizontal EH and vertical EV electric fields will be set to
    !match.  
    !---------------------------------------------------
   
    subroutine RTinit(I,X,K,EH,EV,I0)
        real(kind=rk), intent(in) :: I0(4)              !initial stoke's vector
        real(kind=rk), intent(inout) :: I(4)            
        real(Kind=rk), intent(inout) :: X(3),K(3),EH(3),EV(3)
        integer :: ind
        
        I=I0
        X=0.0_rk
        K=0.0_rk
        EH=0.0_rk      
        EV=0.0_rk
        K(3)=1.0_rk
        EH(2)=-1.0_rk
        EV(1)=1.0_rk
    end subroutine



    
    !---------------------------------------------------
    !pure subroutine setIKEHEV(I,K,EH,EV,I1,K1,EH1,EV1)
    !Sets the Stokes vector, wave vector, and the transversal basis vectors.
    !---------------------------------------------------
    pure subroutine setIKEHEV(I,K,EH,EV,I1,K1,EH1,EV1)
        real(kind=rk), intent(out) :: I(4),K(3),EH(3),EV(3)
        real(kind=rk), intent(in) :: I1(4),K1(3),EH1(3),EV1(3)
        I(:)=I1(:)
        K(:)=K1(:)
        EH(:)=EH1(:)
        EV(:)=EV1(:)
    end subroutine

    
    !---------------------------------------------------
    !subroutine getMyShare(jobSize,ntasks,id,ind1,ind2)
    !distributes the jobs to MPI processess equally. Gives from which ray to 
    !start and where to end
    !---------------------------------------------------
    subroutine getMyShare(jobSize,ntasks,id,ind1,ind2)
        integer, intent(in) :: ntasks   !number of MPI processess
        integer, intent(in) :: id       !id of the MPI process
        integer, intent(in) :: jobSize  !the total number of rays
        integer, intent(out) :: ind1    !start from nth ray
        integer, intent(out) :: ind2    !end to nth ray
        integer :: count0,remainder
        count0=jobSize/ntasks
        remainder=mod(jobSize,ntasks)
        if(id<remainder) then
            ind1=1+id*(count0+1)
            ind2=ind1+count0
        else
            ind1=1+id*(count0)+remainder
            ind2=ind1+count0-1
        endif
    end subroutine

end module
