!----------------------------------------------------
!Collect data from the accessible data to assembled data. 
!Results are stored from stoke's vector to mueller matrix.
!Also collects the total intensities from reflection, Specular reflection,
!absorption, stopping intensity, transmitted and direct transmission.
!plane-parallel but applicable to other geometries also due to 
!not taking account any symmetry. 
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!----------------------------------------------------
module collectdata
    use constants
    use typedefinitions

contains

    !----------------------------------------------------
    !
    !----------------------------------------------------
    subroutine updateMS(aD, accD, I)
        type(assembledData), intent(inout) :: aD    !Collect temporary results to here
        type(accessibleData), intent(in) :: AccD    !temporary results from the algorithm
        real(kind=rk), intent(in) :: I(4)           !initial Stoke's vector configuration
        integer :: j1,j2,j3,k1,k2
        integer :: ntheb,nphi,nthe,nphib
        do j1=2,4
            if(I(j1)==1.0_rk .or. I(j1)==-1.0_rk) then
               
                k1=j1
                k2=I(j1)
                exit
            endif
        end do

        ntheb=size(accD%IBS,dim=3)-1
        nphib=size(accD%IBS,dim=4)
        nphi=size(accD%IRT,dim=3)
        nthe=size(accD%IRT,dim=2)
        

        !Linear polarization Q
        if(k1==2) then
            aD%Aref=aD%Aref+accD%Aref/2.0_rk
            aD%Aspec=aD%Aspec+accD%Aspec/2.0_rk
            aD%Aabs=aD%Aabs+accD%Aabs/2.0_rk
            aD%Astop=aD%Astop+accD%Astop/2.0_rk
            aD%Adt=aD%Adt+accD%Adt/2.0_rk
            aD%Atra=aD%Atra+accD%Atra/2.0_rk            
            do j2=1,nphi
                do j1=1,nthe                
                    aD%MRT(j1,j2,:,1)=&
                    & aD%MRT(j1,j2,:,1)+accD%IRT(:,j1,j2)/2.0_rk
                end do
            end do
            
            do j2=1,nphib
                do j1=1,ntheb+1
                    aD%MBS(1,j1,j2,:,1)=&
                    & aD%MBS(1,j1,j2,:,1)+accD%IBS(:,1,j1,j2)/2.0_rk
                    aD%MBS(2,j1,j2,:,1)=&
                    & aD%MBS(2,j1,j2,:,1)+accD%IBS(:,2,j1,j2)/2.0_rk
                end do
            end do
            
        endif

        do j2=1,nphi
            do j1=1,nthe
                aD%MRT(j1,j2,:,k1)=&
                & aD%MRT(j1,j2,:,k1)+k2*accD%IRT(:,j1,j2)/2.0_rk
            end do
        end do
            
        do j2=1,nphib
            do j1=1,ntheb+1
                aD%MBS(1,j1,j2,:,k1)=&
                & aD%MBS(1,j1,j2,:,k1)+k2*accD%IBS(:,1,j1,j2)/2.0_rk
                aD%MBS(2,j1,j2,:,k1)=&
                & aD%MBS(2,j1,j2,:,k1)+k2*accD%IBS(:,2,j1,j2)/2.0_rk
            end do
        end do

    end subroutine

end module
