!----------------------------------------------------
!Handle propagations inside the medium. The extinction is assumed to be exponential.
!This is the plane-parallel versio
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!----------------------------------------------------

module propagations
    use constants
    use rng
    implicit none
contains
    
    !----------------------------------------------------
    !subroutine prpgz1(X,K,tauh,finite)
    !Propagates the ray within the plane-parallel scattering 
    !and absorbing medium. This is used when entering
    !the medium
    !----------------------------------------------------
    subroutine prpgz1(X,K,tauh,finite)
        real(kind=rk), intent(inout) :: X(3)   !in: entry point, out: next scattering location 
        real(kind=rk),intent(in) :: tauh       !slab thickness (optical depth)
        real(kind=rk), intent(in) :: K(3)      !direction
        logical, intent(in) :: finite          !medium is finite
        real(kind=rk),parameter :: tol = 10.0_rk**(-24)
        real(kind=rk) :: t,rn

        !upward propagation
        if(K(3)>tol) then
            rn = randNum()
            t=-log(1.0_rk-rn+rn*exp(-abs(X(3)/K(3))))
        !downward propagation
        elseif(K(3)<-tol) then
            rn = randNum()
            if(finite) then
                t=-log(1.0_rk-rn+rn*exp(-abs((X(3)+tauh)/K(3))))
            else
                t=-log(rn)
            endif  
        !Close-to-horizontal propagation:
        else
            t=-log(randNum())
            do while(X(3)+t*K(3)>0.0_rk)
                if(finite) then
                     if(X(3)+t*K(3)<-tauh) exit
                endif
                t=-log(randNum())
            enddo  
        endif
        X(:)=X(:)+K(:)*t
    end subroutine


    !----------------------------------------------------
    !subroutine prpgz2(X,K,tauh,finite,inside)
    !Propagates the ray within the plane-parallel scattering 
    !and absorbing medium. The subroutine will tell if the
    !ray scattered out of the medium by setting inside=.false.
    !----------------------------------------------------
    subroutine prpgz2(X,K,tauh,finite,inside,volrad)
        real(kind=rk), intent(inout) :: X(3)   !in: entry point, out: next scattering location 
        real(kind=rk),intent(in) :: tauh       !slab thickness (optical depth)
        real(kind=rk), intent(in) :: K(3)      !direction
        logical, intent(in) :: finite          !medium is finite
        logical, intent(out) :: inside         !Is the ray inside?  
        real(kind=rk) :: t, inter
        real(kind=rk),intent(in) :: volrad
        inside = .true.
        t=-log(randNum())

        if(t<2*volrad) then
            inter=1/12.0_rk*pi*(4*volrad+t)*(2*volrad-t)**2
            if(randNum()>(1-(inter/(4/3.0_rk*pi*volrad**3))**(1.0_rk/1.0_rk))) then
                inside = .false.
                return
            endif
        endif





        if(finite) then
            if(X(3)+t*K(3)<=0.0_rk .and. X(3)+t*K(3)>=-tauh) then
                X(:)=X(:)+K(:)*t
            else
                inside=.false.
            endif
        else
            if(X(3)+t*K(3)<=0.0_rk) then
                X(:)=X(:)+K(:)*t
            else
                inside=.false.
            endif
        endif
    end subroutine


end module
