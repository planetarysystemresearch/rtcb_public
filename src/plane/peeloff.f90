!----------------------------------------------------
!Module peeloff contains all the functions required for
!peel-off. This module needs to be modified for each geometry.
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!
!subroutine reflrt(IDS,I0,...)
!subroutine transrt(IRT,I0,...)
!----------------------------------------------------
module peeloff
    use constants
    use geoopt
    use mathroutines
    use stokes
    implicit none

contains
    
    !----------------------------------------------------
    !subroutine reflrt(IDS,I0,X0,K0,INORM,CTHEI,STHEI,&
    !   &            CPHII,SPHII,Atmp,m,mre,tau,tau_c,&
    !   &            nthe,nphi)
    !
    !Peel-off which is done by collecting intensity at 
    !detectors located at the infinity in the location
    !determined by the normcoeff. reflrt() is for
    !backscattering direction
    !
    !HM version requires that the snell's law
    !and specular diffraction is taken account because the
    !host media will refract the wave at the boundary of the
    !host medium
    !----------------------------------------------------
    subroutine reflrt(IR,I0,X0,K0,INORM,CTHEI,STHEI,&
     &                     CPHII,SPHII,Atmp,m,mre,tau_c,&
     &                     nthe,nphi)
        complex(kind=rk), intent(in) :: m                                   !refractive index, complex
        real(kind=rk), intent(in) :: I0(4)                                  !Stoke's vector of the incident ray
        real(kind=rk), intent(in) :: mre                                    !refractive index, real 
        real(kind=rk), intent(in) :: INORM(:)                               !normalization coefficients
        real(kind=rk), intent(in) :: CTHEI(:),STHEI(:),CPHII(:),SPHII(:)    !location of the detectors
        real(kind=rk), intent(in) :: X0(3)                                  !the location of the scatterer
        real(kind=rk), intent(in) :: K0(3)                                  !incident angle
        real(kind=rk), intent(in) :: tau_c                                  !threshold for exp(tau_c)=>0
        real(kind=rk),intent(out) :: Atmp                                   !The sum of collected intensity
        real(kind=rk), intent(inout) :: IR(:,:,:)                          !intensities collected at detectors
        integer, intent(in) :: nthe,nphi                                    !number of theta and phi angle

        real(kind=rk) :: r0,mu0,phi0,nu0,cphi0,sphi0,I1(4),N(3),nk
        real(kind=rk) :: K1(3),EH1(3),EV1(3),I2(4),EH(3),EH2(3),EH3(3),EV(3)
        real(kind=rk) :: EV2(3),EV3(3),I3(4),K2(3),K3(3)
        real(kind=rk) :: next,t,K(3)
        logical :: totref
        integer :: j1,j2
        call getSphericalCoordinates2(K0,r0,mu0,phi0)
        !Cosines and sines of the local incidence:
        nu0=sqrt(1.0_rk-mu0**2)
        cphi0=cos(phi0)
        sphi0=sin(phi0)
        !refraction
        N(1)=0.0_rk
        N(2)=0.0_rk
        N(3)=-1.0_rk
        !reflection
        Atmp=0.0_rk
 
        do j1=nthe/2+1,nthe
            do j2=1,nphi
                !Stokes vector of scattered radiation
                K1(1)=STHEI(j1)*CPHII(j2)
                K1(2)=STHEI(j1)*SPHII(j2)
                K1(3)=CTHEI(j1)
                call updateStokes(I1,K1,EH1,EV1,I0,mu0,nu0,cphi0,sphi0)
                !Extinction
                t=abs(x0(3)/CTHEI(j1))
                if(t<=tau_c) then 
                    next = exp(-t)
#ifdef HM
                    !Store emergent radiation, account for extinction:
#endif     
                    I2(:)=next*I1(:)*INORM(j1)
                    IR(:,j1,j2)=IR(:,j1,j2)+I2(:)                         
                    Atmp=Atmp+I2(1)
                endif
            enddo
        enddo
    end subroutine

    !----------------------------------------------------
    !subroutine reflrt(IDS,I0,X0,K0,INORM,CTHEI,STHEI,&
    !   &            CPHII,SPHII,Atmp,m,mre,tau,tau_c,&
    !   &            nthe,nphi)
    !
    !Peel-off which is done by collecting intensity at 
    !detectors located at the infinity in the location
    !determined by the normcoeff. transrt() is for
    !forward scattering direction
    !----------------------------------------------------
    subroutine transrt(IRT,I0,X0,K0,INORM,CTHEI,STHEI, &
        &   CPHII,SPHII,Atmp,tau_c,tau,nthe,nphi)
        real(kind=rk), intent(in) :: I0(4)                                  !Stoke's vector of the incident ray
        real(kind=rk), intent(inout) :: IRT(:,:,:)                          !intensities collected at detectors
        real(kind=rk), intent(in) :: INORM(:)                               !normalization coefficients
        real(kind=rk), intent(in) :: CTHEI(:),STHEI(:),CPHII(:),SPHII(:)    !location of the detectors
        real(kind=rk), intent(in) :: X0(3)                                  !the location of the scatterer
        real(kind=rk), intent(in) :: K0(3)                                  !incident angle
        real(kind=rk), intent(in) :: tau                                    !thickness of the slab (as tau)
        real(kind=rk), intent(in) :: tau_c                                  !threshold for exp(tau_c)=>0
        real(kind=rk),intent(out) :: Atmp                                   !The sum of collected intensity
        integer, intent(in) :: nthe,nphi                                    !number of theta and phi angle
        real(kind=rk) :: K(3),I(4),I1(4),I2(4)
        real(kind=rk) :: r0,mu0,phi0,nu0,cphi0,sphi0,next,t
        real(kind=rk) :: EH(3),EV(3)
        integer :: j1,j2
        call getSphericalCoordinates2(K0,r0,mu0,phi0)
        !cosines and sines of the local incidence        
        nu0=sqrt(1.0_rk-mu0**2)
        cphi0=cos(phi0)
        sphi0=sin(phi0)
        !transmission
        Atmp=0.0_rk
        do j1=1,nthe/2
            do j2=1,nphi
                k(1)=STHEI(j1)*CPHII(j2)
                k(2)=STHEI(j1)*SPHII(j2)
                k(3)=CTHEI(j1)
                call updateStokes(I1,K,EH,EV,I0,mu0,nu0,cphi0,sphi0)
                ! Store emergent radiation, account for extinction:
                t=abs((X0(3)+tau)/CTHEI(j1))
                if(t<=tau_c) then
                    next=exp(-t)
                    I2(:) = next*I1(:)*INORM(j1)
                    IRT(:,j1,j2)= IRT(:,j1,j2)+I2(:)
                    Atmp=Atmp+I2(1)
                endif
            enddo
        enddo
    end subroutine

end module 


