!----------------------------------------------------
!Coherent Backscattering
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!----------------------------------------------------

module backscattering
    use constants
    use stokes
    use geoopt
    use efield
    use misc
    implicit none

contains
    !vector coherent backscattering by a plane-parallel medium
    subroutine reflrtcb(IBS,IA,XA,KA,II,&
     &               XPATH,KPATH,CTHB,STHB,CPHB,SPHB,&
     &               m21,mre21,tau_c,xell,mu0,nu0,cphi0,sphi0,&
     &               jsca,ntheb,nphib,finite)
        complex(kind=rk), intent(in) :: m21                 !refractive index
        real(kind=rk),intent(inout) :: IBS(:,:,:,:)         !collected intensities
        real(kind=rk), intent(in) :: XPATH(:,:)             !scattering path (locations)
        real(kind=rk), intent(in) :: KPATH(:,:)             !scattering path (directions)
        real(kind=rk), intent(in) :: KA(3)                  !incident angle previous scatterer
        real(kind=rk), intent(in) :: tau_c                  !threshold for optical depth
        real(kind=rk), intent(in) :: xell                   !mean free path
        real(kind=rk), intent(in) :: mu0,nu0,cphi0,sphi0    !incident angle of the first scatterer
        real(kind=rk), intent(in) :: XA(3)                  !location of the previous scatterer
        real(kind=rk), intent(in) :: STHB(:),CPHB(:)        !location of the detectors
        real(kind=rk), intent(in) :: CTHB(:),SPHB(:)        !location of the detectors
        real(kind=rk), intent(in) :: IA(4),II(4)            !current intensity and initial intensity
        integer, intent(in) :: nphib, ntheb                 !number of points 
        integer, intent(in) :: jsca                         !number of scattering processes currently done
        logical,intent(in) :: finite                        !semi-infinte for finite plane-parallel     

        complex(kind=rk) :: EI(2),EA(2),EB(2),EAF(2),EBF(2),EABF(2)
        real(kind=rk) :: nua,ra,mua,phia,cphia,cphib,cpsi,enorma,enormb
        real(kind=rk) :: nubd,rbd,mubd,phibd,sphibd,cphibd,IAF(4),raa
        real(kind=rk) :: nub,rb,mub,phib,c2psi,K0(3),K1(3),K2(3)
        real(kind=rk) :: KF(3),KB(3),XF(3),XB(3),KI(3),N(3)
        real(kind=rk) :: I1(4),I2(4),EH(3),EH1(3),EH2(3)
        real(kind=rk) :: EH0(3),KFE(3),EHE(3),EVE(3)
        real(kind=rk) :: EV(3),EV0(3),EV1(3),EV2(3),I0(4),IABF(4)
        real(kind=rk) :: nexta,nextb,nk,mre21,IBF(4),phd,R(4,4)
        real(kind=rk) :: taub,sphia,s2psi,sphib,spsi,taab,taua,tauab
        logical :: totref
        integer :: j1,j2

        N(1)=0.0_rk
        N(2)=0.0_rk
        N(3)=-1.0_rk
        call getSphericalCoordinates2(KA,ra,mua,phia)
        nua = sqrt(1.0_rk-mua**2)
        cphia=cos(phia)
        sphia=sin(phia) 
        KF(:)=-KPATH(1,:)

        call getSphericalCoordinates2(KF,rbd,mubd,phibd)
        
        nubd=sqrt(1.0_rk-mubd**2)
        cphibd=cos(phibd)
        sphibd=sin(phibd)

        !multiple scattering for orders higher than the first:
        if(jsca>1) then
    
            !Cosines and sines:
            KI(:)=KPATH(1,:)
            XB(:)=XPATH(1,:)
            KB(:)=-KPATH(2,:)

            call getSphericalCoordinates2(KB,rb,mub,phib)

            nub=sqrt(1.0_rk-mub**2)
            cphib=cos(phib)
            sphib=sin(phib)
     
            !Optical depths toward the exact backscattering direction:
            taua=abs(XA(3)/KF(3))
            taub=abs(XB(3)/KF(3))
            tauab=taua-taub
            
            !Stokes vector at the exact backscattering direction using  
            !forward propagation direction A:
            call updateStokes(IAF,KF,EH,EV,IA,mua,nua,cphia,sphia)

            !Fields incident on the last scatterer in the forward 
            !direction A and backward propagation direction B:
            call eFromStokes(EI,II)
            call forwardE(EA,KPATH,EI,enorma,mu0,nu0,cphi0,sphi0,jsca)
            call backE(EB,KPATH,EI,enormb,mu0,nu0,cphi0,sphi0,jsca)         
            EB(:)=EB(:)*exp(enorma-enormb)
            !Reciprocity verification and normalization of the
            !fields:
            call updateE(EAF,KF,EH,EV,EA,mua,nua,cphia,sphia)
            call updateE(EBF,KF,EH,EV,EB,mub,nub,cphib,sphib)
            
            raa=sqrt(IAF(1)/(abs(EAF(1))**2+abs(EAF(2))**2))
            EA(:)=raa*EA(:)
            EB(:)=raa*EB(:)

            !store coherent backscattering
            do j1=1,ntheb+1
                do j2=1,nphib            
                    if((j1==1) .and. (j2/=1)) cycle
#ifndef HM
                    !Special cycle condition needed
                    if (j1==ntheb+1 .and. j2/=1) cycle  
#endif
                    !Emergent directions in the normal coordinate system from the
                    !directions in the ray coordinate system:
                    KF(1)=STHB(j1)*CPHB(j2)
                    KF(2)=STHB(j1)*SPHB(j2)
                    KF(3)=CTHB(j1)
                    call rayToNorm(KF,mubd,nubd,cphibd,sphibd)
        
                    !Contributions from propagation paths A and B:
                    call updateE(EAF,KF,EH,EV,EA,mua,nua,cphia,sphia)
                    call updateE(EBF,KF,EH,EV,EB,mub,nub,cphib,sphib)

                    !Phase difference due to propagation:
                    phd=xell*((KI(1)+KF(1))*(XA(1)-XB(1))+&
                            & (KI(2)+KF(2))*(XA(2)-XB(2))+&
                            & (KI(3)+KF(3))*(XA(3)-XB(3)))                
                     
                    !Account for extinction:
                    taua=abs(XA(3)/KF(3))
                    taub=abs(XB(3)/KF(3))

                    if (taua<=tau_c) then
                        nexta=exp(-0.5_rk*taua)
                    else
                        nexta=0.0_rk
                    endif

                    if (abs(taub+tauab)<=tau_c) then
                        nextb=exp(-0.5_rk*(taub+tauab))
                    else
                        nextb=0.0_rk
                    endif
                     
                    EAF(:)=nexta*EAF(:)
                    EBF(:)=nextb*EBF(:)

                    !Reconstruction of A,B and A+B
                    call stokesFromE(IAF,EAF)
                    call stokesFromE(IBF,EBF)

                    EABF(:)=EAF(:)*exp(cmplx(0.0_rk,phd,kind=rk))+EBF(:)
                    call stokesFromE(IABF,EABF)
                    IABF(:)=0.5_rk*IABF(:)

#ifdef HM
#else
!Without hm
                    call normToRay(EH,mubd,nubd,cphibd,sphibd)
                    call normToRay(EV,mubd,nubd,cphibd,sphibd)

                    cpsi= SPHB(j2)*EH(1)-CPHB(j2)*EH(2)
                    spsi=-SPHB(j2)*EV(1)+CPHB(j2)*EV(2)
#endif
                    
                    c2psi=cpsi**2-spsi**2
                    s2psi=2.0_rk*spsi*cpsi
                        
                    !call updateRotationMatrix(R,c2psi,s2psi)
                    !call matXVec(I1,R,IABF)
                    !call matXVec(I2,R,IAF)

                    I1 = combinedRotationFunc(IABF,c2psi,s2psi)
                    I2 = combinedRotationFunc(IAF,c2psi,s2psi)
                    !Emergent radiation:
                    IBS(:,1,j1,j2)=IBS(:,1,j1,j2)+I1(:)
                    IBS(:,2,j1,j2)=IBS(:,2,j1,j2)+I2(:)
                end do 
            end do
        else

            
            ! First-order multiple scattering:
            !store coherent backscattering
            do j1=1,ntheb+1 
                do j2=1,nphib
                
                    if(j1==1 .and. j2/=1) cycle
#ifndef HM
                    !Special cycle condition needed
                    if (j1==ntheb+1 .and. j2/=1) cycle  
#endif
                       
                    KF(1)=STHB(j1)*CPHB(j2)
                    KF(2)=STHB(j1)*SPHB(j2)
                    KF(3)=CTHB(j1)
   
                    call rayToNorm(KF,mubd,nubd,cphibd,sphibd)
                    
                    call updateStokes(IAF,KF,EH,EV,IA,&
                        & mua,nua,cphia,sphia)               
                    
                    !Account for extinction (analytically 
                    !for semi-infinite media,
                    !numerically for finite media):
 
                    if(finite) then
                        taua=abs(XA(3)/KF(3))
                        if(taua<tau_c) then
                            nexta=exp(-taua)
                        else
                            nexta=0.0_rk
                        endif
                    else
                        nexta=abs(KF(3))/(abs(KF(3))+abs(KA(3)))
                    endif
                  
                    IAF(:)=nexta*IAF(:)

#ifdef HM       
!HM version    
#else
                    
                    call normToRay(EH,mubd,nubd,cphibd,sphibd)
                    call normToRay(EV,mubd,nubd,cphibd,sphibd)
                    
                    cpsi= SPHB(j2)*EH(1)-CPHB(j2)*EH(2)
                    spsi=-SPHB(j2)*EV(1)+CPHB(j2)*EV(2)
                    I2=IAF
#endif

                    c2psi=cpsi**2-spsi**2
                    s2psi=2.0_rk*spsi*cpsi
                    
                    !call updateRotationMatrix(R,c2psi,s2psi)
                    !I1 = matmul(R,I2)
                    I1=combinedRotationFunc(I2,c2psi,s2psi)
                    !Emergent radiation:
                    
                    IBS(:,1,j1,j2)=IBS(:,1,j1,j2)+I1(:)
                    IBS(:,2,j1,j2)=IBS(:,2,j1,j2)+I1(:)
                end do 
                 
            end do
           
        endif
    end subroutine

end module

