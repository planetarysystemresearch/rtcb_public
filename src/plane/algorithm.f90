!--------------------------------------------
!RT-CB plane geometry
!Markov Chain Monte Carlo solution to multiple scattering by sphere geometry
!
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!
!subroutine start(dB,aD,id,ntasks)
!subroutine estimateTime(dB,id,ntasks,frac)
!subroutine MS(dB,aD,I0,times)
!--------------------------------------------

module mcSolution
    use constants    
    use typedefinitions
    use mathroutines
    use geoopt
    use stokes
    use scatterRayM
    use propagations
    use misc
    use backscattering
    use peeloff
    use collectdata
    use rng
    implicit none
contains

    !---------------------------------------------------
    !subroutine start(dB,aD,id,ntasks)
    !in:        dB:  contains input
    !           id:  id of the MPI/serial process
    !       ntasks:  number of MPI threads
    !inout:     aD:  results will be collected into this
    !
    !
    !IS(4) has all the possible initial Stoke's vector 
    !configurations. Each of these configurations will

    !traced with ntot times meaning that if ntot=10**6
    !the total number of rays traced would be 6*10**6
    !
    !ntot will be distributed evenly to MPI threads 
    !
    !After each configuration the results from accessibleData
    !will be stored to assembledData in updateMS()
    !
    !plane geometry requires going through all the configurations
    !---------------------------------------------------
    subroutine start(dB,aD,id,ntasks)
        type(dataBlock), intent(in) :: dB           !input
        type(assembledData), intent(inout) :: aD    !collected results
        integer, intent(in) :: id                   !MPI thread id
        integer, intent(in) :: ntasks               !number of MPI threads
        type(accessibleData) :: accD
        integer :: j1,ntot,ind1,ind2
        real(kind=rk) :: I0(4)
        real(kind=rk) :: IS(6,4)
        !initial configurations
        IS(1,:) =(/1.0_rk,-1.0_rk,0.0_rk,0.0_rk/)
        IS(2,:) = (/1.0_rk,1.0_rk,0.0_rk,0.0_rk/)
        IS(3,:) = (/1.0_rk,0.0_rk,-1.0_rk,0.0_rk/)
        IS(4,:) = (/1.0_rk,0.0_rk,1.0_rk,0.0_rk/)
        IS(5,:) = (/1.0_rk,0.0_rk,0.0_rk,-1.0_rk/)
        IS(6,:) = (/1.0_rk,0.0_rk,0.0_rk,1.0_rk/)
        !allocate accessibleData
        call allocateAccessibleData(dB,accD)
        !distribute the number of rays traced (dB%ntot) equally 
        call getMyShare(dB%ntot,ntasks,id,ind1,ind2)
        ntot = ind2-ind1+1
        !run the algorithm with all configurations
        do j1=1,6
            I0(:) = IS(j1,:)
            !init albedos and empty arrays"
            call resetAccessibleData(accD)
            !run one configuration
            call MS(dB,accD,I0,ntot)
            write(6,*) id,accD%Aref  
            !save results
            call updateMS(aD,accD,I0)
        enddo
    end subroutine


    !---------------------------------------------------
    !subroutine estimateTime(dB,id,ntasks,frac)
    !in:        dB:  contains input
    !           id:  id of the MPI/serial process
    !       ntasks:  number of MPI threads
    !out:     frac:  (the total number of rays traced)/50
    !
    !
    !almost same as start() but this is can be used to
    !estimate the overall time. Each MPI thread will
    !trace 50 rays (the caller will measure the time).
    !Then the total time spent will be 
    !(time spent in estimateTime())*ntot/50
    !---------------------------------------------------
    subroutine estimateTime(dB,id,ntasks,frac)
        type(dataBlock), intent(in) :: dB           !input
        real(kind=rk), intent(out) :: frac          !ntot/50
        integer, intent(in) :: id                   !MPI thread id
        integer, intent(in) :: ntasks               !number of MPI threads
        type(accessibleData) :: accD
        integer :: j1,ntot,ind1,ind2
        real(kind=rk) :: I0(4)
        real(kind=rk) :: IS(6,4)
        !initial configurations
        IS(1,:) =(/1.0_rk,-1.0_rk,0.0_rk,0.0_rk/)
        IS(2,:) = (/1.0_rk,1.0_rk,0.0_rk,0.0_rk/)
        IS(3,:) = (/1.0_rk,0.0_rk,-1.0_rk,0.0_rk/)
        IS(4,:) = (/1.0_rk,0.0_rk,1.0_rk,0.0_rk/)
        IS(5,:) = (/1.0_rk,0.0_rk,0.0_rk,-1.0_rk/)
        IS(6,:) = (/1.0_rk,0.0_rk,0.0_rk,1.0_rk/)
        !allocate accessibleData
        call allocateAccessibleData(dB,accD)
        !distribute the number of rays traced (dB%ntot) equally 
        call getMyShare(dB%ntot,ntasks,id,ind1,ind2)
        ntot = ind2-ind1+1
        !run the algorithm with all configurations
        do j1=1,6
            I0(:) = IS(j1,:)
            !init albedos and empty arrays"
            call resetAccessibleData(accD)
            !run one configuration
            call MS(dB,accD,I0,50)
        enddo
        frac = ntot/50.0_rk
    end subroutine



    !---------------------------------------------------
    !Main algorithm
    !
    !in:        dB:     input
    !           I0:     initial Stoke's vector
    !        times:     number of rays
    !inout    accD:     collected results
    !
    !Plane-parallel:
    !Take specular diffraction if the host media is present (HM).
    !This affects the rays equally so computing this once is 
    !required. Then rays are distributed equally between omp_threads
    !and the multiple scattering algorithm may start. First
    !direct transmission is computed if there are detectors in the forward
    !scattering angles and then the ray propagates inside the material.
    !First find a new place but do not change the ray position of the ray
    !immediately. Take absorption, compute the scattering with enhanced
    !coherent backscattering, take peel-off (backscattering and/or 
    !forward scattering).
    !Change the place of the scattered ray and normalize. Continue
    !tracing the ray if it has enough energy.
    !---------------------------------------------------
    subroutine MS(dB,accD,I0,times)
        type(dataBlock), intent(in) :: dB               !input
        real(kind=rk), intent(in) :: I0(4)              !initial Stoke's vector
        integer, intent(in) :: times                    !number of rays
        type(accessibleData), intent(inout) :: accD     !collected results


        real(kind=rk) :: I(4),X(3),K(3),EH(3),EV(3)
        real(kind=rk) :: I1(4),X1(3),K1(3),EH1(3),EV1(3)
        real(kind=rk) :: X0(3),mu0,nu0,cphi0,sphi0
        real(kind=rk) :: I00(4),K00(3),EH00(3),EV00(3)
        real(kind=rk) :: KPATH(10000,3),XPATH(10000,3)
        real(kind=rk) :: nk,N(3),rtsum,Atmp,norm
        integer :: jsca,j1,iter,nphib,ntheb,nthe,nphi
        logical :: inside,totref

        !Get initial K,EH,EV
        !Change the coordinate system from the ray's coordinate system 
        !to the normal coordinate system   
        call RTinit(I,X,K,EH,EV,I0)
        X0(:)=X(:)
        ntheb = dB%ntheb
        nthe = dB%nthe
        nphib = dB%nphib
        nphi = dB%nphi
        mu0=cos(dB%the0)
        nu0=sin(dB%the0)
        cphi0=cos(dB%phi0)
        sphi0=sin(dB%phi0)
        call rayToNorm(K,mu0,nu0,cphi0,sphi0)
        call rayToNorm(EH,mu0,nu0,cphi0,sphi0)
        call rayToNorm(EV,mu0,nu0,cphi0,sphi0)
#ifdef      HM
#else
        I00 = I
        K00 = K
        EH00 = EH
        EV00 =  EV
#endif
        !Start tracing
        !$omp parallel default(private) shared(dB,times,X0,I00,K00,EH00,EV00,accD,mu0,nu0,cphi0,sphi0)
        !$omp do schedule(static)
        do iter=1,times 
            X(:)=X0(:)
            jsca=0
            call setIKEHEV(I,K,EH,EV,I00,K00,EH00,EV00)

            !Direct transmission
            if (dB%finite) then
                do j1=1,4
                    I1(j1)=exp(-dB%tau/(-K(3)))*I(j1)
                    I(j1)=I(j1)-I1(j1)
                enddo
                accD%Adt=accD%Adt+I1(1)
            endif
            !initial propagation
            call PRPGZ1(X,K,dB%tau,dB%finite)

            !Propagate conditionally:
            do while(I(1)>=dB%Fstop .and. jsca<dB%nsca)      
                jsca=jsca+1               
                KPATH(jsca,:)=K(:)
                XPATH(jsca,:)=X(:)          

                !Dependent subsequent scattering and propagation:
                inside=.false.
                do while(.not.inside)
                    I1(:)=I(:); X1(:)=X(:); K1(:)=K
                    call scatterRay(I1,K1,EH1,EV1)
                    call PRPGZ2(X1,K1,db%tau,db%finite,inside,dB%volrad) 
                enddo
              
                !Absorption:
                call ABSORB(I,accD%Aabs,dB%albedo)
                
                !Bypass due to large optical depth:
                if (.not.dB%finite .or. abs(X(3))<=dB%tau_c) then
                    !Coherent backscattering:
                    if(dB%compute_cb) then
                        call REFLRTCB(accD%IBS,I,X,K,I00, &
                        &   XPATH,KPATH,dB%CTHEB,dB%STHEB,dB%CPHIB,dB%SPHIB,&
                        &   dB%m21,dB%mre21,dB%tau_c,dB%xell,mu0,nu0, &
                        &   cphi0,sphi0,jsca,dB%ntheb,dB%nphib,dB%finite)
                    endif
                    !Peel-off, Diffuse reflection:
                    rtsum=0.0_rk
                    call REFLRT(accD%IRT,I,X,K,dB%INORM,dB%CTHEI,dB%STHEI,&
                    &  dB%CPHII,dB%SPHII,Atmp,dB%m21,dB%mre21,dB%tau_c,&
                    &  dB%nthe,dB%nphi)           
                    accD%Aref=accD%Aref+Atmp           
                    rtsum=rtsum+Atmp
                    if (dB%finite) then
                        call TRANSRT(accD%IRT,I,X,K,dB%INORM,dB%CTHEI,      &
                        &   dB%STHEI,dB%CPHII,dB%SPHII,Atmp,dB%tau_c,    &
                        &   dB%tau,dB%nthe,dB%nphi)
                        accD%Atra=accD%Atra+Atmp
                        rtsum=rtsum+Atmp
                    endif          

                    !Maintain energy conservation:
                    I(1)=I(1)-rtsum
                endif
                !Set the new position and Stokes vector generated earlier:
                norm=I1(1)          
                I1(:)=I(1)*I1(:)/norm
                call setIKEHEV(I,K,EH,EV,I1,K1,EH1,EV1)
                X(:)=X1(:)  
            enddo
            accD%Astop=accD%Astop+I(1)
        end do
        !$omp end parallel
    end subroutine

end module
