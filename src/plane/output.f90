!----------------------------------------------------
!Output
!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!----------------------------------------------------

module output
    use constants
    use typedefinitions
    use scattererModule
    implicit none
contains


    !----------------------------------------------------
    !subroutine printOutput(aD,dB)
    !
    !----------------------------------------------------
    subroutine printOutput(aD,dB)
        type(assembledData),intent(inout) :: aD     
        type(dataBlock), intent(in) :: dB           
        character(len=120) :: addString
        character(8)  :: date
        character(10) :: time
        character(5)  :: zone
        real(kind=rk) :: Aref,Aspec,Asca,Aabs,Astop,Atot,renorm,Atmp,Atra,Adt
        real(kind=rk) :: norm,rd,phiout,M(4,4),the
        integer :: kphi, nthe,nphi,nphib, ntheb
        integer :: j0,j1,j2,j3,j4
        !normalisation

        
        Aref=aD%Aref/dB%ntot
        Aspec=aD%Aspec/dB%ntot
        Adt=aD%Adt/dB%ntot
        Atra=aD%Atra/dB%ntot

        call date_and_time(DATE=date,ZONE=zone)
        call date_and_time(TIME=time)

#ifndef HM
        Asca=Aref+Atra+Adt
        write(addString,*) "#RT-CB plane-parallel,",date,",",time,",",dB%finite,",",dB%hr
#else
        Asca=Aref
        write(addString,*) "#RT-CB plane-parallel host medium,",date,",",time,",",dB%finite,",",dB%hr
#endif

        Aabs=aD%Aabs/dB%ntot
        Astop=aD%Astop/dB%ntot
        Atot=Asca+Aspec+Aabs+Astop
        rd=pi/180.0_rk

        !parameter output
        open(unit=1,file=trim(adjustl(dB%output_details)))
        write(1,*) addString        
        if(dB%addInfo) write(1,*) "#",dB%outputExt
        call writeInfoScreen(dB,-1,-1,1)
        write(1,'(A)') ""
        write(1,'(A)') "Raw albedos"
        write(1,'(A30,"=",3X,E12.5)') "Aref",Aref
        if(dB%finite) then
            write(1,'(A30,"=",3X,E12.5)') "Atra",Atra
            write(1,'(A30,"=",3X,E12.5)') "Adt",Adt
            write(1,'(A30,"=",3X,E12.5)') "Atra+Adt",Atra+Adt
        endif

#ifdef HM
        write(1,'(A30,"=",3X,E12.5)') "Aspec",Aspec
#endif

        write(1,'(A30,"=",3X,E12.5)') "Asca",Asca
        write(1,'(A30,"=",3X,E12.5)') "Aabs",Aabs
        write(1,'(A30,"=",3X,E12.5)') "Asca+Aabs",Asca+Aabs
        if(Asca+Aabs>1.0_rk) then
            write(1,'(A)') "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            write(1,'(A)') "The conservation of energy was violated. See manual section X"
            write(1,'(A)') "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
        endif
        write(1,'(A)') "Waste albedo"
        write(1,'(A30,"=",3X,E12.5)') "Astop",Astop
        write(1,'(A)') "Total albedo:"
        write(1,'(A30,"=",3X,E12.5)') "Atot",Atot
        write(1,'(A30,"=",3X,E12.5)') "1 - Atot",1.0_rk - Atot
        
        !Conditionally, include wasted energy via renormalization:
        renorm=1.0_rk
        Aref=renorm*Aref
        Asca=renorm*Asca
        Aabs=renorm*Aabs
        Atra=renorm*Atra
        Adt=renorm*Adt

        nthe = dB%nthe
        nphi = dB%nphi
        phiout = dB%phiout
        nphib = dB%nphib
        ntheb = dB%ntheb
        write(1,'(A)') ""
        write(1,'(A)') "Renormalized albedos:"
        write(1,'(A30,"=",3X,E12.5)') "Aref",Aref
        if(dB%finite) then
            write(1,'(A30,"=",3X,E12.5)') "Atra",Atra
            write(1,'(A30,"=",3X,E12.5)') "Adt",Adt
        endif
#ifdef HM
        write(1,'(A30,"=",3X,E12.5)') "Aspec",Aspec
#endif
        write(1,'(A30,"=",3X,E12.5)') "Asca",Asca
        write(1,'(A30,"=",3X,E12.5)') "Aabs",Aabs
        write(1,'(A30,"=",3X,E12.5)') "Asca+Aabs",Asca+Aabs

        do j1=1,nthe
            norm=abs(cos(dB%the0)/dB%CTHEIF(j1))*renorm/(dB%ntot*4.0_rk*dB%INORM(j1))
            do j2=1,dB%nphi
                do j3=1,4
                    do j4=1,4
                        aD%MRT(j1,j2,j3,j4)=norm*aD%MRT(j1,j2,j3,j4)
                    enddo
                enddo
                
            enddo
        enddo    
      
        norm=abs(cos(dB%the0)/dB%CTHEBF(1))*renorm/(dB%ntot*4.0_rk)   
        
        do j1=1,4
            do j2=1,4
                
                aD%MBS(1,1,1,j1,j2)=norm*aD%MBS(1,1,1,j1,j2)
                aD%MBS(2,1,1,j1,j2)=norm*aD%MBS(2,1,1,j1,j2)
   
            enddo
        enddo

       do j1=2,ntheb+1
            norm=abs(cos(dB%the0)/dB%CTHEBF(j1))*renorm/(dB%ntot*4.0_rk)
            do j2=1,nphib
                do j3=1,4 
                    aD%MBS(1,j1,j2,j3,:)=norm*aD%MBS(1,j1,j2,j3,:)
                    aD%MBS(2,j1,j2,j3,:)=norm*aD%MBS(2,j1,j2,j3,:)
                enddo
            enddo
        enddo
        !Normalization check:
         
        write (1,'(A)') "Mueller matrix normalization:"
        write (1,'(A30,"=",3X,E12.5)') "Theo",Asca


        Atmp=0.0_rk
        do j1=1,nthe
            norm=abs(dB%CTHEIF(j1)/(pi*cos(dB%the0)))*4.0_rk*pi*dB%INORM(j1)
            do j2=1,dB%nphi 
                Atmp=Atmp+norm*aD%MRT(j1,j2,1,1)
            enddo
        enddo
       
        write (1,'(A30,"=",3X,E12.5)') "Calc",Atmp
        write(1,'(A)') "miscellaneous data"
        write(1,'(A30,"=",3X,I12)') "seed (master process)",dB%seed
       
        call printScattererData(dB,1)
        close(1)


        !Complete output of the Mueller reflection and transmission
        !matrices:
        open(unit=12, file=trim(adjustl(dB%output_rt)))
        if(dB%addInfo) write(12,*) "#",dB%outputExt
        if(dB%addInfo) write(12,*) addString
        do j1=nthe,1,-1
            do j2=1,nphi
                write(12,"(F7.2,1X,F7.2,16(1X,E15.8))") &
            &   acos(dB%CTHEIF(j1))/rd,(j2-1)*360.0_rk/nphi,&
            &   (aD%MRT(j1,j2,1,j3)/abs(cos(db%the0)),j3=1,4),& 
            &   (aD%MRT(j1,j2,2,j3)/abs(cos(db%the0)),j3=1,4),&
            &   (aD%MRT(j1,j2,3,j3)/abs(cos(db%the0)),j3=1,4),& 
            &   (aD%MRT(j1,j2,4,j3)/abs(cos(db%the0)),j3=1,4)
            end do
        end do
        close(12)
        !Complete output of the intensity of reflected radiation close to the backscattering direction:
        open(unit=14, file=trim(adjustl(dB%output_cb)))
        if(dB%addInfo) write(14,*) "#",dB%outputExt
        if(dB%addInfo) write(14,*) addString

        !write(14,"(4X,A,5X,A,5X,A,3X,A,5X,A,8X,A,8X,A,9X,A,9X,A,9X,A)") &
        !& '#the','phi','M11','M11','-M21','-L21',&
        !&  'M31','N31','M41','N41'
        do j1=1,ntheb+1
            do j2=1,nphib
                     write (14,'(F8.4,1X,F7.2,32(1X,E15.8))') dB%THEBF(j1)/rd,dB%PHIB(j2)/rd,     &
                 &   (aD%MBS(1,j1,j2,1,j3)/abs(cos(dB%the0)),j3=1,4), &
                 &   (aD%MBS(1,j1,j2,2,j3)/abs(cos(dB%the0)),j3=1,4), &
                 &   (aD%MBS(1,j1,j2,3,j3)/abs(cos(dB%the0)),j3=1,4), &
                 &   (aD%MBS(1,j1,j2,4,j3)/abs(cos(dB%the0)),j3=1,4), &
                 &   (aD%MBS(2,j1,j2,1,j3)/abs(cos(dB%the0)),j3=1,4), &
                 &   (aD%MBS(2,j1,j2,2,j3)/abs(cos(dB%the0)),j3=1,4), &
                 &   (aD%MBS(2,j1,j2,3,j3)/abs(cos(dB%the0)),j3=1,4), & 
                 &   (aD%MBS(2,j1,j2,4,j3)/abs(cos(dB%the0)),j3=1,4)
            end do
        end do
        close(14)

    end subroutine


    subroutine writeInfoScreen(dB,mpiProcesses,threads,unit0)
        integer, intent(in) :: mpiProcesses     !Number of mpi processes
        integer, intent(in) :: unit0            !unit into where the output is written
        integer, intent(in) :: threads          !Number of openMP threads
        type(dataBlock), intent(in) :: dB



        write(unit0,*) "////////////////////////////"
#ifdef HM
        write(unit0,*) "RT-CB with host media"
#else
        write(unit0,*) "RT-CB"
#endif
        if(dB%finite) then
            write(unit0,*) "Finite plane-parallel (finite=.true.)"        
        else
            write(unit0,*) "Semi-infinite plane-parallel (finite=.false.)" 
        endif
       
        if(mpiProcesses/=-1 .and. threads/=-1) then
#ifdef MPIVERSION
        write(unit0,*) "MPI PROCESSES",mpiProcesses,"openMP threads",threads
#else
        write(unit0,*) "openMP threads",threads
#endif
        endif

        write(unit0,*) "////////////////////////////"
        write(unit0,'(A30,E12.5)') "wavelength", dB%wavel
        write(unit0,'(A30,E12.5)') "single_scattering_albedo", dB%ssalbedo
        write(unit0,'(A30,E12.5)') "mean_free_path",dB%ell1
        write(unit0,'(A30,E12.5)') "medium_thickness",dB%hr
        write(unit0,'(A30,E12.5)') "min_relative_flux",dB%Fstop
        write(unit0,'(A30,E12.5)') "threshold_for_optical_depth",dB%tau_c
        write(unit0,'(A30,I12)') "number_of_theta_angles",dB%nthe
        write(unit0,'(A30,I12)') "number_of_phi_angles",dB%nphi
        write(unit0,'(A30,I12)') "max_number_of_scattering",dB%nsca
        write(unit0,'(A30,I12)') "seed",dB%seed
        write(unit0,'(A30,I12)') "number_of_rays",dB%ntot
        write(unit0,'(A30,E12.5)') "vol_element_radius",dB%volrad0
        !PLANE/SLAB ONLY
        write(unit0,'(A30,E12.5)') "phi_output_angle",dB%phiout
        write(unit0,'(A30,E12.5)') "theta_angle_of_incidence",dB%the0*180.0_rk/pi
        write(unit0,'(A30,E12.5)') "phi_angle_of_incidence",dB%phi0
        write(unit0,*)

        !HM MEDIUM
        write(unit0,'(A30,E12.5)') "host_med_refractive_index_real", dB%mre
        write(unit0,'(A30,E12.5)') "host_med_refractive_index_imag", dB%mim  
        write(unit0,*)

        write(unit0,'(A30,1X,A30)') "output_details",dB%output_details
        write(unit0,'(A30,1X,A30)') "rt_solution",dB%output_rt
        write(unit0,'(A30,1X,A30)') "cb_solution",dB%output_cb
        write(unit0,'(A30,2X,L1)') "output_add_additional_info",dB%addInfo
        write(unit0,'(A30,2X,l1)') "estimate_time",dB%estimateTime
        write(unit0,'(A30,2X,A30)') "output_extra_info",dB%outputExt


        write(unit0,*) "///////////////////////////////////////"
        write(unit0,'(A30,A12)') "scatterer_type ",adjustl(trim(dB%scattererType))
        if(trim(adjustl(dB%scattererType))=="spline") then         
            !Scatterer type        
            if(.not. dB%generateMieScatterer) then
                write(unit0,"(A30)") "mean_free_path is user-defined"
                write(unit0,"(A30)") "single_scattering_albedo is user-defined" 
                write(unit0,'(A30,A12)') "load_scatterer_data_from",adjustl(trim(dB%scattererData))
            else
                write(unit0,"(A30)") "mie scatterer was generated"
                write(unit0,"(A30)") "mean_free_path is generated by the program" 
                write(unit0,"(A30)") "single_scattering_albedo is generated by the program" 
                write(unit0,'(A30,I12)') "points_in_spline_presentation",dB%np
                write(unit0,'(A30,E12.5)') "scatterer_radius",dB%scRadius
                write(unit0,'(A30,E12.5)') "scatterer_real_ref",dB%scRealRef
                write(unit0,'(A30,E12.5)') "scatterer_imag_ref",dB%scImagRef
                write(unit0,'(A30,E12.5)') "volume_fraction",dB%vf
                write(unit0,'(A30,A30)') "save_scatterer_data_to",adjustl(trim(dB%saveScatterer))
                write(unit0,'(A30,I12)') "nrn",dB%nrn
                write(unit0,'(A30,I12)') "ncm",dB%ncm

            endif
        else
            write(unit0,"(A40)") "mean_free_path is user-defined"
            write(unit0,"(A40)") "single_scattering_albedo is user-defined"
        endif




        write(unit0,*) "////////////////////////////"
    end subroutine

end module
