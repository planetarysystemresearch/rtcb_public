!Copyright (C) 2016, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt

subroutine splineMPISend()
    use mpi_f08
    real(kind=rk) :: buffer(100000)
    integer :: listOfInds(8),j1,j2,j3,ind,x,nodes,pos,ierr
    listOfInds(1) = size(XP)
    listOfInds(2) = size(coeffs,dim=1)
    listOfInds(3) = size(coeffs,dim=2)
    listOfInds(4) = size(coeffs,dim=3)
    listOfInds(5) = size(coeffs2,dim=1)
    listOfInds(6) = size(coeffs2,dim=2)
    listOfInds(7) = size(coeffs2,dim=3)
    listOfInds(8) = size(CSRN)
    ind=0
    x=0
    do j1=1,listOfInds(1)
        x=x+1        
        buffer(x)=XP(j1)        
    enddo
    do j3=1,listOfInds(4)
        do j2=1,listOfInds(3)
            do j1=1,listOfInds(2)
                x=x+1                
                buffer(x)=coeffs(j1,j2,j3)                
            enddo
        enddo
    enddo
    do j3=1,listOfInds(7)
        do j2=1,listOfInds(6)
            do j1=1,listOfInds(5)
                x=x+1
                buffer(x)=coeffs2(j1,j2,j3)
            enddo
        enddo
    enddo
    do j1=1,listOfInds(8)
        x=x+1
        buffer(x)=CSRN(j1)
    enddo 
    call MPI_COMM_SIZE(MPI_COMM_WORLD,nodes,ierr)
    do j1=1,nodes-1
        call MPI_Send(np, 1, MPI_INTEGER, j1, 0, MPI_COMM_WORLD)
        call MPI_Send(ncm, 1, MPI_INTEGER, j1, 0, MPI_COMM_WORLD)
        call MPI_Send(nrn, 1, MPI_INTEGER, j1, 0, MPI_COMM_WORLD)
        call MPI_Send(ns, 1, MPI_INTEGER, j1, 0, MPI_COMM_WORLD)
        call MPI_Send(dthe, 1, MPI_REAL8, j1, 0, MPI_COMM_WORLD)
        call MPI_Send(listOfInds,8,MPI_INTEGER,j1,0,MPI_COMM_WORLD)
        call MPI_Send(buffer, 50000, MPI_REAL8, j1, 0, MPI_COMM_WORLD)
    enddo
end subroutine

subroutine splineMPIrcv()
    use mpi_f08
    real(kind=rk) :: buffer(50000)
    integer :: listOfInds(8),ind,x,ierr,j1,j2,j3
    call MPI_Recv(np, 1, MPI_INTEGER, 0, 0, MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
    call MPI_Recv(ncm, 1, MPI_INTEGER, 0, 0, MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
    call MPI_Recv(nrn, 1, MPI_INTEGER, 0, 0, MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
    call MPI_Recv(ns, 1, MPI_INTEGER, 0, 0, MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
    call MPI_Recv(dthe, 1, MPI_REAL8, 0, 0, MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
    call MPI_Recv(listOfInds,8,MPI_INTEGER,0,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
    call MPI_Recv(buffer, 50000, MPI_REAL8, 0, 0, MPI_COMM_WORLD,MPI_STATUS_IGNORE,ierr)
    allocate(xp(listOfInds(1)))
    allocate(coeffs(listOfInds(2),listOfInds(3),listOfInds(4)))
    allocate(coeffs2(listOfInds(5),listOfInds(6),listOfInds(7)))
    allocate(CSRN(listOfInds(8)))
    x=0
    do j1=1,listOfInds(1)
        x=x+1
        XP(j1)=buffer(x)
    enddo
    do j3=1,listOfInds(4)
        do j2=1,listOfInds(3)
            do j1=1,listOfInds(2)
                x=x+1
                coeffs(j1,j2,j3)=buffer(x)
            enddo
        enddo
    enddo
    do j3=1,listOfInds(7)
        do j2=1,listOfInds(6)
            do j1=1,listOfInds(5)
                x=x+1
                coeffs2(j1,j2,j3)=buffer(x)
            enddo
        enddo
    enddo
    do j1=1,listOfInds(8)
        x=x+1
        CSRN(j1)=buffer(x)
    enddo 
end subroutine
            
