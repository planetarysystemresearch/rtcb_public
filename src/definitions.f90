!----------------------------------------------------
!RTCB
!Defines precisions and other constants.
!
!Copyright (C) 2016 Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!----------------------------------------------------

module constants
    use, intrinsic :: iso_fortran_env
    implicit none
    integer, parameter :: rk = REAL64                       !global precision(double)
    integer, parameter :: qp = REAL128
    real(kind=rk), parameter :: pi = 4.0_rk*atan(1.0_rk)    !pi
end module

!------------------------------------------
!TYPES:
!dataBlock: Keeps track of variables
!assembledData: Store final results
!accessibleData: Store local results
!
!Copyright (C) 2016 Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!
!NOTIFICATION about comments!
!CB-solution refers to intensities which are enhanced with coherent backscattering
!RT-solution refers to intensities without coherent backscattering
!------------------------------------------
module typedefinitions
    use constants
    implicit none


    !------------------------------------------
    !dataBlock
    !abbreviated int the code as dB
    !Keeps track of input and precalculated tables.
    !The policy was to create a type
    !which can transfer variables easily between
    !subroutines.
    !------------------------------------------
    type :: dataBlock 
!#MPITOOLS_SCRIPT1
        real(kind=rk) :: wavel        !wavelength
        real(kind=rk) :: mre          !medium refractive index(real)
        real(kind=rk) :: mim          !medium refractive index(imag)
        real(kind=rk) :: ssalbedo     !single-scattering albedo
        real(kind=rk) :: ell1         !mean free path
        real(kind=rk) :: Fstop        !minimum relative flux
        real(kind=rk) :: the0         !incident theta angle
        real(kind=rk) :: phi0         !incident phi angle
        real(kind=rk) :: phiout       !phi output angle
        real(kind=rk) :: tau_c        !threshold for optical depth
        real(kind=rk) :: hr           !thickness/radius
        real(kind=rk) :: volrad0      !radius of the volume element
        real(kind=rk) :: volrad      !radius of the volume element
        integer :: seed               !seed
        integer :: nrn                !number of points in cdf
        integer :: ntot               !number of rays
        integer :: nsca               !max num of scatt. processes
        integer :: ncm                !integration points in spline presen.
        logical :: finite             !medium is finite
        logical :: addInfo            !add additional info to output
        logical :: estimateTime       !estimate time
        logical :: I21test            !before run test that 
                                      !the spline presention is valid.
        logical :: compute_cb         !compute coherent backscattering
        logical :: ssf                !use static structure factor correction
        logical :: coated             !coated sphere
        
        real(kind=rk) :: mu0                !incident mu-angle
        real(kind=rk) :: nu0                !incident mu-angle
        real(kind=rk) :: cphi0              !incident cos(phi)
        real(kind=rk) :: sphi0              !incident sin(phi)
        
        real(kind=rk) :: tau                !thickness/radius as optical depth
        real(kind=rk) :: albedo             !albedo

        real(kind=rk) :: mre12              !refrel    
        real(kind=rk) :: mre21              !1/refrel
        complex(kind=rk) :: m12             !complex refractive index
        complex(kind=rk) :: m21             !1/complex refrective index

        real(kind=rk) :: waven              !wavenumber
        real(kind=rk) :: xell               !mean free path in a host media

        real(kind=rk) :: dthe               !angle between theta bins
        integer :: ns                       !number of spline
        integer :: ntheb                    !number of theta angles(cb-solution)
        integer :: nphib                    !number of phi angles(cb-solution)
        integer :: nthe                     !number of theta angles(rt-solution)
        integer :: nphi                     !number of theta angles(rt-solution)


        !Detector bins(RT-solution)
        real(kind=rk),allocatable :: CTHEIF(:)  !cos(theta)
        real(kind=rk),allocatable :: STHEIF(:)  !sin(theta)
        real(kind=rk),allocatable :: INORM(:)   !gauss-legendre weighting values used in integration
        real(kind=rk),allocatable :: STHEI(:)   !cos(theta), takes account of host media
        real(kind=rk),allocatable :: CTHEI(:)   !sin(theta), takes account of host media
        real(kind=rk),allocatable :: CPHII(:)   !cos(phi), takes account of host media
        real(kind=rk),allocatable :: SPHII(:)   !sin(phi), takes account of host media


        !Detector bins(CB-solution)
        real(kind=rk),allocatable :: THEBF(:)   !theta
        real(kind=rk),allocatable :: CTHEBF(:)  !cos(theta)
        real(kind=rk),allocatable :: STHEBF(:)  !sin(theta)
        real(kind=rk),allocatable :: STHEB(:)   !cos(theta), takes account of host media
        real(kind=rk),allocatable :: CTHEB(:)   !sin(theta), takes account of host media
        real(kind=rk),allocatable :: CPHIB(:)   !cos(phi), takes account of host media
        real(kind=rk),allocatable :: SPHIB(:)   !sin(phi), takes account of host media
        real(kind=rk),allocatable :: PHIB(:)    !phi


        !Scatterer Specific (Spline)
        real(kind=rk) :: v                          !stickiness parameter 
        real(kind=rk) :: vf                         !volume fraction
        real(kind=rk) :: scRadius                   !scatterer radius
        real(kind=rk) :: scRealRef                  !scatterer real refractive index
        real(kind=rk) :: scImagRef                  !scatterer imag refractive index
        real(kind=rk) :: coRadius                   !core radius
        real(kind=rk) :: coRealRef                  !core real refractive index
        real(kind=rk) :: coImagRef                  !core imag refractive index

        
        logical :: generateMieScatterer             !generate Mie scattering otherwise load from file
        integer :: np                               !points in spline presentation

!#MPITOOLS_SCRIPT2

        real(kind=rk) :: ssca                            !!Scatterer's scattering cross section
        real(kind=rk) :: sext                            !!Scatterer's extinction cross section
        real(kind=rk) :: sabs                            !!Scatterer's absorption cross section

        character(len=64) :: scattererData          !load scattering data
        character(len=64) :: saveScatterer          !save scatterer data

        character(len=64) :: output_details !Print input/output parameters
        character(len=64) :: output_rt      !RT-solution output filename
        character(len=64) :: output_cb      !CB-solution output filename
        character(len=64) :: scattererType  !Scatterer type
        character(len=64) :: outputExt      !extra information to output

    end type

    !------------------------------------------
    !assembledData
    !abbreviated as aD
    !
    !The store for final results.
    !When a thread has finished a job with
    !one Stokes parameter configuration, it updates 
    !these values using the gathered data(accessibleData).
    !assembledData has to be initialised before
    !starting the simulation.
    !
    !Indexing:
    !MBS(1:2,1:4,1:4,ntheb,nphib): Mueller cb-enhanced
    !MRT(1:4,1:4,nthe,nphi): Mueller rt-only
    !------------------------------------------
    type :: assembledData
        !Albedos
        real(kind=rk) :: Aref = 0.0_rk                  !reflected/scattered
        real(kind=rk) :: Atra = 0.0_rk                  !transmitted
        real(kind=rk) :: Adt = 0.0_rk                   !direct transmission
        real(kind=rk) :: Aabs = 0.0_rk                  !absorption
        real(kind=rk) :: Astop = 0.0_rk                 !leftover intensity
        real(kind=rk) :: Aspec = 0.0_rk                 !specular
        integer :: ntheb                                !number of theta angles(RT-solution)
        integer :: nphib                                !number of phi angles(RT-solution)
        integer :: nphi                                 !number of theta angles(CB-solution)
        integer :: nthe                                 !number of phi angles(CB-solution)
        integer :: seed                                 !seed of the thread/process
        real(kind=rk), allocatable :: MBS(:,:,:,:,:)    !CB-solution. Accumulated Mueller matrices
        real(kind=rk), allocatable :: MRT(:,:,:,:)      !RT-solution. Accumulated Mueller matrices
    end type


    !------------------------------------------
    !accessibleData
    !abbreviated as accD
    !
    !This type holds variables which are
    !updated frequently during the simulation.
    !Every thread must have it's own accessibleData
    !
    !Indexing:
    !IBS(1:4,1:2,ntheb,nphib): Stokes cb-enhanced
    !IRT(1:4,nthe,nphi): Mueller rt-only
    !------------------------------------------
    type :: accessibleData
        !Albedos
        real(kind=rk) :: Aref = 0.0_rk                  !reflected/scattered
        real(kind=rk) :: Atra = 0.0_rk                  !transmitted
        real(kind=rk) :: Adt = 0.0_rk                   !direct transmission
        real(kind=rk) :: Aabs = 0.0_rk                  !absorption
        real(kind=rk) :: Astop = 0.0_rk                 !leftover intensity
        real(kind=rk) :: Aspec = 0.0_rk                 !specular

        real(kind=rk), allocatable :: IBS(:,:,:,:)      !CB-solution. Accumulated intensities
        real(kind=rk), allocatable :: IRT(:,:,:)        !RT-solution. Accumulated intensities
    end type



end module






















