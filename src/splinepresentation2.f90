!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt
!------------------------------------------
    !pmie(p,dB)
    !
    !Create a phase matrix using input. 
    !Only for preparation purposes
    !  
    !
    !IN: dB: input data
    !OUT: P: phase matrix
    !
    !Reads phase matrix from scatterer_file1(default "pmie.in")
    !------------------------------------------
    subroutine PSMIE(P,dB)
        type(dataBlock), intent(inout) :: dB                !dataBlock
        real(kind=rk), allocatable, intent(out) :: P(:,:)   !Scattering matrix data and different angles
        integer :: j1,stat,np0
        logical :: fileExists,firstLine
        real(kind=rk) :: the,p11,p12r,p33r,p34r,tmp,AA(4)
        real(kind=rk),allocatable :: tmpS(:,:),tmpP(:,:),theta(:)
        character(len=512) :: A
        character :: ch
        if(dB%generateMieScatterer) then
           
            call getPSmatrix(dB%ssf, dB%v, dB%wavel,db%coRadius,db%scRadius,           &
            &                       dB%coRealRef,dB%coImagRef,dB%scRealRef,dB%scImagRef,      &
            &                       dB%mre,dB%vf,dB%np,theta,tmpP,tmpS, &
            &                       dB%ssalbedo,db%ell1,db%sext,db%ssca,dB%sabs, db%coated)
            call savePSToFile(theta,tmpP,tmpS,dB%outputExt,dB%saveScatterer,dB%addInfo)
            !write(6,*) "single scattering albedo will be replaced"
            !write(6,*) "mean free path will be replaced"
        else
            inquire(file=adjustl(trim(dB%scattererData)),exist=fileExists)
            if(fileExists) then
                call getLineNumber(dB%scattererData,np0,firstLine)
                db%np=np0-1
                allocate(theta(dB%np+1),tmpP(4,dB%np+1),tmpS(4,dB%np+1))
                open(unit=1,file=adjustl(trim(dB%scattererData)))
                if(firstLine) read(1,*) A        
                do j1=1,dB%np+1
                    read(1,*) theta(j1),tmpP(:,j1),tmpS(:,j1)
                enddo
                close(1)
            else
                call addError("spline not found from :"//dB%scattererData)
            endif 
        endif
        theta=theta*pi/180.0_rk
        allocate(P(4,dB%np+1))
        do j1=1,dB%np+1
            P(1,j1)=tmpP(1,j1)
            P(2,j1)=-tmpP(2,j1)*tmpP(1,j1)
            P(3,j1)=tmpP(3,j1)*tmpP(1,j1)
            P(4,j1)=tmpP(4,j1)*tmpP(1,j1)   
        enddo
        dthe=pi/(1.0_rk*dB%np)
        np=dB%np
        ncm=dB%ncm
        nrn=dB%nrn
        call initPresentation(xp,coeffs2,theta,tmpS)
        deallocate(tmpS,theta,tmpP)
    end subroutine

    !------------------------------------------
    !PSPLII(P)
    !
    !Fills coeffs using phase matrix P from pmie()
    !Read description from splineTools
    !  
    !IN: P: contains data input from pmie
    !------------------------------------------
    subroutine PSPLII(P,pnorm)
        real(kind=rk),intent(in) :: P(:,:)          !Scattering matrix data at different angles
        integer :: j1,j2,j3
        real(kind=rk), intent(out) :: pnorm
        real(kind=rk), allocatable :: XI(:),WXI(:)
        real(kind=rk) :: R,tmpP(6),tmpVal,tmpX(1)
        integer :: n
        allocate(XI(ncm),WXI(ncm))   
        call initPresentation(xp,coeffs,tmpX,P)       
        !normalise
        call gaussLQuads(-1.0_rk,1.0_rk,XI,WXI)
        pnorm = 0.0_rk

        do j1=1,ncm
            call interpolate2(R,xp,coeffs,acos(XI(j1)))
            pnorm = pnorm+WXI(j1)*R
        end do

        do j1=1,np
            coeffs(:,:,j1)=2.0_rk*coeffs(:,:,j1)/pnorm
        enddo

        !Map scattering angle cosines with random numbers:
        call cspspli()
        deallocate(XI,WXI)
    end subroutine


   
    !------------------------------------------
    !Modified Bisection method which finds a root 
    !for f(x)-rn=0
    !
    !Input:    ncm: random points total
    !              rn: how much a function has 
    !                   cumulated at the point      
    !           x01,x02: interval
    !               
    !
    !Output:   retVal: solution to f(x)-rn=0
    !------------------------------------------
    function bisectionMod(rn,x1,x2,tol,ncm) result(retVal)
        real(kind=rk), intent(in) :: rn             !see manual
        real(kind=rk), intent(in) :: tol            !tolerance
        real(kind=rk), intent(in) :: x1,x2          !lower and upper bound
        integer, intent(in) :: ncm                  !number of integration points

        real(kind=rk) :: dx,fmid
        real(kind=rk) :: a,b,c,retVal,f,y1
        integer :: j1,nmax

        nmax = 60
        if(x1>=x2) then
            call addError('bisection condition violation.')
            return
        endif
        a=x1
        b=x2
        y1=PCDFSPLI(a,ncm)-rn
        do j1=1,nmax
            c=(a+b)*0.5_rk
            f=PCDFSPLI(c,ncm)-rn
            if(f==0.0_rk .or. (b-a)*0.5_rk<tol) then
                retVal=c
                return            
            endif
            if(sign(1.0_rk,f)==sign(1.0_rk,y1)) then
                a=c
                y1=f        
            else
                b=c
            endif 
        enddo
        call addError('bisection failed to find a root.')
        return
        !iee_denormal
    end function


    !---------------------------------------------
    !pcdfspli(xi,ncm)
    !
    !IN: ncm:   number of random points
    !     xi:   point f(x1) 
    !
    !Computes the cumulative distribution function for the spline
    !scattering phase function of the spline scattering phase matrix
    !---------------------------------------------
    function pcdfspli(xi,ncm) result(retVal)
        integer,intent(in) :: ncm       !number of integration points
        real(kind=rk), intent(in) :: xi !x-value
        integer :: j1,nnt       
        real(kind=rk) :: retVal
        real(kind=rk) :: P
        real(kind=rk), allocatable :: X(:),WX(:)
        nnt=16+int(0.5_rk*(xi+1.0_rk)*ncm)
        allocate(X(nnt),WX(nnt))
        call gaussLQuads(-1.0_rk,xi,X,WX)
        retVal=0.0_rk
        do j1=1,nnt
            call interpolate2(P,xp,coeffs,acos(X(j1)))
            retVal=retVal+WX(j1)*0.5_rk*P
        enddo
        deallocate(X,WX)
    end function


    !---------------------------------------------
    !subroutine cspspli()
    !
    !Maps the cosines of scattering angles to random numbers for
    !the spline scattering phase function of the spline scattering 
    !phase matrix.
    !---------------------------------------------
    subroutine cspspli()
        integer :: j1
        real(kind=rk) :: rn
        real(kind=rk), parameter :: tol = (10.0_rk**(-14))

        if(.not. allocated(CSRN)) then
            allocate(CSRN(nrn+1))
        endif
        
        CSRN(1)=-1.0_rk
        do j1=2,nrn
            rn=(j1-1)/(nrn*1.0_rk)
            CSRN(j1)=bisectionMod(rn,-1.0_rk,1.0_rk,tol,ncm)
        enddo
        CSRN(nrn+1)=1.0_rk
    end subroutine


    !---------------------------------------------
    !subroutine getLineNumber(filename,np,additionalInfo)
    !
    !Get how many lines (np) are in the file "filename"
    !Also inform the called if the additional line is present (additionalInfo)
    !---------------------------------------------
    subroutine getLineNumber(filename,np,additionalInfo)
        integer, intent(out) :: np                !number of lines
        logical, intent(out) :: additionalInfo    !addiotional line was present
        character(len=*), intent(in) :: filename    !file name
        integer :: stat
        character :: ch
        character(len=512) :: A
        np=0
        additionalInfo=.false.
        open(unit=1,file=adjustl(trim(filename)))
        do while(.true.)
            read(1,*,iostat=stat)  A
            if(stat==0) then
                read(A,*) ch
                if(.not.ch.eq.'#') then
                    np=np+1
                else
                    additionalInfo=.true.
                endif
            else
                exit    
            endif
        enddo
        close(1)
    end subroutine

