#!/bin/bash -l
#SBATCH -J RT-CB_run
#SBATCH -t 00:30:00
#SBATCH --mem-per-cpu=200
#SBATCH -p parallel
#SBATCH -n 10
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=END

module purge
module load intel
module load intelmpi
cd /homeappl/home/<USER>/appl_taito/RTCB/
srun --mpi=pmi2 ./rtcbSphere_MPI 1 exampleRun4.in

