###############################################
#Makefile for RT-CB
#
#Copyright (C) 2016 Timo Väisänen and University of Helsinki
#All rights reserved.
#The new BSD License is applied to this software, see LICENSE.txt
#Versio 1.0###################################
#Created: 9th. March. 2016

###############################################
#FLAGS#########################################
#COMPILER!######################
#Select compiler/wrapper. Comment if intel is used
###############################################
#GCC specific. Comment if intel compiler is used
#FC 		    := mpifort
FC         := gfortran
FCFLAGS 	?= -O3 -funroll-loops -ffast-math -fopenmp -cpp -mtune=native
INCLUDES2	= $(INCLUDES) -I$(MODDIR)
MODDIR2     =-J$(MODDIR)

###############################################
#Intel compiler specific. Comment if gcc is used
#FC         := mpiifort
#FC         := ifort
#FCFLAGS    ?= -O3 -funroll-loops -openmp -cpp 
#INCLUDES2	= $(INCLUDES) -I$(MODDIR)
#MODDIR2     =-module $(MODDIR) -I$(MODDIR)

INCLUDES 	?= -I/usr/include
LIBS 		?= 
OBJDIR		?= obj
MODDIR		?= mod
SRCDIR		?= src
DEBUGGER	?= gdb
DEBUG		?=
ANALYSIS	?= analysis.txt
TMPFILE		?= gmon.out
RMADD		?= -rf
PROFILER	?= gprof
PROFILERADD ?= -pg
VERSION     ?= 1.0
SFMTPATH    ?= src/dsfmt
NAME		:= cbz
SPHDIR      =sphere
PLNDIR      =plane


#For portability
SHELL = /bin/sh

.SUFFIXES:
.SUFFIXES: .o .mod .f90 .f .f95 .f03 .f08

.SECONDEXPANSION:

ifdef DEBUG
FCFLAGS=$(DEBUG)
FCFLAGS+=-fcheck=bounds -ffpe-trap=invalid,underflow,zero -fbacktrace -fopenmp -cpp -std=f2008 -g -Wall -Wextra
endif

ifdef PROFILE
FCFLAGS=$(PROFILE)
FCFLAGS+=-pg -funroll-loops -ffast-math -fopenmp -cpp -std=f2008
endif


###############################################
#SOURCES#######################################

#Common source list1
SRCLISTC1 = definitions.f90 errorhandler.f90 hashtable.f90 mie.f90 inputparser.f90 mathroutines.f90 geoopt.f90 splinetools.f90 rayleighscatterer.f90  splinepresentation.f90 scatterermodule.f90  stokes.f90 efield.f90 scatterRay.f90 inputreader.f90 misc.f90

#Common source list2
SRCLISTC2 = preparedata.f90 main.f90

#Sphere geometry
SRCLISTSPH = collectdata_s.f90 cbscattering_s.f90 peeloff_s.f90 propagations_s.f90 algorithm_s.f90 output_s.f90 

#Plane/slab geometry
SRCLISTPLN = collectdata.f90 cbscattering.f90 peeloff.f90 propagations.f90 algorithm.f90 output.f90 

#Source file for Mersenne Twister
DSFMTLIST = dSFMT_interface.o dSFMT_utils.o dSFMT-jump.o dSFMT.o

#RNGs
STDRNG=stdrng.f90
SFMT=sfmtrng.f90

#MPI tools
WITHMPI=$(OBJDIR)/mpitools.o
WITHOUTMPI=$(OBJDIR)/mpitools2.o

SRCDIRSPH=$(SRCDIR)/$(SPHDIR)
SRCDIRPLN=$(SRCDIR)/$(PLNDIR)

COEFF=normcoeff.o
COEFFHM=normcoeffhm.o
COEFFS=normcoeff_s.o

#Normalization coefficients
NORMCOEFF		= $(OBJDIR)/$(COEFF)
NORMCOEFFHM		= $(OBJDIR)/$(COEFFHM)
NORMCOEFFS		= $(OBJDIR)/$(COEFFS)


FILE=buildInfo.txt
LASTBUILD=`cat $(FILE)`
AAA=$(shell cat buildInfo.txt)

###############################################
##OBJ FILES###################################

#Create list of objects
OBJLISTC1 	= $(SRCLISTC1:.f90=.o)
OBJLISTC2 	= $(SRCLISTC2:.f90=.o)
OBJLISTSPH 	= $(SRCLISTSPH:.f90=.o)
OBJLISTPLN 	= $(SRCLISTPLN:.f90=.o)

OBJLIST1_O 	= $(addprefix $(OBJDIR)/,$(OBJLISTC1))
OBJLIST2_O 	= $(addprefix $(OBJDIR)/,$(OBJLISTC2))
OBJLISTSFMT = $(addprefix $(OBJDIR)/,$(DSFMTLIST))
OBJLISTSPH_O = $(addprefix $(OBJDIR)/,$(OBJLISTSPH))
OBJLISTPLN_O = $(addprefix $(OBJDIR)/,$(OBJLISTPLN))


#Default PRNG is Mersenne Twister
RNG=$(OBJDIR)/sfmtrng.o
RNGADD:=$(OBJLISTSFMT)
#RNG=$(OBJDIR)/stdrng.o
#RNGADD=

###############################################
#RULES#########################################
.phony: all debug profile createDirs hm rnghelp clean

#Default RT-CB geometry will be sphere
all: createDirs
all: sphere

#create object and mod folders###############################
createDirs:
	@mkdir -p $(OBJDIR)
	@mkdir -p $(MODDIR)



#PLANE#########################################
rtcbPlane:  $(RNG) $(OBJLIST1_O) $(WITHOUTMPI) $(NORMCOEFF) $(OBJLISTPLN_O) $(OBJLIST2_O) 
	$(FC) $(FCFLAGS) $(MDEFS) -o $(NAME) $(INCLUDES2) $(RNGADD) $^ 
	@echo "Name" $(NAME) "plane geometry build" 

rtcbPlane_MPI: $(RNG) $(OBJLIST1_O) $(WITHMPI) $(NORMCOEFF) $(OBJLISTPLN_O) $(OBJLIST2_O)
	$(FC) $(FCFLAGS) $(MDEFS) -o $(NAME) $(INCLUDES2) $(RNGADD) $^ 
	@echo "Name" $(NAME) "plane geometry MPI build"

rtcbPlaneHM: $(RNG) $(OBJLIST1_O) $(WITHOUTMPI) $(NORMCOEFFHM) $(OBJLISTPLN_O) $(OBJLIST2_O)
	$(FC) $(FCFLAGS) $(MDEFS) -o $(NAME) $(INCLUDES2) $(RNGADD) $^ 
	@echo "Name" $(NAME) "plane geometry with host medium build"

rtcbPlaneHM_MPI: $(RNG) $(OBJLIST1_O) $(WITHMPI) $(NORMCOEFFHM) $(OBJLISTPLN_O) $(OBJLIST2_O)
	$(FC) $(FCFLAGS) $(MDEFS) -o $(NAME) $(INCLUDES2) $(RNGADD) $^ 
	@echo "Name" $(NAME) "plane geometry with host medium MPI build"
 
#SPHERE#########################################
rtcbSphere: $(RNG) $(OBJLIST1_O) $(WITHOUTMPI) $(NORMCOEFFS) $(OBJLISTSPH_O) $(OBJLIST2_O)
	$(FC) $(FCFLAGS) $(MDEFS) -o $(NAME) $(INCLUDES2) $(RNGADD) $^ 
	@echo "Name" $(NAME) "sphere geometry build"

rtcbSphere_MPI: $(RNG) $(OBJLIST1_O) $(WITHMPI) $(NORMCOEFFS) $(OBJLISTSPH_O) $(OBJLIST2_O)
	$(FC) $(FCFLAGS) $(MDEFS) -o $(NAME) $(INCLUDES2) $(RNGADD) $^ 
	@echo "Name" $(NAME) "sphere geometry MPI build"

#compile sources###############################
$(OBJLIST1_O): %.o : $$(join $(SRCDIR)/,$$(subst obj/,,%).f90)
	$(FC) $(MODDIR2) $(FCFLAGS) $(MDEFS) -o $@ $< -c 

$(OBJLIST2_O): %.o : $$(join $(SRCDIR)/,$$(subst obj/,,%).f90)
	$(FC) $(MODDIR2) $(FCFLAGS) $(MDEFS) -o $@ $< -c 

$(OBJLISTSPH_O): %.o : $$(join $(SRCDIRSPH)/,$$(subst obj/,,%).f90)
	$(FC) $(MODDIR2) $(FCFLAGS) $(MDEFS) -o $@ $< -c 

$(OBJLISTPLN_O): %.o : $$(join $(SRCDIRPLN)/,$$(subst obj/,,%).f90)
	$(FC) $(MODDIR2) $(FCFLAGS) $(MDEFS) -o $@ $< -c 


##MPI tools
$(OBJDIR)/mpitools2.o: $(SRCDIR)/mpitools2.f90
	$(FC) $(MODDIR2) $(FCFLAGS) $(MDEFS) -o $@ $< -c

$(OBJDIR)/mpitools.o: $(SRCDIR)/mpitools.f90
	$(FC) $(MODDIR2) $(FCFLAGS) $(MDEFS) -o $@ $< -c

##Some special rules###########################
#dSFMT ########################################
$(OBJDIR)/sfmtrng.o: $(SRCDIR)/sfmtrng.f90
	make -C $(SFMTPATH) FC=$(FC)
	$(FC) $(MODDIR2) $(FCFLAGS) $(MDEFS) -o $@ $< -c	

$(OBJDIR)/stdrng.o: $(SRCDIR)/stdrng.f90
	$(FC) $(MODDIR2) $(FCFLAGS) $(MDEFS) -o $@ $< -c	

$(OBJDIR)/$(COEFF): $(SRCDIRPLN)/normcoeff.f90
	$(FC) $(MODDIR2) $(FCFLAGS) $(MDEFS) -o $@ $< -c

$(OBJDIR)/$(COEFFHM): $(SRCDIRPLN)/normcoeffhm.f90
	$(FC) $(MODDIR2) $(FCFLAGS) $(MDEFS) -o $@ $< -c

$(OBJDIR)/$(COEFFS): $(SRCDIRSPH)/normcoeff_s.f90
	$(FC) $(MODDIR2) $(FCFLAGS) $(MDEFS) -o $@ $< -c



#############################################################
##Plane geometry#############################################
plane:  NAME := rtcbPlane
plane: check
plane: FCFLAGS          +=
plane:  rtcbPlane


planeMPI: NAME          := rtcbPlane_MPI
planeMPI: check
planeMPI: FCFLAGS       += -DMPIVERSION 
planeMPI: rtcbPlane_MPI


##Plane geometry with host media#############################
planeHM: NAME           := rtcbPlaneHM
planeHM: check
planeHM: FCFLAGS        += -DHM
planeHM: NAME           := rtcbPlaneHM
planeHM: rtcbPlaneHM


planeHMMPI: NAME        := rtcbPlaneHM_MPI
planeHMMPI: check
planeHMMPI: FCFLAGS     += -DHM -DMPIVERSION
planeHMMPI: rtcbPlaneHM_MPI


#############################################################
##Sphere geometry#############################################
sphere: NAME            := rtcbSphere
sphere: check
sphere: FCFLAGS         +=
sphere: rtcbSphere
	
sphereMPI: NAME         := rtcbSphere_MPI
sphereMPI: check
sphereMPI: FCFLAGS      +=  -DMPIVERSION
sphereMPI: rtcbSphere_MPI


#############################################################
info:
	@echo "RT-CB version" $(VERSION)
	@echo "make clean:             remove all .o and .mod files"
	@echo "make plane:             the non-MPI plane version"
	@echo "make planeMPI:          the MPI plane version"
	@echo "make planeHM:           the non-MPI plane version with a host medium"
	@echo "make planeHMMPI:        the MPI plane version with a host medium"
	@echo "make sphere:            the non-MPI sphere version"
	@echo "make sphereMPI:         the MPI sphere version"

check:
	@if [ "$(NAME)" != "$(LASTBUILD)" ]; then make clean; echo $(NAME) > buildInfo.txt; fi

#############################################################
#delete all objects and mod files################################
clean:
	@$(RM) $(RMADD) $(OBJDIR)/*.o $(MODDIR)/*.mod
	@make clean -C $(SFMTPATH)


