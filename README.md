The public version of the RT-CB (Radiative Transfer Coherent Backscattering) and the first-order incoherent input generator. All host medium codes are removed (check private repository)


## How to compile ##

Compiling RT-CB with the gfortran

```
make
make sphere
make sphereMPI
make plane
make planeMPI
```

Compiling FOICGEN with the gfortran. (Compile where the foicgen source files are located (foicgen))

```
make
```


## What has to be done ##

* Update and clean documentation.


## Referencing the RT-CB ##
* Muinonen K, 2004. Coherent backscattering of light by complex random media of spherical scatterers: Numerical solution. Waves in Random Media 14(3), 365-388.
* V�is�nen T, et al, 2016. Validation of radiative transfer and coherent backscattering for discrete random media, Abstract for 2016 URSI International Symposium on
## Referencing the FOICGEN ##
* Muinonen K, Markkanen J, V�is�nen T, Peltoniemi J I, Penttil� A, 2017. Multiple Scattering in Discrete Random Media Using First-Order Incoherent Interactions. Radio Science 52(11), 1419-1431. DOI: 10.1002/2017RS006419
