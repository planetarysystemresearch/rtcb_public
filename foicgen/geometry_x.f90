
!---------------------------------------------------------
! Generates volume elements made of spheres specified 
! by the RadiusGenerator module. 
! SHORT DESCRIPTION:
! Pack a large container periodically with scatterers and
! extract spherical volume element from it. The packing is accelerated
! by dividing the container into cells which are used to check
! whether a new scatterer will overlap with existing
! scatterers.
!
! LONG DESCRIPTION
! #1 Find the maximum amount of scatterers in a single cell
! #2 Generate a grid of cells. 
!       (1st column (pos,radius), 2nd colum (nth sphere), 3-5th colums (pos in grid))
! #3 Pack periodically the grid with scatterers until the
!       given density of the container is the same as the given density
! #4 Extract a volume element from a container
! 
! NOTICE:
! The packed container can be used again by adjusting 
! N_GEN_NEW0, which tells how many times the same container 
! can be used to extract volume elements until a new container
! will be generated. (Default: N_GEN_NEW0=1, always generate a new)
! The average number of scatterers inside the container: 
! (Default: N_AVG = 10**5, the smaller, the faster)
! The algorithm adjusts the maximum number of scatterers
! by trying how many fits inside a cell. This trial is not
! enough for each cell and that is why the number of scatterers
! must be multiplied (Default: NSPH_MULTIPLY=4).
!
! MAX_FAIL_COUNT (10000), number of tries before the packer
! starts to signal that the container is quite crowded. Won't
! kill the program. 
!
! Copyright (C) 2018 Timo Väisänen, Johannes Markkanen, University of Helsinki
! All rights reserved.
! The new BSD License is applied to this software, see LICENSE.txt
!
! List of public functions:
! subroutine pack_spheres(volrad, dens, eps, N, coords, radiuses, params)
!
! Modifications:
! 23.3.2018 TV: Maintenance, cleaning, commenting
!
! TODO:
! *The algorithm kills the whole program if the 
! number of spheres inside the cell is exceeded.
! A better scheme would be to restart the program.
! Easily done.
! *More explaining
! *Make it possible to force the whole scatterer
! stay inside the volume element
!-----------------------------
module Geometry_x
    use Rng
    use Constants
    use RadiusGenerator
    implicit none

    real(kind=dp), parameter :: NSPH_MULTIPLY = 3
    real(kind=dp), parameter :: p_fix = 4*epsilon(p_fix)
    integer, parameter :: N_AVG = 10**6
    integer, parameter :: N_GEN_NEW = 1
    integer, parameter :: MAX_FAIL_COUNT = 10000
    !$omp threadprivate(grid,extract_counter,initialized,n_max,n_edge,l_box,l_edge)
    real(kind=dp), allocatable :: grid(:,:,:,:,:)
    real(kind=dp) :: l_edge
    real(kind=dp) :: l_box,dens
    integer :: n_edge
    integer :: extract_counter
    integer :: n_max 
    logical :: initialized = .false.   

    private 
    public :: pack_spheres

contains


!!This is used to initialize all the parameters of the module
subroutine init_vngeneraattori(vol_rad,dens0)
    real(kind=dp), intent(in)   :: vol_rad
    !!radius of the volume element
    real(kind=dp), intent(in)   :: dens0
    !!density of the container

    integer :: n_z_grid
    real(kind=dp) :: max_rad
    
    call estimate_size(dens0,n_avg,max_rad,l_box)
    l_edge = vol_rad*2.1_dp
    n_z_grid = ceiling(l_box/l_edge)
    l_box = l_edge*n_z_grid 
    if(n_z_grid<4) then
        write(6,*) "Packing algorithm error: grid can't be initialized"
        stop
    endif 
    n_edge = n_z_grid
    n_max = int(estimateN(l_edge,dens0)*NSPH_MULTIPLY)
    extract_counter = N_GEN_NEW+1
    dens = dens0
    initialized = .true.
    allocate(grid(4,n_max,n_z_grid,n_z_grid,n_z_grid))
    write(6,*) "packing generator initialized"
    write(6,*) "Container width/length/depth: ",l_box,"Number of cells:",n_edge**3
    write(6,*) l_box,max_rad,l_edge,n_edge
end subroutine

!!Generates a volume element centered to 0. 
subroutine pack_spheres(volrad, dens, eps, N, coords, radiuses, params)
    real(kind=dp), intent(in)       :: volrad       !!Volume element (VE) radius
    real(kind=dp), intent(in)       :: dens         !!density
    complex(kind=dp), intent(in)    :: eps          !!permittivity of the scatterers
    
    integer, intent(out)                    :: N            !!Number of particles inside the VE
    real(kind=dp), pointer, intent(out)     :: coords(:,:)  !!Coordinates of the scatterer
    real(kind=dp), pointer, intent(out)     :: radiuses(:)  !!Radiuses of the scatterers
    complex(kind=dp), pointer, intent(out)  :: params(:)    !!permittivies of the scatterers

    if(.not. initialized) call init_vngeneraattori(volrad,dens)
    if(extract_counter>=n_gen_new) then
        call generate_medium()
        extract_counter=0
    endif
    call extract_volume_element(volrad,coords,radiuses,params)
    params(:) = eps
    N=size(params)
end subroutine

!!Estimate the size of the container
subroutine estimate_size(dens,n_avg,max_rad,l_edge)
    real(kind=dp), intent(in)   :: dens
    integer, intent(in)         :: n_avg
    real(kind=dp), intent(out)  :: max_rad,l_edge

    real(kind=dp) :: cumulative,rad
    integer :: j1
    
    cumulative  =   0.0_dp
    max_rad     =   0.0_dp
    do j1=1,n_avg
        rad = rad_gen_get_radius()
        cumulative = cumulative + rad**3
        if(max_rad<rad) then
            max_rad = rad
        endif
    enddo
    l_edge = (4*pi/3.0_dp*cumulative/dens)**(1.0_dp/3.0_dp)
end subroutine

!!Estimate the number of particles inside the cell
function estimateN(l_edge,dens) result(retVal)
    real(kind=dp), intent(in) :: l_edge,dens
    integer :: retVal

    real(kind=dp) :: cumulative
    integer :: j1,counter
    retVal=0
    do j1=1,1000
        cumulative=0.0_dp
        counter=0
        do while(cumulative<dens*l_edge**3/(4.0_dp*pi)*3.0_dp)
            counter=counter+1
            cumulative=cumulative+rad_gen_get_radius()**3

        enddo
         
        if(counter>retVal) retVal=counter
    enddo
end function

!!This function generates the container
subroutine generate_medium()
    real(kind=dp) :: radius,new_loc(3)
    real(kind=dp) :: acc_V
    integer :: grid_point(3),fail_counter
    integer :: j1,j2
    logical :: fit
    grid(:,:,:,:,:) = 0.0_dp
    fail_counter = 0
    acc_V=0.0_dp
    j2=1
    !!Do until the container is filled with scatterers
    do while(acc_V<dens*(l_box**3))   
        !!Generate location of the particle
        new_loc(:) = generate_location()
        !!check in which cell
        grid_point(:) = find_grid_point(new_loc)
        !!generate radius
        radius = rad_gen_get_radius()
        !!check that the generator does not produce any illegal radiuses
        if(radius>l_edge*2) then
            write(6,*) "ERROR! vngenerator: radius must be smaller than l_edge*2",radius,l_edge
            stop 
        endif
        !!check overlap
        if(.not. overlap(grid_point,radius,new_loc)) then
            fit = .false.
            !!save the scatterer to the container
            do j1=1,n_max
                !!if the 4th element is 0, there is an empty space
                if(grid(4,j1,grid_point(1),grid_point(2),grid_point(3))==0.0_dp) then
                    grid(4,j1,grid_point(1),grid_point(2),grid_point(3))=radius
                    grid(1:3,j1,grid_point(1),grid_point(2),grid_point(3))=new_loc(:)
                    acc_V=acc_V+4.0_dp/3.0_dp*pi*radius**3
                    fit = .true.
                    exit
                endif
            enddo
            !!check that the cell was not full
            if(.not.fit) then
                write(6,*) "ERROR! Not enough space for a new object"
                stop
            endif
            fail_counter=0
            j2=j2+1
        else
            !!fail counter is used to signal whether we failed to pack a ascatterer
            fail_counter=fail_counter+1
            if(fail_counter>=MAX_FAIL_COUNT) then
                write(6,*) "WARNING! Failed to insert new particle time, trying again",new_loc
                fail_counter = 0
            endif
        endif 
    enddo
end subroutine

!!This function is used to extract a volume element from the container
subroutine extract_volume_element(vol_rad,coord_data,radius_data,param_data)

    real(kind=dp), intent(in)               :: vol_rad
    real(kind=dp), pointer,intent(out)      :: coord_data(:,:)
    real(kind=dp), pointer,intent(out)      :: radius_data(:)
    complex(kind=dp), pointer,intent(out)   :: param_data(:)

    integer :: grid_point(3)
    integer :: inds(3,27),ind,j1,j2,j3
    real(kind=dp) :: new_loc(3)
    real(kind=dp) :: mod_pos(3),values(4),dist(3)
    real(kind=dp), allocatable :: tmp(:,:)

    extract_counter=extract_counter+1
    allocate(tmp(4,n_max))
    new_loc(:) = generate_location()
    grid_point(:) = find_grid_point(new_loc)
    inds(:,:) = get_neighbours(grid_point)
    ind=0
    do j1=1,27
        do j2=1,size(grid,dim=2)
            values(:) = grid(:,j2,inds(1,j1),inds(2,j1),inds(3,j1))
            mod_pos(:) = values(1:3)
            if(values(4)==0.0_dp) exit            
            if(j1/=1) then
                do j3=1,3
                    if(abs(inds(j3,j1)-grid_point(j3))>1) then
                        if(grid_point(j3)==n_edge) then
                            mod_pos(j3) = l_box+mod_pos(j3)
                        elseif(grid_point(j3)==1) then
                            mod_pos(j3) = mod_pos(j3)-l_box
                        endif
                    endif
                enddo
            endif
            dist = abs(new_loc(:)-mod_pos(:))
            if(vol_rad**2>=dot_product(dist,dist)) then
                ind=ind+1
                tmp(1:3,ind) = mod_pos(:)-new_loc(:)
                tmp(4,ind) = grid(4,j2,inds(1,j1),inds(2,j1),inds(3,j1))
            endif
        enddo
    enddo
    ALLOCATE(coord_data(3,ind),radius_data(ind),param_data(ind))
    coord_data(:,:) = tmp(1:3,1:ind)
    radius_data(:) = tmp(4,1:ind)
    deallocate(tmp)
end subroutine
    
!!Generate a random position in the container
function generate_location() result(new_location)
    real(kind=dp) :: new_location(3)
    new_location(1)=l_box*randNum()
    new_location(2)=l_box*randNum()
    new_location(3)=l_box*randNum()
end function

!!Find the index of the current cell
function find_grid_point(location) result(grid_point)
    real(kind=dp), intent(in) :: location(3)  
    integer :: grid_point(3)
    grid_point(:) = ceiling(location(:)/l_edge)
end function


!!Check overlap
function overlap(grid_point,radius,location) result(retVal)
    logical :: retVal
    real(kind=dp), intent(in) :: radius,location(3)
    real(kind=dp) :: values(4),dist(3)
    integer, intent(in) :: grid_point(:)
    integer :: inds(3,27)
    integer :: j1,j2    
    inds(:,:) = get_neighbours(grid_point)
    retVal = .false.
    do j1=1,27
        do j2=1,size(grid,dim=2)
            values(:) = grid(:,j2,inds(1,j1),inds(2,j1),inds(3,j1))
            if(values(4)==0.0_dp) exit
            dist = abs(location(:)-values(1:3))
            if(j1/=1) then
                if(abs(inds(1,j1)-grid_point(1))>1) dist(1) = dist(1)-l_box
                if(abs(inds(2,j1)-grid_point(2))>1) dist(2) = dist(2)-l_box
                if(abs(inds(3,j1)-grid_point(3))>1) dist(3) = dist(3)-l_box
            endif
            if((radius+values(4))**2+p_fix>=dot_product(dist,dist)) then
                retVal = .true.
                exit
            endif
        enddo
        if(retVal) exit 
    enddo
end function


!!Get the indices of the neighbouring cells
function get_neighbours(grid_point) result(inds)
    integer, intent(in) :: grid_point(3)
    integer :: inds(3,27)
    
    integer :: j1,j2,j3,j4    
    integer :: ind
    ind = 1
    inds(1,ind) = grid_point(1)
    inds(2,ind) = grid_point(2)
    inds(3,ind) = grid_point(3)
    do j1=-1,1
        do j2=-1,1
            do j3=-1,1
                if(j1==0 .and. j2==0 .and. j3==0) cycle
                ind=ind+1
                inds(1,ind) = grid_point(1)+j1
                inds(2,ind) = grid_point(2)+j2
                inds(3,ind) = grid_point(3)+j3
                do j4=1,3
                    if(inds(j4,ind)==0) then
                        inds(j4,ind) = n_edge
                    else if(inds(j4,ind)==(n_edge+1)) then
                        inds(j4,ind) = 1
                    endif
                enddo
            enddo
        enddo
    enddo
end function

!!!wow such nested loops. This beauty is used to 
!!is used to check, whether the container has overlapping particles.
function test_overlap() result(retVal)
    logical :: retVal
    real(kind=dp) :: dist(3)
    real(kind=dp) :: values1(4),values2(4)
    integer :: j1,j2,j3,i1,i2,i3,k1,k2,l1
    do j1=1,n_edge
        do j2=1,n_edge
            do j3=1,n_edge
                write(6,*) "testing grid cell",j1,j2,j3
                do i1=1,n_edge
                    do i2=1,n_edge
                        do i3=1,n_edge
                            do k1=1,n_max
                                values1(:) = grid(:,k1,j1,j2,j3)
                                if(values1(4)==0.0_dp) exit
                                do k2=1,n_max 
                                    values2(:) = grid(:,k2,i1,i2,i3)
                                    if(values2(4)==0.0_dp) exit
                                    if(k1==k2 .and. j1==i1 .and. j2==i2 .and. j3==i3) cycle
                                    dist = abs(values1(1:3)-values2(1:3))
                                    if((values1(4)+values2(4))**2>=dot_product(dist,dist)) then
                                        write(6,*) "overlap",j1,j2,j3,i1,i2,i3,k1,k2,values1(:), &
                                                        &   values2(:),values1(4)+values2(4),sqrt(dot_product(dist,dist))
                                        stop
                                    endif

                                    if(values2(1)>=(l_box-l_edge) .or. values2(2)>=(l_box-l_edge) &
                                        &   .or. values2(3)>=(l_box-l_edge)) then
                                        do l1=1,3
                                            if(values1(l1)<l_edge) values1(l1)=l_box+values1(l1)
                                        enddo
                                        dist = abs(values1(1:3)-values2(1:3))
                                        if((values1(4)+values2(4))**2>=dot_product(dist,dist)) then
                                            write(6,*) "overlap 2",j1,j2,j3,i1,i2,i3,k1,k2,values1(:), &
                                                        &   values2(:),values1(4)+values2(4),sqrt(dist)
                                            stop
                                        endif
                                    endif
                                enddo
                            enddo
                        enddo
                    enddo
                enddo
            enddo
        enddo
    enddo
    retVal = .true.
end function




end module
