module output
!!RT-Engine
!!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!!All rights reserved.
!!The new BSD License is applied to this software, see LICENSE.txt
    use constants
    use typedefinitions
    implicit none

    character(len=*), parameter  :: fmt1 = "(A20,3X,I2,'=',E12.5)"

    private fmt1
contains

    subroutine print_arr_with_message(arr,r_arr,ntot,stream,msg,fmt0)
        integer, intent(in) :: stream
        integer, intent(in) :: ntot
        character(*), intent(in) :: msg
        character(*), intent(in) :: fmt0
        real(kind=rk), intent(in) :: arr(:)
        integer, intent(in) :: r_arr(:)
        integer :: j1
        do j1=1,size(arr)
            write(stream,fmt0) msg,r_arr(j1),arr(j1)/ntot
        enddo
    end subroutine

    subroutine printOutput(dB,KIC,PIC0,SIC0,x_vol)
        type(dataBlock),intent(in) :: dB
        real(kind=rk), intent(in) :: x_vol
        real(kind=rk), intent(in) :: PIC0(:,:),KIC(3)
        complex(kind=rk), intent(in) :: SIC0(:,:)
        character(len=120) :: addString
        character(8)  :: date
        character(10) :: time
        character(5)  :: zone
        real(kind=rk) :: renorm,Atmp,Atra,Adt
        real(kind=rk) :: norm,rd,phiout,M(4,4),the
        integer :: j0,j1,j2,j3,j4
        real(kind=rk), allocatable :: MBS(:,:,:,:,:,:),MRT(:,:,:,:,:)
        integer :: ps_count,stream,k2   
        !Atot=sum(Aref)+sum(Asca)+sum(Aspec)+sum(Aabs)+sum(Astop)
        call date_and_time(DATE=date,ZONE=zone)
        call date_and_time(TIME=time)
        write(addString,*) "#FOICGEN: ", " ,date, ",trim(adjustl(date))," ,time, ",trim(adjustl(time)) 
        rd=pi/180.0_rk
        !parameter output
        stream=1
        open(unit=stream,file=trim(adjustl(dB%output_details)))
        write(stream,*) trim(adjustl(addString))        
        call writeInfoScreen(dB,stream,.true.)
        write(stream,*) "////////////////////////////"
        write(stream,'(A30,E12.5)') "volume_element_size",x_vol
        write(stream,'(A30,E12.5)') "kappa extinction",KIC(1)
        write(stream,'(A30,E12.5)') "kappa scattering",KIC(2)
        write(stream,'(A30,E12.5)') "kappa absorption",KIC(3)
        write(stream,'(A30,E12.5)') "albedo",KIC(2)/KIC(1)
        write(stream,'(A30,E12.5)') "mean_free_path",1/(KIC(1))

        write(stream,*) "=============================="
        write(6,'(A30,E12.5)') "volume_element_x",x_vol
        write(6,'(A30,E12.5)') "kappa extinction",KIC(1)
        write(6,'(A30,E12.5)') "kappa scattering",KIC(2)
        write(6,'(A30,E12.5)') "kappa absorption",KIC(3)
        write(6,'(A30,E12.5)') "albedo",KIC(2)/KIC(1)
        write(6,'(A30,E12.5)') "mean_free_path",1/(KIC(1))


        close(unit=stream)

        open(unit=stream,file=trim(adjustl(dB%saveScatterer)))
            write(stream,*)     0.0_rk,PIC0(1,1),0.0d0,1.0d0,0.0d0, &
                            &   real(SIC0(1,1)),aimag(SIC0(1,1)),  &
                            &   real(SIC0(1,2)),aimag(SIC0(1,2))
        do j1=2,size(PIC0,dim=2)
            write(stream,*)     180.0_rk/(size(PIC0,dim=2)-1)*(j1-1),PIC0(1,j1),-PIC0(2,j1)/PIC0(1,j1),    &
                            &   PIC0(3,j1)/PIC0(1,j1),                          &
                            &   PIC0(4,j1)/PIC0(1,j1),                          &
                            &   real(SIC0(j1,1)),aimag(SIC0(j1,1)),                 &
                            &   real(SIC0(j1,2)),aimag(SIC0(j1,2))
        enddo
        close(stream)

    end subroutine

    !For test purposes
    subroutine writeInfoScreen(dB,stream,print_header)
        integer, intent(in) :: stream

        logical, intent(in) :: print_header
        type(dataBlock), intent(in) :: dB
        write(stream,*) "////////////////////////////"
        
        write(stream,*) "FOICGEN (The First Order IC Input GENerator)"
        write(stream,*) "////////////////////////////"
        
!#OUTPUT_GENERATOR_SCRIPT1
        write(stream,'(A30,E12.5)') "wavelength", dB%wavel
        write(stream,'(A30,E12.5)') "single_scattering_albedo", dB%ssalbedo
        write(stream,'(A30,E12.5)') "mean_free_path",dB%ell1
        write(stream,'(A30,E12.5)') "medium_thickness",dB%hr
        write(stream,'(A30,E12.5)') "min_relative_flux",dB%Fstop
        write(stream,'(A30,E12.5)') "threshold_for_optical_depth",dB%tau_c
        write(stream,'(A30,I12)') "number_of_theta_angles",dB%nthe
        write(stream,'(A30,I12)') "number_of_phi_angles",dB%nphi
        write(stream,'(A30,I12)') "max_number_of_scattering",dB%nsca
        write(stream,'(A30,I12)') "seed",dB%seed
        write(stream,'(A30,I12)') "number_of_rays",dB%ntot
        write(stream,*)

        write(stream,'(A30,E12.5)') "host_med_refractive_index_real", dB%mre
        write(stream,'(A30,E12.5)') "host_med_refractive_index_imag", dB%mim  
        write(stream,*)

        write(stream,'(A30,1X,A30)') "output_details",dB%output_details
        write(stream,'(A30,1X,A30)') "rt_solution",dB%output_rt
        write(stream,'(A30,1X,A30)') "cb_solution",dB%output_cb
        write(stream,'(A30,2X,L1)') "output_add_additional_info",dB%addInfo
        write(stream,'(A30,2X,l1)') "estimate_time",dB%estimateTime
        write(stream,'(A30,2X,A30)') "output_extra_info",dB%outputExt


        write(stream,*) "///////////////////////////////////////"
        write(stream,'(A30,A12)') "scatterer_type ",adjustl(trim(dB%scattererType))
        if(trim(adjustl(dB%scattererType))=="spline") then         
            !Scatterer type        
            if(.not. dB%generateMieScatterer) then
                write(stream,"(A30)") "mean_free_path is user-defined"
                write(stream,"(A30)") "single_scattering_albedo is user-defined" 
                write(stream,'(A30,A12)') "load_scatterer_data_from",adjustl(trim(dB%scattererData))
            else
                write(stream,"(A30)") "mie scatterer was generated"
                write(stream,"(A30)") "mean_free_path is generated by the program" 
                write(stream,"(A30)") "single_scattering_albedo is generated by the program" 
                write(stream,'(A30,I12)') "points_in_spline_presentation",dB%np
                write(stream,'(A30,E12.5)') "scatterer_radius",dB%scRadius
                write(stream,'(A30,E12.5)') "scatterer_real_ref",dB%scRealRef
                write(stream,'(A30,E12.5)') "scatterer_imag_ref",dB%scImagRef
                write(stream,'(A30,E12.5)') "volume_fraction",dB%vf
                write(stream,'(A30,A30)') "save_scatterer_data_to",adjustl(trim(dB%saveScatterer))
                write(stream,'(A30,I12)') "nrn",dB%nrn
                write(stream,'(A30,I12)') "ncm",dB%ncm

            endif
        else
            write(stream,"(A40)") "mean_free_path is user-defined"
            write(stream,"(A40)") "single_scattering_albedo is user-defined"
        endif
!#OUTPUT_GENERATOR_SCRIPT2
        if(dB%generateMieScatterer) then
            write(stream,"(A50)") "Mie Scatterer will be generated"
        else
            write(stream,"(A50)") "Scatterer will be user defined"
        endif
        write(stream,*) str_line
    end subroutine

end module


