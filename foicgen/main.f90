!Copyright (C) 2017 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt

program FirstOrder
!Computes the first order of incoherent scattering
use constants
use rng
use mie
use splinepresentation
use typedefinitions
use incoherent
use errorhandler
use input_reader
use output
use omp_lib
use RadiusGenerator

implicit none


real(kind=rk) :: r_sca,r_vol,vf,wavel
real(kind=rk) :: waven,x_sca,x_vol,dthe
integer :: np,npz,nzm,nsm,nor,nang,size0
real(kind=rk), allocatable :: theta(:)
real(kind=rk), allocatable :: P,S0
real(kind=rk) :: varpi, ell,arg
real(kind=rk) :: ssca,sext,sabs,KIC(3)
real(kind=rk), allocatable :: PIC0(:,:)
complex(kind=rk), allocatable :: SIC0(:,:)
integer, allocatable :: seed(:)
type(dataBlock) :: dB
integer :: tid,clock,id,j1


call readInput(dB)


if(db%seed==0) then
    call random_seed(size=size0)
    allocate(seed(size0))
    call system_clock(count=clock)
    id = getpid() !Intel might need IFPORT, fix it later
    seed = clock+id * (/(j1-1,j1=1,size0)/)
    call random_seed(put=seed)
    call random_number(arg)
    dB%seed=int(arg*100000)
endif

!Initialize random number generators
!$omp parallel private(tid) shared(dB)
tid = omp_get_thread_num()
!tid = 0
call init_rng(dB%seed,tid)
!call initRng(2)
!write(6,*) "mpi_thread, openmp_thread, seed:", id,tid,dB%seed+id*mtid+tid
!$omp end parallel

waven = 2*pi/dB%wavel
x_sca = waven*dB%scRadius
call pass_arguments(x_sca,0.0_rk)


db%dthe = pi/dB%np
dB%ns = dB%np
nang = dB%np

if(dB%Nspheres>0) then
    write(6,*) "Radius of the volume element defined from a number of spheres"
    x_vol = waven*(dB%scRadius**3*dB%Nspheres/dB%vf)**(1/3)
    dB%r_vol = x_vol/waven
else
    write(6,*) "Number of scatterers in the volume element defined from a radius of the volume element"
    x_vol = waven*dB%r_vol
    dB%Nspheres = dB%vf*dB%r_vol**3/dB%scRadius**3
endif

call writeInfoScreen(dB,6,.false.)

if(.not. dB%generateMieScatterer) then
    call assert(dB%ssca>=0.0_rk, "s_scattering_cross_sec must be defined if mie scatterer is not generated")
    call assert(dB%sabs>=0.0_rk, "s_abs_cross_sec must be defined if mie scatterer is not generated")
    call assert(dB%sext>=0.0_rk, "s_extinction_cross_sec must be defined if mie scatterer is not generated")
endif

write(6,*) "Volume element size will be ", x_vol


call prepareSpline(dB)
write(6,*) "Spline ready"
call compute_fo_incoherent(KIC,PIC0,SIC0,x_vol,x_sca,dB%vf,dB%ssca/dB%sext,waven,dB%npz,dB%nzm,dB%nsm,dB%nor,nang)
!call compute_fo_incoherent(KIC,PIC0,SIC0,x_vol,x_sca,dB%vf,dB%ssca/dB%sext,waven,2,2,2,2,nang)
write(6,*) "Output"
call printOutput(dB,KIC,PIC0,SIC0,x_vol)
end program
