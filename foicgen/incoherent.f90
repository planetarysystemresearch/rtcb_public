!Copyright (C) 2017 Karri Muinonen, Timo Väisänen and University of Helsinki
!All rights reserved.
!The new BSD License is applied to this software, see LICENSE.txt

module incoherent
use constants
use typedefinitions
use splinePresentation
use rng
use geometry_x

implicit none

contains

subroutine compute_fo_incoherent(KIC,PIC,SIC,xr0,xa,vf,omgt,waven,npz,nzm,nsm,nor,nang)
    complex(kind=rk),allocatable :: EF(:,:),FEF(:),ECF(:,:),FCF(:),F_(:),ECF_(:,:),FCF_(:)
    real(kind=rk), allocatable :: FIC(:),F(:),THE(:)
    real(kind=rk), intent(out) :: KIC(3)
    real(kind=rk),allocatable, intent(out) :: PIC(:,:)
    complex(kind=rk),allocatable, intent(out) :: SIC(:,:)
    real(kind=rk), intent(in) :: xr0,xa,vf,omgt,waven
    integer, intent(in) :: npz,nsm,nor,nang,nzm
    real(kind=rk) :: K(3),X(3),pnorm
    real(kind=rk), allocatable :: XC1(:,:),XC2(:,:)
    integer :: j0,j1,j2,j3,j4,j5,npar,nps
    complex(kind=rk) :: cf
    real(kind=rk) :: L0(3),sextic,sscaic,sabsic,xk,img
    real(kind=rk), allocatable :: XC0(:,:),A0(:),XA0(:)
    real(kind=rk) :: a
    integer :: dthe,ncm,nrn
    character(len=32) :: filename
    integer :: N_ave,N
    real(kind=rk) :: stdev
    real(kind=rk), pointer :: radius(:),coord(:,:)
    complex(kind=rk), pointer :: param(:)
    complex(kind=rk) :: eps
    logical :: obsolete
    allocate(ECF(nang+1,2),FCF(nang+1),FIC(nang+1),F(nang+1),PIC(4,nang+1),SIC(nang+1,2),THE(nang+1))
    allocate(XC0(npz,3),XC1(npz,3),XC2(npz,3),XA0(npz))
    allocate(EF(nang+1,2),FEF(nang+1))
    EF(:,:)=cmplx(0.0_rk,0.0_rk,kind=rk)
    FEF(:)=cmplx(0.0_rk,0.0_rk,kind=rk)
    ECF(:,:)=cmplx(0.0_rk,0.0_rk,kind=rk)
    FCF(:)=cmplx(0.0_rk,0.0_rk,kind=rk)
    F(:)=0.0_rk
    FIC(:)=0.0_rk
    eps=cmplx(0,0,kind=rk)
    stdev=0.0_rk
    do j1=1,nang+1
        THE(j1)=(j1-1)*pi/nang
    enddo

    !$omp parallel do default(private) shared(stdev, N_ave, eps, nzm,xa,vf,npz,nsm,xr0,nor,nang,THE,coeffs2) &
    !$omp reduction(+:ECF) reduction(+:FCF) reduction(+:F)
    do j1=1,nzm
        write(6,*) j1,nzm

        call pack_spheres(xr0, vf, eps, N, coord, radius, param)    
        
        write(6,*) N, 2, vf, xa, stdev
        write(6,*) "ready"
        !zero EF and FEF
        EF(:,:)=cmplx(0.0_rk,0.0_rk,kind=rk)
        FEF(:)=cmplx(0.0_rk,0.0_rk,kind=rk)
        do j4=1,N
            do j5=1,nang+1
                K(1)=sin(THE(j5))
                K(2)=0.0_rk
                K(3)=cos(THE(j5))
                X(:)=coord(:,j4)
                xk=K(1)*X(1)+K(2)*X(2)+K(3)*X(3)
                cf=exp(cmplx(0.0_rk,X(3)-xk,kind=rk))
                !write(6,*) EF(j5,1),cmplx(coeffs2(4,1,j5),coeffs2(4,2,j5),kind=rk)
                !write(6,*) EF(j5,2),cmplx(coeffs2(4,3,j5),coeffs2(4,4,j5),kind=rk)
                EF(j5,1)=EF(j5,1)+cf*cmplx(coeffs2(4,1,j5),coeffs2(4,2,j5),kind=rk)
                EF(j5,2)=EF(j5,2)+cf*cmplx(coeffs2(4,3,j5),coeffs2(4,4,j5),kind=rk)
                FEF(j5)=FEF(j5)+cf
            enddo
        enddo   



        ECF(:,:)=ECF(:,:)+EF(:,:)
        FCF(:)=FCF(:)+FEF(:)
        F(:)=F(:)+abs(FEF(:))**2
        deallocate(coord, radius, param)
    enddo
    !$omp end parallel do





    do j1=1,nang+1
        ECF(j1,:)=ECF(j1,:)/(nzm)
        FCF(j1)=FCF(j1)/(nzm)
        F(j1)=F(j1)/(nzm)
        FIC(j1)=F(j1)-abs(FCF(j1))**2
    enddo

    PIC(:,:)=0.0_rk

    do j1=1,nang+1
        SIC(j1,1)=cmplx(coeffs2(4,1,j1),coeffs2(4,2,j1),kind=rk)*sqrt(FIC(j1))
        SIC(j1,2)=cmplx(coeffs2(4,3,j1),coeffs2(4,4,j1),kind=rk)*sqrt(FIC(j1))
        PIC(1,j1)= 0.5_rk*(abs(SIC(j1,1))**2+abs(SIC(j1,2))**2)
        PIC(2,j1)=-0.5_rk*(abs(SIC(j1,1))**2-abs(SIC(j1,2))**2)
        PIC(3,j1)=real(SIC(j1,2)*conjg(SIC(j1,1)))
        PIC(4,j1)=aimag(SIC(j1,2)*conjg(SIC(j1,1)))
    enddo


    call PSPLII(PIC,pnorm)

    dthe=pi/nang
    sscaic=0.5_rk*4.0_rk*pi*(pnorm/waven**2)
    sabsic=(1.0_rk-omgt)*sscaic/omgt
    sextic=sscaic+sabsic
    pnorm=4.0_rk*pi/(waven**2*sscaic)
    PIC(:,:)=pnorm*PIC(:,:)

    pnorm=1.0_rk/(4.0_rk*pi*(xr0/waven)**3/3.0_rk)
    KIC(1)=pnorm*sextic
    KIC(2)=pnorm*sscaic
    KIC(3)=pnorm*sabsic
end subroutine




!Vector rotation using Euler angles. 
!
!Author: Karri Muinonen
!Version: 2015 December 11
pure subroutine VROTEUT(X,CA,SA)
    real(kind=rk),intent(inout) :: X(3)
    real(kind=rk), intent(in) :: CA(3),SA(3)
    call VROT(X,CA(3),-SA(3),'z')
    call VROT(X,CA(2),-SA(2),'y')
    call VROT(X,CA(1),-SA(1),'z')
end subroutine

!Vector rotation about the axis ('x','y','z'). 
!
!Author: Karri Muinonen
!Version: 2015 December 11
pure subroutine VROT(X,c,s,axis)
    real(kind=rk),intent(inout) ::  X(3)
    real(kind=rk),intent(in) :: c,s
    character, intent(in) :: axis
    integer :: i,j
    real(kind=rk) :: q
    if(axis=='x') then
        i=2;j=3
    elseif(axis=='y') then
        i=3;j=1
    elseif(axis=='z') then
        i=1;j=2
    endif
    q   = c*X(i)+s*X(j)
    X(j)=-s*X(i)+c*X(j)
    X(i)=q
end subroutine








end module


