module input_reader
!!Input reader module for rtcb
!!
!!Uses hashtable and InputParser by Antti Penttilä, 2012, 2015
!!
!!There are two possible ways to give input
!!1: program reads input from "input.dat"
!!2: program reads input from the file given
!!   through the command line
!!
!!SYNTAX for the input is:
!!wavelength=0.71123123
!!
!!Possible parameters are listed below and in the
!!documentation coming with the program
!!
!!If the program can't find "input.dat", it will 
!!use default values which are written below.
!!If the program fails to read or find the given file,
!!the program will be terminated
!!
!!Copyright (C) 2016 Timo Väisänen and University of Helsinki
!!All rights reserved.
!!The new BSD License is applied to this software, see LICENSE.txt

    use InputParser
    !!Input parser for input value handling
    use typedefinitions
    !!get definition for dataBlock
    use errorhandler
    implicit none

    private readInput2,filldataBlock,setDefaultValues
contains

!////////////////////////////////////////////////////////
    subroutine readInput(dB)
        type(dataBlock),intent(inout) :: dB
        call setDefaultValues()
        call readInput2()
        if(check_if_errors()) return
        call filldataBlock(dB) 
    end subroutine

!////////////////////////////////////////////////////////
    subroutine setDefaultValues()
    !!Read input to dataBlock
    !!
    !!INOUT: dB
    !!
    !!Fills inputParser with default values and 
    !!inputParser will overwrite them depending on the choice of input.
    !!After it, these values are read to dataBlock
        character(len=64) :: tmp    
        !init inputParser

!#INPUT_READER_SCRIPT1
        call init_input(73)
        call setup_value("wavelength",6.283185307179586_rk)      !!wavelength  (unit)
        call setup_value("med_refractive_real",1.0_rk)           !!medium refractive index(real)
        call setup_value("scatterer_radius",2.0_rk)              !!scatterer radius
        call setup_value("sca_refractive_real",1.31_rk)          !!scatterer refractive index(real)
        call setup_value("sca_refractive_imag",0.0_rk)           !!scatterer refractive index(imag)
        call setup_value("volume_element_radius",20.0_rk)        !!Radius of the volument element (unit)
        call setup_value("volume_fraction",0.125_rk)             !!Volume fraction
        call setup_value("n_angles_in_P",180)                    !!Number of angles in phase and amplitude matrices.
        call setup_value("n_particles_cubic_cell",10000)         !!Number of particles in the cubic cell.
        call setup_value("n_of_cells_generated",5)               !!Number of cubic cells generated.
        call setup_value("n_of_cubic_cells_drawn",100)           !!Number of volume elements drawn from a cubic cell.
        call setup_value("n_of_orientations_for_vol",100)        !!Number of orientations for a volume element.
        call setup_value("generateMieScatterer",.True.)          !!Generate mie scatterer
        write(tmp,"(A)") "scatterer.in"
        call setup_value("input_scatterer_data",tmp,64)          !!Input data for scatterer
        write(tmp,"(A)") "fodetails.out"
        call setup_value("output_details",tmp,64)                !!output for details
        write(tmp,"(A)") "scatterer.out"
        call setup_value("output_scatterer",tmp,64)              !!phase_and_amplitude_matrix
        call setup_value("number_of_scatterers",-1)              !!Radius of the volument element (unit) (overrides r_vol)
        call setup_value("ncm",32)                               !!integration points in spline presen.
        call setup_value("nrn",1000)                             !!number of points in cdf
        call setup_value("seed",0)                               !!seed of prgn
        call setup_value("s_scattering_cross_sec",-1.0_rk)       !!Scatterer's scattering cross section
        call setup_value("s_extinction_cross_sec",-1.0_rk)       !!Scatterer's extinction cross section
        call setup_value("s_abs_cross_sec",-1.0_rk)              !!Scatterer's absorption cross section
!#INPUT_READER_SCRIPT2
        !----------------------------------------
    end subroutine



!////////////////////////////////////////////////////////
    subroutine readInput2()
        !!readInput2()
        !!
        !!check which kind of input is given and
        !!overwrite default values
        !!
        !!The subroutine will check if there are
        !!any command line arguments. If there are none, 
        !!it will try to find "input.dat". Otherwise
        !!default values are used.
        !file where input is located
        character(len=128) :: filename
        !integer for various tasks
        integer :: i
        logical :: fileExists   
        !default input location
        filename="input.dat"
        
        !check whether user has given filename
        i = command_argument_count()      
        if(i==0) then
            write(6,*) "FOICGEN will use default values. Give input file as an argument."
            write(6,*) str_line 
            return
            !Leave, use default values
        else
            !get filename from command line
            call get_command_argument(1,filename)
            !check that file exists
            inquire(file=filename,exist=fileExists)
            if(.not. fileExists) then
                !Critical error so terminate program
                call addError("input_reader: file """//trim(adjustl(filename))//""" does not exists")
                write(6,*) str_line 
                stop
                return
            endif
        endif
        write(6,*) "Reading input from: ",adjustl(trim(filename))
        write(6,*) str_line 
        call read_input(filename, .false. )
    end subroutine




!////////////////////////////////////////////////////////
    subroutine filldataBlock(dB)
    !!Get data from inputParser to datablock
    !!
    !!INOUT: DB: input will be put into this

        type(dataBlock),intent(inout) :: dB
        integer :: tmp
        tmp=64
!#INPUT_READER_SCRIPT3
        call get_value("wavelength",dB%wavel)                    !!wavelength  (unit)
        call get_value("med_refractive_real",dB%mre)             !!medium refractive index(real)
        call get_value("scatterer_radius",dB%scRadius)           !!scatterer radius
        call get_value("sca_refractive_real",dB%scRealRef)       !!scatterer refractive index(real)
        call get_value("sca_refractive_imag",dB%scImagRef)       !!scatterer refractive index(imag)
        call get_value("volume_element_radius",dB%r_vol)         !!Radius of the volument element (unit)
        call get_value("volume_fraction",dB%vf)                  !!Volume fraction
        call get_value("n_angles_in_P",dB%np)                    !!Number of angles in phase and amplitude matrices.
        call get_value("n_particles_cubic_cell",dB%npz)          !!Number of particles in the cubic cell.
        call get_value("n_of_cells_generated",dB%nzm)            !!Number of cubic cells generated.
        call get_value("n_of_cubic_cells_drawn",dB%nsm)          !!Number of volume elements drawn from a cubic cell.
        call get_value("n_of_orientations_for_vol",dB%nor)       !!Number of orientations for a volume element.
        call get_value("generateMieScatterer",dB%generateMieScatterer)!!Generate mie scatterer
        call get_value("input_scatterer_data",dB%scattererData,tmp)!!Input data for scatterer
        call get_value("output_details",dB%output_details,tmp)   !!output for details
        call get_value("output_scatterer",dB%saveScatterer,tmp)  !!phase_and_amplitude_matrix
        call get_value("number_of_scatterers",dB%Nspheres)       !!Radius of the volument element (unit) (overrides r_vol)
        call get_value("ncm",dB%ncm)                             !!integration points in spline presen.
        call get_value("nrn",dB%nrn)                             !!number of points in cdf
        call get_value("seed",dB%seed)                           !!seed of prgn
        call get_value("s_scattering_cross_sec",dB%ssca)         !!Scatterer's scattering cross section
        call get_value("s_extinction_cross_sec",dB%sext)         !!Scatterer's extinction cross section
        call get_value("s_abs_cross_sec",dB%sabs)                !!Scatterer's absorption cross section
!#INPUT_READER_SCRIPT4
    end subroutine


end module
