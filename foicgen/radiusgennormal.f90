
!-------------------------------------------------------------------
! Radius generator of the packing algorithm (single scatterers). 
! This version generates normal distributed radiuses (stdev>0) or
! gives the mean radius (stdev<=0). 
! Arguments are checked with parseArgument(), if the given
! argument is not recognized in the main.f90.
!
! Arguments given to a command line:
! -radius       :: mean radius          (Default: 2)
! -stdev        :: standard deviation   (Default: 0)
!
! Copyright (C) 2018 Timo Väisänen,  University of Helsinki
! All rights reserved.
! The new BSD License is applied to this software, see LICENSE.txt
!
! Modifications:
! 19.3.2018 TV: Maintenance, cleaning, commenting
!
! List of functions/subroutine:
! *parseArgument(arg,i_arg) result(retVal)
! *rad_gen_get_radius() result(retVal)
! *distribute_args_MPI()
! *print_RG_information()
!-----------------------------

module RadiusGenerator
use constants
use rng
implicit none

!Default values
real(kind=rk) :: mean = 2.0
real(kind=rk) :: stdev = 0.0

private :: mean,stdev
contains

!!Parse command line arguments
!!Returns .true. if recognised otherwise .false. 
subroutine pass_arguments(mean0,stdev0)
    real(kind=rk), intent(in) :: mean0,stdev0
    mean = mean0
    stdev = stdev0
end subroutine

!!Call this to generate radiuses
function rad_gen_get_radius() result(retVal)
    real(kind=rk) :: retVal
    retVal = rnd_normal(mean,stdev) 
end function 

!!Print initial information
subroutine print_RG_information()
    write(6,*) "====================================="
    write(6,*) "Radius generator: Normal distribution"
    write(6,*) "mean=",mean,", stdev=",stdev
    write(6,*) "====================================="
end subroutine

!!Print scatterer information to stream
subroutine print_scatterer_information(stream)
    integer, intent(in) :: stream
    write(stream,*) "Normal distribution"
    write(stream,*) "Mean",mean
    write(stream,*) "stdev",stdev
end subroutine

end module
