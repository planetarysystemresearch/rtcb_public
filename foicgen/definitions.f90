module typedefinitions
!!dataBlock: Holds variables
!!Copyright (C) 2017 Timo Väisänen and University of Helsinki
!!All rights reserved.
!!The new BSD License is applied to this software, see LICENSE.txt

    use constants
    implicit none

    type :: dataBlock 
    !!abbreviated int the code as dB
    !!Keeps track of input and precalculated tables.
    !!The policy was to create a type
    !!which can transfer variables easily between
    !!subroutines.


!#INPUT_GENERATOR_SCRIPT1
!#MPITOOLS_SCRIPT1
        real(kind=rk) :: wavel                           !!wavelength  (unit)
        real(kind=rk) :: mre                             !!medium refractive index(real)
        real(kind=rk) :: scRadius                        !!scatterer radius
        real(kind=rk) :: scRealRef                       !!scatterer refractive index(real)
        real(kind=rk) :: scImagRef                       !!scatterer refractive index(imag)
        real(kind=rk) :: r_vol                           !!Radius of the volument element (unit)
        real(kind=rk) :: vf                              !!Volume fraction
        integer :: np                                    !!Number of angles in phase and amplitude matrices.
        integer :: npz                                   !!Number of particles in the cubic cell.
        integer :: nzm                                   !!Number of cubic cells generated.
        integer :: nsm                                   !!Number of volume elements drawn from a cubic cell.
        integer :: nor                                   !!Number of orientations for a volume element.
        logical :: generateMieScatterer                  !!Generate mie scatterer
        character(len=64) :: scattererData               !!Input data for scatterer
        character(len=64) :: output_details              !!output for details
        character(len=64) :: saveScatterer               !!phase_and_amplitude_matrix
        integer :: Nspheres                              !!Radius of the volument element (unit) (overrides r_vol)
        integer :: ncm                                   !!integration points in spline presen.
        integer :: nrn                                   !!number of points in cdf
        real(kind=rk) :: dthe                            !!variable needed by splines1
        integer :: ns                                    !!variable needed by splines2
        integer :: seed                                  !!seed of prgn
        real(kind=rk) :: ssca                            !!Scatterer's scattering cross section
        real(kind=rk) :: sext                            !!Scatterer's extinction cross section
        real(kind=rk) :: sabs                            !!Scatterer's absorption cross section
!#MPITOOLS_SCRIPT2
        real(kind=rk) :: ell1                            !!mean free path(not used)
        real(kind=rk) :: ssalbedo                        !!Albedo of the scatterer(not used)
        character(len=64) :: outputExt                   !!not used
        logical :: addInfo                               !!not used
!#INPUT_GENERATOR_SCRIPT2
    end type


    type :: assembledData
        !!The store for final results.
        !!When a thread has finished a job with
        !!one Stokes parameter configuration, it updates 
        !!these values using the gathered data(accessibleData).
        !!assembledData has to be initialised before
        !!starting the simulation.
        !!abbreviated as aD
        !!MBS(1:2,1:4,1:4,ntheb,nphib): Mueller cb-enhanced
        !!MRT(1:4,1:4,nthe,nphi): Mueller rt-only
        integer, allocatable :: pol_state(:)            !!polarization states
        real(kind=rk), allocatable :: Aref(:)                  !!reflected/scattered
        real(kind=rk), allocatable :: Atra(:)                  !!transmitted
        real(kind=rk), allocatable :: Adt(:)                   !!direct transmission
        real(kind=rk), allocatable :: Aabs(:)                  !!absorption
        real(kind=rk), allocatable :: Astop(:)                 !!leftover intensity
        real(kind=rk), allocatable :: Aspec(:)                 !!specular
        !!4x4x 3(EA,EB,EAB) x ntheb x nphib x pol_states
        real(kind=rk), allocatable :: MBS(:,:,:,:,:,:)  !!CB-solution. Accumulated Mueller matrices
        !!4x4 x nthe x nphi
        real(kind=rk), allocatable :: MRT(:,:,:,:,:)    !!RT-solution. Accumulated Mueller matrices
    end type


    type :: accessibleData
        !!This type holds variables which are
        !!updated frequently during the simulation.
        !!abbreviated as accD
        !!IBS(1:4,1:2,ntheb,nphib): Stokes cb-enhanced
        !!IRT(1:4,nthe,nphi): Mueller rt-only
        real(kind=rk) :: Aref = 0.0_rk                  !!reflected/scattered
        real(kind=rk) :: Atra = 0.0_rk                  !!transmitted
        real(kind=rk) :: Adt = 0.0_rk                   !!direct transmission
        real(kind=rk) :: Aabs = 0.0_rk                  !!absorption
        real(kind=rk) :: Astop = 0.0_rk                 !!leftover intensity
        real(kind=rk) :: Aspec = 0.0_rk                 !!specular
        !!4x4 x 3(EA,EB,EAB) x ntheb x nphib
        real(kind=rk), allocatable :: MBS(:,:,:,:,:)      !!CB-solution. Accumulated intensities
        !!4x4 x nthe x nphi
        real(kind=rk), allocatable :: MRT(:,:,:,:)       !!RT-solution. Accumulated intensities
    end type



end module


