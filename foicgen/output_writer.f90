module output
!!RT-Engine
!!Copyright (C) 2016 Karri Muinonen, Timo Väisänen and University of Helsinki
!!All rights reserved.
!!The new BSD License is applied to this software, see LICENSE.txt
    use constants
    use typedefinitions
    implicit none

    character(len=*), parameter  :: fmt1 = "(A20,3X,I2,'=',E12.5)"

    private fmt1
contains

    subroutine print_arr_with_message(arr,r_arr,ntot,stream,msg,fmt0)
        integer, intent(in) :: stream
        integer, intent(in) :: ntot
        character(*), intent(in) :: msg
        character(*), intent(in) :: fmt0
        real(kind=rk), intent(in) :: arr(:)
        integer, intent(in) :: r_arr(:)
        integer :: j1
        do j1=1,size(arr)
            write(stream,fmt0) msg,r_arr(j1),arr(j1)/ntot
        enddo
    end subroutine

    subroutine printOutput(dB,KIC,PIC0,SIC0,x_vol)
        type(dataBlock),intent(in) :: dB
        real(kind=rk), intent(in) :: x_vol
        real(kind=rk), intent(in) :: PIC0(:,:),KIC(3)
        complex(kind=rk), intent(in) :: SIC0(:,:)
        character(len=120) :: addString
        character(8)  :: date
        character(10) :: time
        character(5)  :: zone
        real(kind=rk) :: renorm,Atmp,Atra,Adt
        real(kind=rk) :: norm,rd,phiout,M(4,4),the
        integer :: j0,j1,j2,j3,j4
        real(kind=rk), allocatable :: MBS(:,:,:,:,:,:),MRT(:,:,:,:,:)
        integer :: ps_count,stream,k2   
        !Atot=sum(Aref)+sum(Asca)+sum(Aspec)+sum(Aabs)+sum(Astop)
        call date_and_time(DATE=date,ZONE=zone)
        call date_and_time(TIME=time)
        write(addString,*) "#FOICGEN: ", " ,date, ",trim(adjustl(date))," ,time, ",trim(adjustl(time)) 
        rd=pi/180.0_rk
        !parameter output
        stream=1
        open(unit=stream,file=trim(adjustl(dB%output_details)))
        write(stream,*) trim(adjustl(addString))        
        call writeInfoScreen(dB,stream,.true.)
        write(stream,*) "////////////////////////////"
        write(stream,'(A30,E12.5)') "volume_element_size",x_vol
        write(stream,'(A30,E12.5)') "kappa extinction",KIC(1)
        write(stream,'(A30,E12.5)') "kappa scattering",KIC(2)
        write(stream,'(A30,E12.5)') "kappa absorption",KIC(3)
        write(stream,'(A30,E12.5)') "albedo",KIC(2)/KIC(1)
        write(stream,'(A30,E12.5)') "mean_free_path",1/(KIC(1))

        write(stream,*) "=============================="
        write(6,'(A30,E12.5)') "volume_element_x",x_vol
        write(6,'(A30,E12.5)') "kappa extinction",KIC(1)
        write(6,'(A30,E12.5)') "kappa scattering",KIC(2)
        write(6,'(A30,E12.5)') "kappa absorption",KIC(3)
        write(6,'(A30,E12.5)') "albedo",KIC(2)/KIC(1)
        write(6,'(A30,E12.5)') "mean_free_path",1/(KIC(1))


        close(unit=stream)

        open(unit=stream,file=trim(adjustl(dB%saveScatterer)))
            write(stream,*)     0.0_rk,PIC0(1,1),0.0d0,1.0d0,0.0d0, &
                            &   real(SIC0(1,1)),aimag(SIC0(1,1)),  &
                            &   real(SIC0(1,2)),aimag(SIC0(1,2))
        do j1=2,size(PIC0,dim=2)
            write(stream,*)     180.0_rk/(size(PIC0,dim=2)-1)*(j1-1),PIC0(1,j1),-PIC0(2,j1)/PIC0(1,j1),    &
                            &   PIC0(3,j1)/PIC0(1,j1),                          &
                            &   PIC0(4,j1)/PIC0(1,j1),                          &
                            &   real(SIC0(j1,1)),aimag(SIC0(j1,1)),                 &
                            &   real(SIC0(j1,2)),aimag(SIC0(j1,2))
        enddo
        close(stream)

    end subroutine

    !For test purposes
    subroutine writeInfoScreen(dB,stream,print_header)
        integer, intent(in) :: stream

        logical, intent(in) :: print_header
        type(dataBlock), intent(in) :: dB
        write(stream,*) "////////////////////////////"
        
        write(stream,*) "FOICGEN (The First Order IC Input GENerator)"
        write(stream,*) "////////////////////////////"
        
!#OUTPUT_GENERATOR_SCRIPT1
        write(stream,'(A30,20X,E12.5)') "wavelength", dB%wavel
        write(stream,'(A30,20X,E12.5)') "med_refractive_real", dB%mre
        write(stream,'(A30,20X,E12.5)') "sca_refractive_real", dB%scRealRef
        write(stream,'(A30,20X,E12.5)') "scatterer_radius", dB%scRadius
        write(stream,'(A30,20X,E12.5)') "sca_refractive_imag", dB%scImagRef
        write(stream,'(A30,20X,E12.5)') "volume_element_radius", dB%r_vol
        write(stream,'(A30,20X,E12.5)') "volume_fraction", dB%vf
        write(stream,'(A30,20X,I12)') "n_angles_in_P", dB%np
        write(stream,'(A30,20X,I12)') "n_particles_cubic_cell", dB%npz
        write(stream,'(A30,20X,I12)') "n_of_cells_generated", dB%nzm
        write(stream,'(A30,20X,I12)') "n_of_cubic_cells_drawn", dB%nsm
        write(stream,'(A30,20X,I12)') "n_of_orientations_for_vol", dB%nor
        write(stream,'(A30,20X,L12)') "generateMieScatterer", dB%generateMieScatterer
        write(stream,'(A30,2X,A30)') "input_scatterer_data",adjustl(trim( dB%scattererData))
        write(stream,'(A30,2X,A30)') "output_details",adjustl(trim( dB%output_details))
        write(stream,'(A30,2X,A30)') "output_scatterer",adjustl(trim( dB%saveScatterer))
        write(stream,'(A30,20X,I12)') "nrn", dB%nrn
        write(stream,'(A30,20X,I12)') "ncm", dB%ncm
        write(stream,'(A30,20X,I12)') "number_of_scatterers", dB%Nspheres
        write(stream,'(A30,20X,E12.5)') "s_scattering_cross_sec", dB%ssca
        write(stream,'(A30,20X,E12.5)') "s_extinction_cross_sec", dB%sext
        write(stream,'(A30,20X,E12.5)') "s_abs_cross_sec", dB%sabs
        write(stream,'(A30,20X,I12)') "seed", dB%seed
!#OUTPUT_GENERATOR_SCRIPT2
        if(dB%generateMieScatterer) then
            write(stream,"(A50)") "Mie Scatterer will be generated"
        else
            write(stream,"(A50)") "Scatterer will be user defined"
        endif
        write(stream,*) str_line
    end subroutine

end module


